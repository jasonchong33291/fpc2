<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(AdminRoleSeeder::class);
        $this->call(AdminSeeder::class);
		$this->call(BannerTypeSeeder::class);
        $this->call(GameSeeder::class);
        $this->call(GameRateSeeder::class);
        $this->call(RoomSeeder::class);
        $this->call(RoomRateSeeder::class);
        $this->call(SettingTemplateSeeder::class);
        $this->call(TopupMethodSeeder::class);
    }
}
