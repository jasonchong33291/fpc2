<?php

namespace Database\Seeders;

use App\Models\BannerType;
use Illuminate\Database\Seeder;

class BannerTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [[
			'id' => 1,
			'name' => "Lobby Top",
			'key' => 'lobby_top'
		]];
			
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = BannerType::find($data['id']);
			
			if(!$inst) {
				$inst = BannerType::create($data);
			}
		}
    }
}
