<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\SettingTemplate;

class SettingTemplateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [[
			'name' => '彩票开彩',
			'key' => 'open_betting',
			'type' => "button",			
			'details' => [
				['name' => '开彩', 'value' => ""],
			]
		], [
			'name' => '彩票生成',
			'key' => 'generate_betting',
			'type' => "button",			
			'details' => [
				['name' => '生成', 'value' => ""],
			]
		], [
			'name' => '删除所有彩票',
			'key' => 'flush_betting',
			'type' => "button",			
			'details' => [
				['name' => '删除', 'value' => ""],
			]
		], [
			'name' => '删除所有下注记录',
			'key' => 'flush_betting_record',
			'type' => "button",			
			'details' => [
				['name' => '删除', 'value' => ""],
			]
		], [
			'name' => '最低提取金额',
			'key' => 'withdraw.min',
			'type' => "number"
		], [
			'name' => '最低充值金额',
			'key' => 'topup.min',
			'type' => "number"
		], [
			'name' => '自动开奖设定',
			'key' => 'betting',
			'type' => "select",
			'details' => [
				['name' => '完全随机', 'value' => "0"],
				['name' => '避重就轻', 'value' => "1"],
				['name' => '通杀', 'value' => "2"],
				['name' => '自动调整', 'value' => "3"],
			]
		], [
			'name' => '自动，避重就轻底线',
			'key' => 'betting.1.min',
			'type' => "number"
		], [
			'name' => '自动，通杀底线',
			'key' => 'betting.2.min',
			'type' => "number"
		], [
			'name' => '避重就轻亏损率(%)',
			'key' => 'betting.1.rate',
			'type' => "number"
		], [
			'name' => '金额名',
			'key' => 'currency',
			'type' => "text"
		]];
			
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];			
			$inst = SettingTemplate::where('key', $data['key'])->first();			
			$details = isset($data['details']) ? $data['details'] : [];
			unset($data['details']);			
			if(!$inst) {
				$inst = SettingTemplate::create($data);
				foreach($details as $detail) {
					$inst->details()->create($detail);
				}
			} else {
				$inst->update($data);							
				$inst->details()->delete();				
				foreach($details as $detail) {
					$inst->details()->create($detail);
				}
			}
		}
    }
}
