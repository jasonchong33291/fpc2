<?php

namespace Database\Seeders;

use App\Models\TopupMethod;
use Illuminate\Database\Seeder;

class TopupMethodSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [[
			'id' => 1,
			'name' => '手动',
		], [
			'id' => 2,
			'name' => '系统',
		], [
			'id' => 3,
			'name' => '钱包',
		]];
			
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = TopupMethod::find($data['id']);
			
			if(!$inst) {
				$inst = TopupMethod::create($data);
			}
		}
    }
}
