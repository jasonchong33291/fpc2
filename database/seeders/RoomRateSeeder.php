<?php

namespace Database\Seeders;

use App\Models\Game;
use App\Models\Room;
use App\Models\RoomRateGroup;
use Illuminate\Database\Seeder;

class RoomRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [];
		$prIndex = 0;
		foreach(Game::all() as $index => $game) {
			if ($game->key == "4d") {
				continue;
			}
			foreach($game->rateGroups as $rgIndex => $group) {
				$item = $group->toArray();
				$item['id'] = $prIndex + $rgIndex + 1;
				$item['room_id'] = $index + 1;
				$item['rates'] = $group->rates->toArray();
				unset($item['game_id']);
				array_push($items, $item);
				//
				if ($game->key == "fpc") {
					$item = $group->toArray();
					$item['id'] = $prIndex + $rgIndex + 1000;
					$item['room_id'] = 10;
					$item['rates'] = $group->rates->toArray();
					foreach($item['rates'] as $rIndex => $rate) {
						$item['rates'][$rIndex]['id'] = $rate['id'] + 1000;
					}
					unset($item['game_id']);
					array_push($items, $item);
				}
			}
			$prIndex += $rgIndex + 1;
		}
		
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = RoomRateGroup::find($data['id']);
			$rates = $data['rates'];
			unset($data['rates']);
			if(!$inst) {
				$inst = RoomRateGroup::create($data);
				foreach($rates as $rate) {
					$inst->rates()->create($rate);
				}
			} else if(config('app.env') !== "production") {
				$inst->update($data);
				$inst->rates()->delete();
				foreach($rates as $rate) {
					$inst->rates()->create($rate);
				}
			}
		}
    }
}
