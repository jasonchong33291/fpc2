<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [[
			'id' => 1,
			'name' => 'Dev Admin',
			'username' => 'dadmin',
			'password' => bcrypt("password"),
			'role_id' => 1
		]];
			
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = Admin::find($data['id']);
			
			if(!$inst) {
				$inst = Admin::create($data);
			}
		}
    }
}
