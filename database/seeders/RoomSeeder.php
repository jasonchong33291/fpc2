<?php

namespace Database\Seeders;

use App\Models\Room;
use App\Models\Game;
use Illuminate\Database\Seeder;

class RoomSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [];
		$intervals = [45, 60];
		
		foreach(Game::all() as $index => $game) {
			if ($game->key == "4d") {
				continue;
			}
			array_push($items, [
				'id' => $index + 1,
				'name' => $game->name . " test deck",
				'icon_url' => asset('images/' . $game->key . '.jpeg'),
				'image_url' => asset('images/' . $game->key . '.jpeg'),
				'game_id' => $game->id,
				'interval' => isset( $intervals[$index]) ? $intervals[$index] : null
			]);
			if ($game->key == "fpc") {
				array_push($items, [
					'id' => 10,
					'name' => $game->name . " standalone test deck",
					'icon_url' => asset('images/' . $game->key . '.jpeg'),
					'image_url' => asset('images/' . $game->key . '.jpeg'),
					'game_id' => $game->id,
					'is_standalone' => true
				]);
			}
		}
					
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = Room::find($data['id']);
			
			if(!$inst) {
				Room::create($data);
			} else {// if(config('app.env') !== "production") {
				$inst->update($data);
			}
		}
    }
}
