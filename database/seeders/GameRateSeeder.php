<?php

namespace Database\Seeders;

use App\Models\GameRateGroup;
use App\Models\Betting;
use Illuminate\Database\Seeder;

class GameRateSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		$items = [
			[
				'id' => 1,
				'name' => '三军',
				'group' => 'single_any',
				'rates' => [
					[
						"name" => "单股 1x",
						"matched" => 1,
						"rate" => 1
					],
					[
						"name" => "单股 1x",
						"matched" => 2,
						"rate" => 2
					],
					[
						"name" => "单股 1x",
						"matched" => 3,
						"rate" => 3
					]
				],
				'game_id' => 1
			],
			[
				'id' => 2,
				'name' => '任何二色',
				'group' => 'double_any',
				'rates' => [
					[
						"name" => "各为单股 1x",
						"matched" => 2,
						"rate" => 5
					],
					[
						"name" => "各为单股 1x",
						"matched" => 3,
						"rate" => 5
					]
				],
				'game_id' => 1
			],
			[
				'id' => 3,
				'name' => '任何围股',
				'group' => 'match_all',
				'rates' => [
					[
						"name" => "任何围股 1x",
						"matched" => 3,
						"rate" => 30
					]
				],
				'game_id' => 1
			],
			[
				'id' => 4,
				'name' => 'color',
				'group' => 'color',
				'rates' => [
					[
						"name" => "color",
						"matched" => 1,
						"rate" => 2.5
					]
				],
				'game_id' => 2
			],
			[
				'id' => 5,
				'name' => 'number',
				'group' => 'number',
				'rates' => [
					[
						"name" => "number",
						"matched" => 1,
						"rate" => 6
					]
				],
				'game_id' => 2
			],
			[
				'id' => 6,
				'name' => 'ten range',
				'group' => 'ten_range',
				'rates' => [
					[
						"name" => "ten_range",
						"matched" => 1,
						"rate" => 6
					]
				],
				'game_id' => 2
			],
			[
				'id' => 7,
				'name' => 'twenty range',
				'group' => 'twenty_range',
				'rates' => [
					[
						"name" => "twenty_range",
						"matched" => 1,
						"rate" => 3
					]
				],
				'game_id' => 2
			],
			[
				'id' => 8,
				'name' => 'single',
				'group' => 'single',
				'rates' => [
					[
						"name" => "single",
						"matched" => 1,
						"rate" => 60
					]
				],
				'game_id' => 2
			],
			[
				'id' => 9,
				'name' => '4D 1st Prize',
				'group' => '4d_1st_prize',
				'rates' => [
					[
						"name" => "1st_prize",
						"matched" => 4,
						"rate" => 60
					]
				],
				'game_id' => 3
			],
			[
				'id' => 10,
				'name' => '4D 2nd Prize',
				'group' => '4d_2nd_prize',
				'rates' => [
					[
						"name" => "2nd_prize",
						"matched" => 4,
						"rate" => 30
					]
				],
				'game_id' => 3
			],
			[
				'id' => 11,
				'name' => '4D 3rd Prize',
				'group' => '4d_3rd_prize',
				'rates' => [
					[
						"name" => "3rd_prize",
						"matched" => 4,
						"rate" => 10
					]
				],
				'game_id' => 3
			],
			[
				'id' => 12,
				'name' => '4D Special Prize',
				'group' => '4d_special_prize',
				'rates' => [
					[
						"name" => "special_prize",
						"matched" => 4,
						"rate" => 5
					]
				],
				'game_id' => 3
			],
			[
				'id' => 13,
				'name' => '4D Consolation Prize',
				'group' => '4d_consolation_prize',
				'rates' => [
					[
						"name" => "consolation_prize",
						"matched" => 4,
						"rate" => 3
					]
				],
				'game_id' => 3
			],
			[
				'id' => 14,
				'name' => '2D 1st Prize',
				'group' => '2d_1st_prize',
				'rates' => [
					[
						"name" => "big_1st_prize",
						"matched" => 2,
						"rate" => 16
					],
					[
						"name" => "small_1st_prize",
						"matched" => 2,
						"rate" => 23
					]
				],
				'game_id' => 4
			],
			[
				'id' => 15,
				'name' => '2D 2nd Prize',
				'group' => '2d_2nd_prize',
				'rates' => [
					[
						"name" => "big_2nd_prize",
						"matched" => 2,
						"rate" => 7
					],
					[
						"name" => "small_2nd_prize",
						"matched" => 2,
						"rate" => 14
					]
				],
				'game_id' => 4
			],
			[
				'id' => 16,
				'name' => '2D 3rd Prize',
				'group' => '2d_3rd_prize',
				'rates' => [
					[
						"name" => "big_3rd_prize",
						"matched" => 2,
						"rate" => 4
					],
					[
						"name" => "small_3rd_prize",
						"matched" => 2,
						"rate" => 7
					]
				],
				'game_id' => 4
			],
			[
				'id' => 17,
				'name' => '2D Special Prize',
				'group' => '2d_special_prize',
				'rates' => [
					[
						"name" => "special_prize",
						"matched" => 2,
						"rate" => 2
					]
				],
				'game_id' => 4
			],
			[
				'id' => 18,
				'name' => '2D Consolation Prize',
				'group' => '2d_consolation_prize',
				'rates' => [
					[
						"name" => "consolation_prize",
						"matched" => 2,
						"rate" => 2
					]
				],
				'game_id' => 4
			]
		];
			
		for($i = 0; $i < count($items); $i++) {
			$data = $items[$i];
			
			$inst = GameRateGroup::find($data['id']);
			$rates = $data['rates'];
			unset($data['rates']);
			if(!$inst) {
				$inst = GameRateGroup::create($data);
				foreach($rates as $rate) {
					$inst->rates()->create($rate);
				}
			} else if(config('app.env') !== "production") {
				$inst->update($data);
				$inst->rates()->delete();
				foreach($rates as $rate) {
					$inst->rates()->create($rate);
				}
			}
		}
    }
}
