<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateInvitationRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('invitation_records', function (Blueprint $table) {
            $table->id();
			$table->string('mobile');
			$table->string('status');
			$table->string('from');
			$table->foreignId('inviter_id')->constrained("users")->onDelete('cascade');
			$table->foreignId('invited_id')->constrained("users")->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('invitation_records');
    }
}
