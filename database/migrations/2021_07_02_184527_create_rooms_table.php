<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('rooms', function (Blueprint $table) {
            $table->id();
			$table->string("name");
			$table->decimal("single_rate", 5, 2);
			$table->decimal("soe_rate", 5, 2);
			$table->decimal("boe_rate", 5, 2);
			$table->decimal("br_rate", 5, 2);
			$table->decimal("entrance", 12, 2)->nullable();
			$table->decimal("withdraw_limit", 12, 2)->nullable();
			$table->decimal("min", 12, 2)->nullable();
			$table->decimal("max", 12, 2)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
