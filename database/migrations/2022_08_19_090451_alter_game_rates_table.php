<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterGameRatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('game_rates', function (Blueprint $table) {
            $table->dropColumn('group');
            $table->dropForeign('game_rates_game_id_foreign');
            $table->dropColumn('game_id');
			$table->tinyInteger("matched");
            $table->foreignId('group_id')->nullable()->onDelete('set null')->constrained("game_rate_groups");
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
