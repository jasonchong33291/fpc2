<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopupMethodsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_methods', function (Blueprint $table) {
            $table->id();
			$table->string('name');
			$table->text('icon_path')->nullable();
			$table->text('trigger_url')->nullable();
			$table->text('callback_url')->nullable();
			$table->text('api_key')->nullable();
			$table->text('api_secret')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_methods');
    }
}
