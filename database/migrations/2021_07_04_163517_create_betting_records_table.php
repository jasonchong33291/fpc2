<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBettingRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('betting_records', function (Blueprint $table) {
            $table->id();
			$table->decimal('rate', 12, 2);
			$table->decimal('amount', 12, 2);
			$table->string('bigsmall')->nullable();
			$table->string('oddeven')->nullable();
			$table->string('blackred')->nullable();
			$table->foreignId('user_id')->constrained()->onDelete('cascade');
			$table->foreignId('betting_id')->constrained()->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('betting_records');
    }
}
