<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterBettingRecords2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('betting_records', function (Blueprint $table) {
            $table->dropColumn('bigsmall');
            $table->dropColumn('oddeven');
            $table->dropColumn('blackred');
            $table->string('play_type');
            $table->string('play_group');
            $table->string('play_option');
		});
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
