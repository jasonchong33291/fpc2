<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTopupRecordsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('topup_records', function (Blueprint $table) {
            $table->id();
			$table->decimal('amount', 12, 2);
			$table->string('status');
			$table->foreignId('user_id')->constrained()->onDelete('cascade');
			$table->foreignId('method_id')->constrained("topup_methods")->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('topup_records');
    }
}
