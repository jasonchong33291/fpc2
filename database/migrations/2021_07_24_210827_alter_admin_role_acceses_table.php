<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AlterAdminRoleAccesesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('admin_role_access', function (Blueprint $table) {
			$table->dropColumn('module');
			$table->dropColumn('access');
			$table->foreignId('access_id')->constrained("role_accesses");
			$table->rename("admin_role_accesses");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
