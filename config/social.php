<?php

return [
	'telegram' => env('TELEGRAM'),
	'whatsapp' => env('WHATSAPP'),
];