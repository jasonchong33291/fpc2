<?php

namespace App;

use Route;
use Auth;
use Str;
use URL;

class RouteGuard
{
	private static function match($path, ...$patterns) {
		$decoded = rawurldecode(str_replace(config('app.url'), "", $path));
		foreach ($patterns as $pattern) {
            if (Str::is($pattern, $decoded)) {
                return true;
            }
        }
        return false;
	}
	
	public static function available($route) {
		$user = Auth::guard('admin')->user();
		if($user->role) {
			if($user->role->accesses) {
				foreach($user->role->accesses as $access) {
					foreach(explode("\n", $access->modules) as $module) {
						if(self::match($route, $module)) {
							if($access->available) {
								return true;
							} else {
								return false;
							}
						}
					}
				}
			}
			
			if (!$user->role->default_access) {
				return false;
			} else {		
				return true;
			}
		} else {
			return null;
		}
	}
	
	public static function check($routeName, $default = "") {
		$route = Route($routeName);
		return self::available($route) ? $route : $default;
	}
}