<?php

namespace App\Exceptions;

use App\Exceptions\HandledException;
use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class Handler extends ExceptionHandler
{
    /**
     * A list of the exception types that are not reported.
     *
     * @var array
     */
    protected $dontReport = [
        HandledException::class
    ];

    /**
     * A list of the inputs that are never flashed for validation exceptions.
     *
     * @var array
     */
    protected $dontFlash = [
        'current_password',
        'password',
        'password_confirmation',
    ];

    /**
     * Register the exception handling callbacks for the application.
     *
     * @return void
     */
    public function register()
    {
        $this->reportable(function (Throwable $e) {
            //
        });
		
		$this->renderable(function (HttpException $e, $request) {
			if ($e->getPrevious() instanceof TokenMismatchException) {
				return $request->expectsJson()
					? response()->json(['message' => '网页超时，请刷新尝试'], 419)
					: redirect()->route('login');
			}
		});
    }
}
