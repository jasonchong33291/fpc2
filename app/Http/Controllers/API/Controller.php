<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller as BaseController;
use Auth;

class Controller extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth:admin-api')->except("token");
    }

	public function getAdmin() {
		return Auth::guard("admin-api")->user();
	}
}
