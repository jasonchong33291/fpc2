<?php

namespace App\Http\Controllers\User;

use App\Models\Betting;
use App\Models\Room;
use Exception;
use Request;
use stdClass;
use Carbon\Carbon;

class BettingRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('records.betting', ['rooms' => Room::all(),
			'fpc_order' => Betting::FPC_ORDER]);
    }
	
    public function index4d($id)
    {
		return view('records.betting-list', ['four_d' => Betting::FOUR_D, 'room_id' => $id]);
    }
	
	private function dataset($item) {
		$item->betting_results = $item->betting ? $item->betting->results->pluck('value') : null;
		$item->betting_rates = $item->rates()->pluck('rate');
		return $item;
	}
	
	private function checkColumn($query, $index, $sort, $search, $tz) {
		// join
		$query->join("bettings", 'bettings.id', '=', 'betting_records.betting_id')
			->join("rooms", 'bettings.room_id', '=', 'rooms.id');
		// pre-sort
		switch($index) {
			case 0:
				// betting.room.name
				$query->orderBy("room_name", $sort);
				break;
			case 1:
				// betting.version
				$query->orderBy("betting_version", $sort);
				break;
			case 2:
				// amount
				$query->orderBy("betting_records.amount", $sort);
				break;
			case 3:
				// translated_play
				// $query->orderBy("translated_play", $sort);
				break;
			case 4:
				// rate
				$query->orderBy("betting_records.rate", $sort);
				break;
			case 5:
				// won
				$query->orderBy("betting_records.won", $sort);
				break;
			case 6:
				// result
				$query->orderBy("result_text", $sort);
				break;
			case 7:
				// created_at
				$query->orderBy("betting_records.created_at", $sort);
				break;
			default:
				//
		}
		// select
		$query->select(['betting_records.amount', 'betting_records.rate', 'betting_records.won', 'betting_records.created_at'
				, 'rooms.name as room_name', 'bettings.version as betting_version'
				, 'betting_records.ball', 'betting_records.bigsmall', 'betting_records.oddeven', 'betting_records.blackred'])
			->selectRaw("if(betting_records.won is null, '未开奖', if(betting_records.won > 0, '中奖', '没中奖')) as result_text");
			// ->selectRaw("rooms.name as translated_play");
		// pre-filter
		if($search) {
			dd($tz);
			dd(Carbon::createFromFormat('m/d/Y, H:i:s u', $search, $tz));
			return $query->where('betting_records.amount', 'like', '%' . $search . '%')
			->orWhere('betting_records.rate', 'like', '%' . $search . '%')
			->orWhere('betting_records.won', 'like', '%' . $search . '%')
			->orWhere('betting_records.created_at', '>=', Carbon::parse($search))
			->orWhere('rooms.name', 'like', '%' . $search . '%')
			->orWhere('bettings.version', 'like', '%' . $search . '%');
		} else {		
			return $query;
		}
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function listing()
    // {
		// $res = new stdClass;
		// $res->data = [];
		
		// try {
			// $offset = Request::input('start', 0);
			// $limit = Request::input('length', 10);
			// $order = Request::input('order', [["column" => 0, "dir" => "asc"]])[0];
			// $search = Request::input('search');
			// $tz = Request::input('tz');
			
			// $query = parent::getUser()->bettingRecords();
			// $query = $this->checkColumn($query
				// , $order["column"], $order["dir"], $search['value'], $tz);
			
			// $res->draw = Request::input('draw');
			// $res->recordsTotal = $query->count();
			// $res->recordsFiltered = $search['value'] ? $query->count() : $res->recordsTotal;		
			
			// $filtered = $query->offset($offset)->limit($limit)->get();
			// foreach($filtered as $item) {
				// array_push($res->data, $this->dataset($item));
			// }
		// } catch(Exception $ex) {
			// $res->error = $ex->getMessage();
		// }
        // return response()->json($res);
    // }
	public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {			
			$query = parent::getUser()->bettingRecords()->with(['betting.room']);
			if($roomId = Request::input('room_id')) {
				$query->whereHas('betting', function($bQuery) use ($roomId) {
					$bQuery->where('room_id', $roomId);
				});
			}
			if($result = Request::input('result')) {
				$query->whereRaw("if(betting_records.won is null, '未开奖', if(betting_records.won > 0, '中奖', '没中')) = '" . $result . "'");
			}
			foreach($query->get() as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
        return response()->json($res);
    }
	
	public function listing4d($roomId)
    {
		$res = new stdClass;
		$res->data = [];
		
		try {			
			$offset = Request::input('offset', 0);
			$limit = Request::input('length', 10);
			$pre = parent::getUser()->bettingRecords()
			->whereHas('betting', function($bQuery) use ($roomId) {
				$bQuery->where('room_id', $roomId);
			})->orWhereNull('betting_id')
			->groupBy(['group'])
			->select(['group'])
			->offset($offset)
			->limit($limit)
			->pluck('group');
			$query = parent::getUser()->bettingRecords()->with(['betting.room'])
				->orderBy('created_at', 'desc')
				->whereIn('group', $pre);
			foreach($query->get() as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			throw $ex;
		}
        return response()->json($res);
    }
}
