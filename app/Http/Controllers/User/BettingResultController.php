<?php

namespace App\Http\Controllers\User;

use App\Exceptions\HandledException;
use App\Models\Betting;
use Request;
use Carbon\Carbon;
use Artisan;

class BettingResultController extends Controller
{	
    public function index($id)
    {
        return view('result', ['four_d' => Betting::FOUR_D, 'room_id' => $id]);
    }
	
    public function result($id)
    {
		$date = Request::input('date');
		if (!$date) {
			throw new HandledException("Missing Date");
		}
		$betting = Betting::whereDate('date', '<', $date)
			->where("group", Request::input('type'))
			->first();
		$loading = false;
		if (!$betting && $date < Carbon::now()->toDateString()) {
			// load old data if not exist
			$loading = true;
			Artisan::call("betting:fetch --date=" . $date . " --no-open");
		}
		return response()->json([
			'betting' => $betting ? $betting->only(['version', 'date', 'group']) : null,
			'prizes' => $betting ? $betting->results: [],
			'loading' => $loading
		]);
    }
}
