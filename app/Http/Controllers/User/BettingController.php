<?php

namespace App\Http\Controllers\User;

use App\Exceptions\HandledException;
use App\Models\Betting;
use App\Models\BettingRecord;
use App\Models\Room;
use App\Models\Setting;
use Carbon\Carbon;
use Request;
use Exception;
use DB;

class BettingController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index($id)
    {
		$room = Room::find($id);
		if(!$room) {
			return redirect()->route('lobby')->withErrors(["message" => "房间不存在。"]);
		}
		$user = parent::getUser();
		if($room->entrance && $user->accumulate < $room->entrance) {
			return redirect()->route('lobby', ['group' => $room->group])->withErrors(["message" => "权限不足，此房间最低需充值过". $room->entrance . "美金才能入场。"]);
		}
		$rpiSetting = Setting::where('key', 'redpack_interval')->first();
		$interval = $rpiSetting ? $rpiSetting->value : 10 * 60;
		$now = Carbon::now();
		if ($last = $user->redPacks()->orderByDesc('created_at')->first()) {
			$last->created_at = $last->created_at->addSecond($interval);
			$remain = $last->created_at->gt($now) ? $last->created_at->diffInSeconds($now) : 0;
		} else {
			$remain = 0;
		}
		return view($room->game->key, [
			'user' => $user,
			'room' => $room,
			'interval' => $remain,
			'fpc_order' => Betting::FPC_ORDER,
			'four_d' => Betting::FOUR_D
		]);
    }
	
	public function status($id)
    {
		$user = parent::getUser();
		$records = $user->bettingRecords()->where('betting_id', Request::input("betting_id", null))->get();
		
		return response()->json([
			'cost' => $records->sum("amount"),
			'won' => $records->sum("won"),
			"user" => ["total_balance" => number_format($user->totalBalance, 2)]
		]);
    }
	
	public function record($id)
    {
		$room = Room::find($id);
		if(!$room) {
			throw new HandledException("房间不存在。");
		}
        return $room->bettingRecords()->with(['user', 'betting'])
			->where('betting_id', Request::input('betting_id', null))
			->orderByDesc('created_at')->get();
    }
	
	// currently, process 4d differently
    public function bet4d($id)
    {
		$room = Room::find($id);
		if(!$room) {
			throw new HandledException("房间不存在，请尝试刷新页面。");
		}
		$user = parent::getUser();
		$data = Request::input();	
		if (!isset($data['records'])) {
			throw new HandledException("数据异常，请尝试刷新页面。");
		}
		$rebet = isset($data['rebet']) ? $data['rebet'] : 1;
		$total = array_reduce($data['records'], function($pv, $item) {
			return $pv + $item['amount'];
		}, 0);
		if($user->totalBalance < $total * $rebet) {
			throw new HandledException("余额不足，请充值。");
		}		
		$newRecords = [];
		$date = Carbon::now();
		$counter = BettingRecord::whereDate('created_at', $date->format('Y-m-d'))->distinct()->count('group');
		$group = $date->format("ymd") . str_pad($counter + 1, 6, "0", STR_PAD_LEFT);
		$upcomingBettings = $room->upcomingBettings;
		try {
			DB::beginTransaction();
			for ($i = 0; $i < $rebet; $i++) {
				foreach($data['records'] as $item) {
					$amount = $item['amount'];
					if($room->min && $amount < $room->min) {
						throw new HandledException("最低" . $room->min . "美元，请调整后重试。");
					}
					if($room->max && $amount > $room->max) {
						throw new HandledException("最多" . $room->max . "美元，请调整后重试。");
					}
					if(!$game = $room->game) {
						throw new HandledException("玩法不存在，请尝试刷新页面。");
					}
					if (!$item['value']) {
						throw new HandledException("资料不完整。");
					}
					$betting = $upcomingBettings->where("group", $item['type'])->skip($i)->first();
					if (!$betting) {
						$betting = $room->bettings()->create([
							"version" => -$i - 1,
							"group" => $item["type"]
						]);
					}
					$record = $betting->records()->create([
						"user_id" => $user->id,
						'amount' => $amount,
						"play_type" => $game->key,
						"play_name" => Betting::FOUR_D[$item['type']],
						"play_group" => $item['option'],
						"play_option" => $item['value'],
						'group' => $group,
						'group2' => $item['group']
					]);
					$user->bettingRecords()->save($record);
					array_push($newRecords, $record);
				}
			}
			$response = [
				"record" => $newRecords,
				"count" => count($newRecords),
				"user" => ["total_balance" => number_format($user->totalBalance - $total, 2)]
			];
			DB::commit();
			return response()->json($response);
		} catch (Exception $ex) {
			DB::rollback();
			throw $ex;
		}
    }
	
    public function bet($id)
    {
		$room = Room::find($id);
		if(!$room) {
			throw new HandledException("房间不存在，请尝试刷新页面。");
		}
		$user = parent::getUser();
		$data = Request::input();	
		if (!isset($data['records'])) {
			throw new HandledException("数据异常，请尝试刷新页面。");
		}
		$rebet = isset($data['rebet']) ? $data['rebet'] + 1 : 1;
		$total = array_reduce($data['records'], function($pv, $item) {
			return $pv + $item['amount'];
		}, 0);
		if($user->totalBalance < $total * $rebet) {
			throw new HandledException("余额不足，请充值。");
		}
		if (!$room->is_standalone) {
			if (!isset($data['betting_id'])) {
				throw new HandledException("数据异常，请尝试刷新页面。");
			}
			$pvBetting = $room->activeBettings()->find($data["betting_id"]);
			if(!$pvBetting) {
				throw new HandledException("当前投注结果正在处理中。");
			}
			if($pvBetting->ended_at < Carbon::now()) {
				throw new HandledException("第" . $pvBetting->id . "期投注已结束。");
			}
			// rebet
			if ($rebet > 0) {
				$bettings = $room->activeBettings()
					->where('id', '>=', $pvBetting->id)
					->limit($rebet + 1)
					->get();
			}
		}
		$newRecords = [];
		$date = Carbon::now();
		$counter = BettingRecord::whereDate('created_at', $date->format('Y-m-d'))->distinct()->count('group');
		$group = $date->format("ymd") . str_pad($counter + 1, 6, "0", STR_PAD_LEFT);
		for ($i = 0; $i < $rebet; $i++) {
			if ($room->is_standalone) {
				$date = Carbon::now();						
				$betting = $room->bettings()->create([
					"version" => str_pad($date->hour, 2, "0", STR_PAD_LEFT)
							. ":" . str_pad($date->minute, 2, "0", STR_PAD_LEFT)
							. ":" . str_pad($date->second, 2, "0", STR_PAD_LEFT),
					"ended_at" => null,
					"reward" => 0,
					"standalone_user" => $user->id
				]);
			} else {
				$betting = $bettings->get($i);
			}
			foreach($data['records'] as $item) {
				$amount = $item['amount'];
				if($room->min && $amount < $room->min) {
					throw new HandledException("最低" . $room->min . "美元，请调整后重试。");
				}
				if($room->max && $amount > $room->max) {
					throw new HandledException("最多" . $room->max . "美元，请调整后重试。");
				}
				if(!$game = $room->game) {
					throw new HandledException("玩法不存在，请尝试刷新页面。");
				}
				if(!$rateGroup = $room->rateGroups()->find($item['room_rate_id'])) {
					throw new HandledException("赔率不存在，请尝试刷新页面。");
				}
				$record = new BettingRecord([
					'amount' => $amount,
					"play_type" => $game->key,
					"play_name" => $rateGroup->name,
					"play_group" => $rateGroup->group,
					"play_option" => $item['value'],
					'group' => $group
				]);
				$record->betting()->associate($betting);
				$user->bettingRecords()->save($record);
				foreach($rateGroup->rates as $gRate) {
					$record->rates()->create([
						'matched' => $gRate->matched,
						'rate' => $gRate->rate
					]);
				}
				array_push($newRecords, $record);
			}
		}
		// standalone
		if ($room->is_standalone) {
			$betting->endBetting();
			$betting->deliverReward();
		}
		$response = [
			"record" => $newRecords,
			"count" => count($newRecords),
			"user" => ["total_balance" => number_format($user->totalBalance - $total, 2)]
		];		
		if ($room->is_standalone) {
			$response["result"] = $betting->only(["id", "version", "results"]);
		}		
		return response()->json($response);
    }
	
	public function clear($id) {
		$room = Room::find($id);
		if(!$room) {
			throw new HandledException("房间不存在，请尝试刷新页面。");
		}		
		$data = Request::input();
		$betting = $room->activeBettings()->find($data["betting_id"]);
		if(!$betting) {
			throw new HandledException("当前投注结果正在处理中。");
		}
		if($betting->ended_at < Carbon::now()) {
			throw new HandledException("第" . $betting->id . "期投注已结束。");
		}
		$user = parent::getUser();
		$recordIds = Request::input("ids", []);
		if (count($recordIds) > 0) {
			$records = $user->bettingRecords()->whereIn("id", $recordIds)
				->where('betting_id', ">=", $room->current->id)->get();
		} else {
			$records = $user->bettingRecords()->where('betting_id', $room->current->id)->get();
		}
		$total = $records->sum("amount");
		BettingRecord::destroy($records->pluck('id'));
		$balance = $user->totalBalance + $total;
        return response()->json([
			"user" => ["total_balance" => number_format($balance, 2)],
			"count" => $user->bettingRecords()->where('betting_id', $betting->id)->count(),
		]);
	}
	
	public function redpack($id) {
		$room = Room::find($id);
		if(!$room) {
			throw new HandledException("房间不存在，请尝试刷新页面。");
		}		
		$user = parent::getUser();
		$rpiSetting = Setting::where('key', 'redpack_interval')->first();
		$interval = $rpiSetting ? $rpiSetting->value : 10 * 60;
		$now = Carbon::now();
		if ($last = $user->redPacks()->orderByDesc('created_at')->first()) {
			$last->created_at = $last->created_at->addSecond($interval);
			if($last->created_at->gt($now)) {
				throw new HandledException("冷却未结束," . $last->created_at->diffInSeconds($now) . "秒后可领取。");
			}
		}
		// check interval
		$minSetting = Setting::where('key', 'redpack_min')->first();
		$min = $minSetting ? $minSetting->value : 1;
		$maxSetting = Setting::where('key', 'redpack_max')->first();
		$max = $maxSetting ? $maxSetting->value : 50;
		$redPack = $user->redPacks()->create([
			'amount' => rand($min, $max) / 100,
			'room_jd' => $room->id
		]);
		return response()->json([
			"redpack" => $redPack,
			"interval" => $interval,
			"user" => ["total_balance" => number_format($redPack->user->totalBalance, 2)]
		]);
	}
	
	public function rebet($id) {
		$room = Room::find($id);
		if(!$room) {
			throw new HandledException("房间不存在，请尝试刷新页面。");
		}
		$data = Request::input();
		$pvBetting = $room->activeBettings()->find($data["betting_id"]);
		if(!$pvBetting) {
			throw new HandledException("当前投注结果正在处理中。");
		}
		if($pvBetting->ended_at < Carbon::now()) {
			throw new HandledException("第" . $pvBetting->id . "期投注已结束。");
		}
		$user = parent::getUser();
		$total = array_reduce($data['records'], function($pv, $item) {
			return $pv + $item['amount'];
		}, 0);
		if($user->totalBalance < $total) {
			throw new HandledException("余额不足，请充值。");
		}
		$count = $data['count'];
		$pvRecords = $user->bettingRecords()->where("betting_id", $pvBetting->id)->get();
		$query = $room->activeBettings()->where('betting_id', '>', $pvBetting->id);
		if ($count > 1) {
			$query->offset($count - 1);
		}
		$betting = $query->first();
		if ($betting) {
			foreach ($pvRecords as $pvRecord) {
				$record = new BettingRecord([
					'amount' => $pvRecord->amount,
					"play_type" => $pvRecord->play_type,
					"play_name" => $pvRecord->play_name,
					"play_group" => $pvRecord->play_group,
					"play_option" => $pvRecord->play_option
				]);
				$record->betting()->associate($betting);
				$user->bettingRecords()->save($record);
				foreach($pvRecord->rates as $pvRate) {
					$record->rates()->create([
						'matched' => $pvRate->matched,
						'rate' => $pvRate->rate
					]);
				}
			}
		}
		return response()->json([
			"user" => ["total_balance" => number_format($user->totalBalance - $total, 2)],
		]);
	}
	
	public function rebetLast($id) {
		$room = Room::find($id);
		if(!$room) {
			throw new HandledException("房间不存在，请尝试刷新页面。");
		}
		$user = parent::getUser();
		$betting = $room->endedBettings()
			->whereHas('records', function($q) use ($user) {
				return $q->where('user_id', $user->id);
			})
			->where('room_id', $room->id)
			->first();
		$records = $betting ? $betting->records : [];
		if($records->count() == 0) {
			throw new HandledException("无投注记录，无发重复投注。");
		}
		if (!$room->is_standalone) {
			$betting = $room->activeBettings()->find(Request::input("betting_id"));
			if(!$betting) {
				throw new HandledException("当前投注结果正在处理中。");
			}
		}
		$pvRecords = [];
		$index = 0;
		$targetBettingId = $records->get($index)->betting_id;
			
		while (($record = $records->get($index))
			&& $record->betting_id == $targetBettingId) {
			array_push($pvRecords, $record);
			$index++;
		}
		$total = array_reduce($pvRecords, function($pv, $record) {
			return $pv + $record->amount;
		}, 0);
		if($user->totalBalance < $total) {
			throw new HandledException("余额不足，请充值。");
		}
		if ($room->is_standalone) {
			$newRecords = [];
			foreach ($pvRecords as $pvRecord) {
				if ($roomRate = $room->rateGroups()
					->where('name', $pvRecord->play_name)
					->where('group', $pvRecord->play_group)
					->first()) {
					$pvRecord->room_rate_id = $roomRate->id;
					$pvRecord->group = $pvRecord->betting_id;
				}
				unset($pvRecord->id);
				array_push($newRecords, $pvRecord);
			}
			$balance = $user->totalBalance;
		} else {
			$newRecords = [];
			foreach ($pvRecords as $pvRecord) {
				$record = new BettingRecord([
					'amount' => $pvRecord->amount,
					"play_type" => $pvRecord->play_type,
					"play_name" => $pvRecord->play_name,
					"play_group" => $pvRecord->play_group,
					"play_option" => $pvRecord->play_option,
					'group' => $pvRecord->betting_id
				]);
				$record->betting()->associate($betting);
				$user->bettingRecords()->save($record);
				foreach($pvRecord->rates as $pvRate) {
					$record->rates()->create([
						'matched' => $pvRate->matched,
						'rate' => $pvRate->rate
					]);
				}
				array_push($newRecords, $record);
			}
			$balance = $user->totalBalance - $total;
		}
		return response()->json([
			"user" => ["total_balance" => number_format($balance, 2)],
			"records" => $newRecords
		]);
	}
}
