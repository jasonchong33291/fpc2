<?php

namespace App\Http\Controllers\User;

use App\Exceptions\HandledException;
use App\Models\BannerType;
use App\Models\Room;
use App\Models\RoomGroup;
use App\Models\Setting;
use Request;
use Exception;

class HomeController extends Controller
{	
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function lobby()
    {
		$sGroup = $this->getGroup(Request::input('group'));
		$groups = null;
		$rooms = [];
		
		if($sGroup) {
			$rooms = $sGroup->rooms;
		} else {
			$groups = RoomGroup::all();
			
			foreach($groups as $group) {
				$roomGroup = $group->rooms->sortBy(function($item) {
					return $item->rates()->max('rate');
				});
				$group->rate = $roomGroup->count() > 0 ? $roomGroup->first()->rates()->max('rate') : 0;
				$group->total = 0;
				foreach($roomGroup as $room) {
					$group->total += $room->base_user_count + $room->totalUniqueUsers;
				}
			}
			
			if ($groups->count() == 0) {
				$groups = null;
				$rooms = Room::all();
			}
		}
        return view('home', ['user' => $this->getUser(), 'banner_groups' => BannerType::where("key", "like", "lobby_%")->with("banners")->get()
			, 'rooms' => $rooms, 'groups' => $groups, 'group' => $sGroup]);
    }
	
	private function getGroup($groupAlias) {
		$group = RoomGroup::where('group', $groupAlias)->first();
		
		return $group ?: null;
	}
	
	public function withdraw() {
		$user = parent::getUser();
		$data = Request::input();
		
		if(!isset($data['amount']) || $data['amount'] == 0) {
			throw new HandledException("提取金额不可为0。");
		}
		
		$minWithdraw = Setting::where('key', 'withdraw.min')->first();
		
		if($minWithdraw && $data['amount'] < $minWithdraw->value) {
			throw new HandledException("提取金额不可少过" . $minWithdraw->value . "。");
		}
		
		if($data['amount'] > $user->totalBalance) {
			throw new HandledException("余额不足。");
		}
		
		return $user->withdrawRecords()->create([
			'amount' => $data['amount'],
			'status' => "",
			'remark' => null,
		]);
	}
}
