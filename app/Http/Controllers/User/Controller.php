<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller as BaseController;
use Auth;

class Controller extends BaseController
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }
	
	public function getUser() {
		return Auth::user();
	}
}
