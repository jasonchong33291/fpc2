<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\AdminRole;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;
use Hash;

class StaffController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('admins.staffs.listing'
			, ['record_count' => Admin::count(), 'roles' => AdminRole::all()]);
    }

	private function preDataset($item) {
		$item->role;
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		return $item;
	}
	
	private function validateDataset($data, $existingId) {
		$rules = [
			'name' => ["required", "max:255", "string"],
			// 'role_id' => ["required", "exists:admin_roles,id"],
            'username' => ['required', 'string', 'max:255', 'unique:users,username,' . $existingId],
            'password' => ['string', 'min:8', 'confirmed'],
		];
		if(!$existingId) {
			array_push($rules['password'], 'required');
		} else {
			array_push($rules['password'], 'nullable');
		}
		$validator = Validator::make($data, $rules);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$listing = Admin::with('role')->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Admin::create([
						'name' => $data['name'],
						'role_id' => $data['role_id'],
						'username' => $data['username'],
						'password' => Hash::make($data['password']),
					]);
					
					array_push($res->data, $this->preDataset($item));
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Admin::find($id);
					
					if($item) {
						$uData = [
							'name' => $data['name'],
							'role_id' => $data['role_id'],
							'username' => $data['username'],
						];
						if(isset($data['password'])) {
							$uData['password'] = Hash::make($data['password']);
						}
						$item->update($uData);					
											
						array_push($res->data, $this->preDataset($item));
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {	
				$item = Admin::find($id);
				
				if($item) {
					$item->delete();
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
	
	public function api() {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$datas = Request::input('ids');
			$users = [];
			DB::beginTransaction();
			
			foreach($datas as $id) {	
				$item = Admin::find($id);
				
				if($item) {
					$token = \Str::random(60);
					$item->api_token = hash('sha256', $token);
					$item->save();
					$item->api_token = $token;
					array_push($users, $item->only(['name', 'api_token']));
				}
			}
			
			DB::commit();
			$res->users = $users;
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
	}
}
