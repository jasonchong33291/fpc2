<?php

namespace App\Http\Controllers\Admin;

use App\Models\Admin;
use App\Models\User;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;
use Hash;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = parent::getUser();
		if($user->role) {
			if($user->role->regional) {
				$users = User::where('admin_id', $user->id)->get();
			} else {
				$users = User::all();
			}
		} else {
			return redirect()->route('admin.dashboard')->withErrors(['message' => "工作范围外。"]);
		}
		return view('admins.users.listing', ['user' => $user, 'users' => $users, 'admins' => Admin::all()]);
    }

	private function preDataset($user, $item) {
		$item->inviter;
		if($user->role && !$user->role->regional) {
			$item->admin;
			$item->last_login_ip_text = $item->last_login_ip;
		}
		return $this->dataset($user, $item);
	}
	
	private function dataset($user, $item) {
		if($user->role && !$user->role->regional) {
			$item->last_login_ip_text = $item->last_login_ip;
		}
		return $item;
	}
	
	private function validateDataset($data, $existingId) {
		$rules = [
			'inviter_id' => ["nullable", "exists:users,id"],
			'name' => ["required", "max:255", "string"],
            'username' => ['required', 'string', 'max:255', 'unique:users,username,' . $existingId],
            'password' => ['string', 'min:8', 'confirmed'],
		];
		if(!$existingId) {
			array_push($rules['password'], 'required');
		} else {
			array_push($rules['password'], 'nullable');
		}
		$validator = Validator::make($data, $rules);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$user = parent::getUser();
			if($user->role) {
				if($user->role->regional) {
					$query = User::withTrashed()->where('admin_id', $user->id);
				} else {
					$query = User::withTrashed();
				}
			} else {
				throw new Exception("工作范围外。");
			}
			$withs = ['inviter'];
			if($user->role && !$user->role->regional) {
				array_push($withs, 'admin');
			}
			$listing = $query->with($withs)->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($user, $item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {			
			$user = parent::getUser();			
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$rawPassword = $data['password'];
					$data["password"] = Hash::make($rawPassword);
					$data['invitation_code'] = "";
					$data['real_name'] = "";
					$data['withdraw_pin'] = "";
					$data['card_number'] = "";
					$data['admin_id'] = $user->id;
					$item = User::create($data);
					$item->raw_password = $rawPassword;
					
					array_push($res->data, $this->preDataset($user, $item));
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$user = parent::getUser();
			if($user->role) {
				if($user->role->regional) {
					$query = User::withTrashed()->where('admin_id', $user->id);
				} else {
					$query = User::withTrashed();
				}
			} else {
				throw new Exception("工作范围外。");
			}
			
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = $query->find($id);
					
					if($item) {
						$data['invitation_code'] = "";
						$data['real_name'] = "";
						$data['withdraw_pin'] = "";
						$data['card_number'] = "";
						$rawPassword = null;
						if(isset($data['password'])) {
							$rawPassword = $data['password'];
							$data["password"] = Hash::make($rawPassword);
						} else {
							unset($data['password']);
						}
						$item->update($data);	
						$item->raw_password = $rawPassword;
											
						array_push($res->data, $this->preDataset($user, $item));
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$user = parent::getUser();
			if($user->role) {
				if($user->role->regional) {
					$query = User::withTrashed()->where('admin_id', $user->id);
				} else {
					$query = User::withTrashed();
				}
			} else {
				throw new Exception("工作范围外。");
			}
			
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {	
				$item = $query->find($id);
				
				if($item) {
					if($item->deleted_at) {
						$item->restore();
					} else {
						$item->delete();
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
}
