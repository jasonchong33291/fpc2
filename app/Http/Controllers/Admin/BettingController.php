<?php

namespace App\Http\Controllers\Admin;

use App\Exceptions\HandledException;
use App\Models\Betting;
use App\Models\Room;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;
use Carbon\Carbon;

class BettingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('admins.bettings.listing', ['dice_min' => Betting::DICE_MIN, 'dice_max' => Betting::DICE_MAX
			, 'dice_count' => Betting::DICE_COUNT, 'dice_name' => Betting::DICE_NAME
			, 'record_count' => Betting::count(), 'rooms' => Room::all(), 'room_id' => Request::input('room_id')
			, 'fpc_order' => Betting::FPC_ORDER]);
    }

	public function random()
    {
		$ids = Request::input('ids');
		$result = [];
		foreach($ids as $id) {
			$betting = Betting::find($id);
			if(!$betting) {
				throw new HandledException("投注不存在，请尝试刷新页面。");
			}
			if($betting->reward) {
				throw new HandledException("投注已结算，彩球不可改变。");
			}
			$betting->results()->delete();
			array_push($result, $betting->endBetting());
		}
		return $result;
    }	

	public function deliver()
    {
		$ids = Request::input('ids');
		$result = [];
		foreach($ids as $id) {
			$betting = Betting::find($id);
			if(!$betting) {
				throw new HandledException("投注不存在，请尝试刷新页面。");
			}
			if($betting->total === null) {
				throw new HandledException("投注尚未生成彩球，不可结算。");
			}
			if($betting->ended_at > Carbon::now()) {
				throw new HandledException("投注尚未结束，不可结算。");
			}
			array_push($result, $betting->deliverReward());
		}
		return $result;
    }

	private function preDataset($item) {
		$item->unique_user_count = $item->uniqueUsers()->count();
		$item->record_count = $item->records()->count();
		$item->room_name = $item->room ? $item->room->name : null;
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		$item->betting_results = $item->results->pluck('value');
		return $item;
	}
	
	private function validateDataset($data, $existingId) {
		$rules = [];
		for($i = 1; $i <= Betting::DICE_COUNT; $i++) {
			$rules[Betting::DICE_NAME . $i] = ["required", "numeric", "min:" . Betting::DICE_MIN, "max:" . Betting::DICE_MAX];
		}
		$validator = Validator::make($data, $rules);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
	private function checkColumn($query, $index, $sort, $search) {
		// join
		$query->join("rooms", 'bettings.room_id', '=', 'rooms.id')
			// ->leftJoin("results",  function($q) {
				// Betting::leftJoin('betting_results', 'bettings.id', '=', 'betting_results.betting_id')
				// ->selectRaw("count(betting_results.id) as court");
			// })
			->leftJoin('betting_records', 'bettings.id', '=', 'betting_records.betting_id')
			->leftJoin('games', 'games.id', '=', 'rooms.game_id');
		// pre-sort
		switch($index) {
			case 0:
				// betting.room.name
				$query->orderBy("room_name", $sort);
				break;
			case 1:
				// betting.version
				$query->orderBy("bettings.version", $sort);
				break;
			case 2:
				// betting.results
				$query->orderBy("bettings.id", $sort);
				break;
			case 3:
				// records.length
				$query->orderBy("record_count", $sort);
				break;
			case 4:
				// unique_users.length
				$query->orderBy("unique_user_count", $sort);
				break;
			case 5:
				// out_sum
				$query->orderBy("out_sum", $sort);
				break;
			case 6:
				// in_sum
				$query->orderBy("in_sum", $sort);
				break;
			case 7:
				// ended_at
				$query->orderBy("bettings.ended_at", $sort);
				break;
			default:
				//
		}
		// select
		$query
		->select(['rooms.name as room_name', "bettings.id", "bettings.version"
			, 'bettings.reward', 'bettings.ended_at', 'games.key as game_key'])
		->selectRaw("count(betting_records.id) as record_count")
		->selectRaw("count(distinct(betting_records.user_id)) as unique_user_count")
		->selectRaw("sum(betting_records.amount) as in_sum")
		->selectRaw("sum(betting_records.won) as out_sum")
		->groupBy(['room_name', "bettings.id", "bettings.version"
			, 'bettings.reward', 'bettings.ended_at', 'games.key']);
		// pre-filter
		// $query->dd();
		if($search) {
			return $query->having('version', 'like', '%' . $search . '%')
			->orHaving('reward', 'like', '%' . $search . '%')
			->orHaving('ended_at', 'like', '%' . $search . '%')
			->orHaving('room_name', 'like', '%' . $search . '%')
			->orHaving('record_count', 'like', '%' . $search . '%')
			->orHaving('unique_user_count', 'like', '%' . $search . '%')
			->orHaving('in_sum', 'like', '%' . $search . '%')
			->orHaving('out_sum', 'like', '%' . $search . '%');
		} else {		
			return $query;
		}
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$res->draw = Request::input('draw');
			$offset = Request::input('start', 0);
			$limit = Request::input('length', 10);
			$order = Request::input('order', [["column" => 0, "dir" => "asc"]])[0];
			$search = Request::input('search');
			$query = Betting::query();
			if($roomId = Request::input('room_id')) {
				$query->where('room_id', $roomId);
			}
			$res->recordsTotal = $query->count();			
			$query = $this->checkColumn($query
				, $order["column"], $order["dir"], $search['value']);
			$res->recordsFiltered = $search['value'] ? $query->count() : $res->recordsTotal;		
			$filtered = $query->offset($offset)->limit($limit)->get();
			foreach($filtered as $item) {
				array_push($res->data, $this->dataset($item));
			}			
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Betting::create($data);
					
					array_push($res->data, $this->preDataset($item));
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Betting::find($id);					
					if(!$item) {
						throw new HandledException("投注不存在，请尝试刷新页面。");
					}
					if($item->reward) {
						throw new HandledException("投注已结算，彩球不可改变。");
					}					
					if($item) {
						$item->update($data);					
											
						array_push($res->data, $this->preDataset($item));
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {	
				$item = Betting::find($id);
				
				if($item) {
					$item->delete();
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
}
