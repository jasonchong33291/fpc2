<?php

namespace App\Http\Controllers\Admin;

use App\Models\Betting;
use App\Models\BettingRecord;
use App\Models\Room;
use App\Models\User;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;

class BettingRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = parent::getUser();
		$users = null;
		if($user->role) {
			if($user->role->regional) {
				$users = User::where('admin_id', $user->id)->pluck('id');
			}
		}
		$query = BettingRecord::query();
		if($users) {
			$query->whereIn('user_id', $users);
		}
		return view('admins.bettings.records.listing', ['record_count' => $query->count(), 'rooms' => Room::all(),
			'fpc_order' => Betting::FPC_ORDER]);
    }
	
    public function index4d()
    {
		$user = parent::getUser();
		$users = null;
		if($user->role) {
			if($user->role->regional) {
				$users = User::where('admin_id', $user->id)->pluck('id');
			}
		}
		$query = BettingRecord::query();
		if($users) {
			$query->whereIn('user_id', $users);
		}
		return view('admins.bettings.records.4d-listing', ['record_count' => $query->count()]);
    }

	private function preDataset($item) {
		$item->user_name = $item->user ? $item->user->name : null;
		$item->betting_version = $item->betting ? $item->betting->version : null;
		$item->room_name = null;
		if($item->betting && $item->betting->room) {
			$item->room_name = $item->betting->room->name;
		}
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		$item->betting_results = $item->betting ? $item->betting->results : null;
		unset($item->betting);
		$item->betting_rates = $item->rates()->pluck('rate');
		return $item;
	}
	
	private function validateDataset($data, $existingId) {
		$validator = Validator::make($data, [
			'name' => ["required", "max:255", "string"],
		]);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
	private function checkColumn($query, $index, $sort, $search) {
		// join
		$query->leftJoin("bettings", 'bettings.id', '=', 'betting_records.betting_id')
			->leftJoin("rooms", 'bettings.room_id', '=', 'rooms.id')
			->join("users", 'betting_records.user_id', '=', 'users.id');
		// pre-sort
		switch($index) {
			case 0:
				// user_name
				$query->orderBy("user_name", $sort);
				break;
			case 1:
				// room_name
				$query->orderBy("room_name", $sort);
				break;
			case 2:
				// betting_version
				$query->orderBy("betting_version", $sort);
				break;
			case 3:
				// amount
				$query->orderBy("betting_records.amount", $sort);
				break;
			case 4:
				// play_group
				$query->orderBy("betting_records.play_group", $sort);
				break;
			case 5:
				// play_option
				$query->orderBy("play_option", $sort);
				break;
			case 6:
				// rate
				break;
			case 7:
				// won
				$query->orderBy("betting_records.won", $sort);
				break;
			case 8:
				// betting_result
				$query->orderBy("betting_result_value", $sort);
				break;
			case 9:
				// result
				$query->orderBy("result_value", $sort);
				break;
			case 10:
				// created_at
				$query->orderBy("betting_records.created_at", $sort);
				break;
			default:
				//
		}
		// select
		$query
			->select(['betting_records.id', 'betting_records.amount', 'betting_records.won', 'betting_records.created_at'
				, 'betting_records.betting_id', 'rooms.name as room_name', 'users.name as user_name'
				, 'bettings.version as betting_version', 'betting_records.ball'
				, 'betting_records.play_name', 'betting_records.play_group'
				, 'betting_records.play_type', 'betting_records.play_option'
				, 'betting_records.next_counter'])
			->selectRaw("if(betting_records.won is null, '未开奖', if(betting_records.won > 0, '中奖', '没中')) as result_value");
		// pre-filter
		if($search) {
			return $query->where('betting_records.amount', 'like', '%' . $search . '%')
			->orWhere('betting_records.won', 'like', '%' . $search . '%')
			->orWhere('betting_records.play_name', 'like', '%' . $search . '%')
			->orWhere('betting_records.play_option', 'like', '%' . $search . '%')
			->orWhere('betting_records.created_at', 'like', '%' . $search . '%')
			->orWhere('rooms.name', 'like', '%' . $search . '%')
			->orWhere('users.name', 'like', '%' . $search . '%')
			->orWhere('bettings.version', 'like', '%' . $search . '%');
			// ->orWhere('betting_result_value', 'like', '%' . $search . '%')
			// ->orWhere('result_value', 'like', '%' . $search . '%');
		} else {		
			return $query;
		}
	}
	
	
	
	private function checkColumn4d($query, $index, $sort, $search) {
		// join
		$query->leftJoin("bettings", 'bettings.id', '=', 'betting_records.betting_id')
			->leftJoin("rooms", 'bettings.room_id', '=', 'rooms.id')
			->join("users", 'betting_records.user_id', '=', 'users.id');
		// pre-sort
		switch($index) {
			case 0:
				// user_name
				$query->orderBy("user_name", $sort);
				break;
			case 1:
				// betting_version
				$query->orderBy("betting_version", $sort);
				break;
			case 2:
				// amount
				$query->orderBy("betting_records.amount", $sort);
				break;
			case 3:
				// play_group
				$query->orderBy("betting_records.play_group", $sort);
				break;
			case 4:
				// play_option
				$query->orderBy("play_option", $sort);
				break;
			case 5:
				// won
				$query->orderBy("betting_records.won", $sort);
				break;
			case 6:
				// betting_result
				$query->orderBy("betting_result_value", $sort);
				break;
			case 7:
				// result
				$query->orderBy("result_value", $sort);
				break;
			case 8:
				// created_at
				$query->orderBy("betting_records.created_at", $sort);
				break;
			default:
				//
		}
		// select
		$query
			->select(['betting_records.id', 'betting_records.amount', 'betting_records.won', 'betting_records.created_at'
				, 'betting_records.betting_id', 'rooms.name as room_name', 'users.name as user_name'
				, 'bettings.version as betting_version', 'betting_records.ball'
				, 'betting_records.play_name', 'betting_records.play_group'
				, 'betting_records.play_type', 'betting_records.play_option'
				, 'betting_records.next_counter'])
			->selectRaw("if(betting_records.won is null, '未开奖', if(betting_records.won > 0, '中奖', '没中')) as result_value");
		// pre-filter
		if($search) {
			return $query->where('betting_records.amount', 'like', '%' . $search . '%')
			->orWhere('betting_records.won', 'like', '%' . $search . '%')
			->orWhere('betting_records.play_name', 'like', '%' . $search . '%')
			->orWhere('betting_records.play_option', 'like', '%' . $search . '%')
			->orWhere('betting_records.created_at', 'like', '%' . $search . '%')
			->orWhere('rooms.name', 'like', '%' . $search . '%')
			->orWhere('users.name', 'like', '%' . $search . '%')
			->orWhere('bettings.version', 'like', '%' . $search . '%');
			// ->orWhere('betting_result_value', 'like', '%' . $search . '%')
			// ->orWhere('result_value', 'like', '%' . $search . '%');
		} else {		
			return $query;
		}
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$user = parent::getUser();
			$users = null;
			if($user->role) {
				if($user->role->regional) {
					$users = User::where('admin_id', $user->id)->pluck('id');
				}
			}
			$offset = Request::input('start', 0);
			$limit = Request::input('length', 10);
			$order = Request::input('order', [["column" => 0, "dir" => "asc"]])[0];
			$search = Request::input('search');			
			
			$query = BettingRecord::query()
				->where(function($q) {
					$q->whereNotIn('bettings.room_id', [3, 4]);
				});
			if($users) {
				$query->whereIn('user_id', $users);
			}
			if($roomId = Request::input('room_id')) {
				$query->where('room_id', $roomId);
			}
			if($result = Request::input('result')) {
				$query->whereRaw("if(betting_records.won is null, '未开奖', if(betting_records.won > 0, '中奖', '没中')) = '" . $result . "'");
			}
			$query = $this->checkColumn($query
				, $order["column"], $order["dir"], $search['value']);
			
			$res->draw = Request::input('draw');
			$res->recordsTotal = $query->count();
			$res->recordsFiltered = $search['value'] ? $query->count() : $res->recordsTotal;		
			
			$filtered = $query->offset($offset)->limit($limit)->get();
			foreach($filtered as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
        return response()->json($res);
    }
	
    public function listing4d()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$user = parent::getUser();
			$users = null;
			if($user->role) {
				if($user->role->regional) {
					$users = User::where('admin_id', $user->id)->pluck('id');
				}
			}
			$offset = Request::input('start', 0);
			$limit = Request::input('length', 10);
			$order = Request::input('order', [["column" => 0, "dir" => "asc"]])[0];
			$search = Request::input('search');			
			
			$query = BettingRecord::query()
				->where(function($q) {
					$q->whereIn('bettings.room_id', [3, 4]);
				});
				
			if($users) {
				$query->whereIn('user_id', $users);
			}
			if($result = Request::input('result')) {
				$query->whereRaw("if(betting_records.won is null, '未开奖', if(betting_records.won > 0, '中奖', '没中')) = '" . $result . "'");
			}
			$query = $this->checkColumn4d($query
				, $order["column"], $order["dir"], $search['value']);
			$res->draw = Request::input('draw');
			$res->recordsTotal = $query->count();
			$res->recordsFiltered = $search['value'] ? $query->count() : $res->recordsTotal;		
			
			$filtered = $query->offset($offset)->limit($limit)->get();
			foreach($filtered as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
        return response()->json($res);
    }
}
