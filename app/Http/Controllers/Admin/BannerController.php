<?php

namespace App\Http\Controllers\Admin;

use App\Models\Banner;
use App\Models\BannerType;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;
use Storage;

class BannerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('admins.banners.listing', ['record_count' => Banner::count(), 'types' => BannerType::all()]);
    }

	private function preDataset($item) {
		$item->type;
		if ($item->show_caption == "0") {
			$item->show_caption = false;
		}
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		return $item;
	}
	
	private function fileDataset($item) {
		if($item->url && Storage::disk('public')->exists($item->url)) {
			$file = new stdClass;
			$file->id = $item->url;
			$file->filename = $item->url;
			$file->filesize = Storage::disk('public')->size($item->url);
			$file->path = Storage::disk('public')->url($item->url);
			return $file;
		} else {
			return null;
		}
	}
	
	private function validateDataset($data, $existingId) {
		$rules = [
			'caption' => ["required", "max:255", "string"],
			'description' => ["nullable", "max:8000", "string"],
			"type_id" => ["required"],
			'url' => ["required", "max:8000", "string"],
			'show_caption' => ["boolean"]
		];
		$validator = Validator::make($data, $rules);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$listing = Banner::with(['type'])->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($item));
				if($file = $this->fileDataset($item)) {
					$res->files['images'][$file->id] = $file;
				}
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$res = new stdClass;
		$res->data = [];
		$res->files = [];
		$res->files['icons'] = [];
		$res->fieldErrors = [];
		
		try {
			$user = parent::getUser();
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Banner::create($data);					
					array_push($res->data, $this->preDataset($item));
					if($file = $this->fileDataset($item)) {
						$res->files['images'][$file->id] = $file;
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->files = [];
		$res->files['icons'] = [];
		$res->fieldErrors = [];
		
		try {
			$user = parent::getUser();
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Banner::find($id);
					
					if($item) {
						$item->update($data);
						array_push($res->data, $this->preDataset($item));
						if($file = $this->fileDataset($item)) {
							$res->files['images'][$file->id] = $file;
						}
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$item = Banner::trashed()->find($id);
				if ($item) {
					if ($item->deleted_at) {
						$item->forceDelete();
					} else {
						$item->delete();
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }	
	
	private function validateFile($data) {
		$rules = [			
			'upload' => ['image', 'max:10240', 'required']
		];
		
		$validator = Validator::make($data, $rules);
				
		return $validator;
	}
	
	public function upload() {
		$res = new stdClass();
		
		try {
			$data = Request::all();

			$res->fieldErrors = [];
			
			$validator = $this->validateFile($data);
			
			if($validator->errors()->count() > 0) {
				$error = new stdClass;
				$error->name = "path";
				$error->status = $validator->errors()->get("upload")->first();
				
				array_push($res->fieldErrors, $error);
			} else {
				$upload = $data['upload'];
				
				$fileName = $upload->getClientOriginalName();
				$path = Storage::disk('public')->putFileAs('images', $upload, $fileName);
				
				$item = new stdClass;
				$item->url = $path;
				if($file = $this->fileDataset($item)) {
					$res->files = [];
					$res->files['images'] = [];
					$res->files['images'][$file->id] = $file;
					
					$res->upload = [];
					$res->upload['id'] = $path;
				}
			}
		} catch(Exception $ex) {
			throw $ex;
			$res->error = $ex->getMessage();
		}
		
		return response()->json($res);
	}
}
