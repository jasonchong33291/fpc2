<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\InvitationRecord;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;

class InvitationRecordController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {		
		$user = parent::getUser();
		if($user->role) {
			if($user->role->regional) {
				$users = User::where('admin_id', $user->id)->pluck('id');
			} else {
				$users = User::pluck('id');
			}
		} else {
			$users = [];
		}
		return view('admins.invitations.records.listing'
			, ['record_count' => InvitationRecord::whereIn('inviter_id', $users)->count()]);
    }

	private function preDataset($item) {
		$item->user;
		if($item->betting) {
			$item->betting->room;
		}
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		return $item;
	}
	
	private function validateDataset($data, $existingId) {
		$validator = Validator::make($data, [
			'name' => ["required", "max:255", "string"],
		]);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$user = parent::getUser();
			if($user->role) {
				if($user->role->regional) {
					$users = User::where('admin_id', $user->id)->pluck('id');
				} else {
					$users = User::pluck('id');
				}
			} else {
				$users = [];
			}
			$listing = InvitationRecord::with(['inviter', 'invited'])->whereIn('inviter_id', $users)->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = InvitationRecord::create($data);
					
					array_push($res->data, $this->preDataset($item));
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = InvitationRecord::find($id);
					
					if($item) {
						$item->update($data);					
											
						array_push($res->data, $this->preDataset($item));
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {	
				$item = InvitationRecord::find($id);
				
				if($item) {
					$item->delete();
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
}
