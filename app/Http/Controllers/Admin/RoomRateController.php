<?php

namespace App\Http\Controllers\Admin;

use App\Models\Betting;
use App\Models\Game;
use App\Models\Room;
use App\Models\RoomRate;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;

class RoomRateController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
		return view('admins.rooms.rates.listing', ['record_count' => RoomRate::count()
			, 'room' => Room::find($id)]);
    }

	private function preDataset($item) {
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		return $item;
	}
	
	private function validateDataset($data, $existingId) {
		$rules = [
			'name' => ["required", "max:255", "string"],
			'rate' => ["nullable", "numeric", "max:9999999999.99"],
		];
		
		$validator = Validator::make($data, $rules);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$listing = RoomRate::with(['group'])->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = RoomRate::find($id);
					
					if($item) {
						$item->update($data);
						array_push($res->data, $this->preDataset($item));
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
}
