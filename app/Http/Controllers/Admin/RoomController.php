<?php

namespace App\Http\Controllers\Admin;

use App\Models\Betting;
use App\Models\Game;
use App\Models\Room;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;
use Storage;

class RoomController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('admins.rooms.listing', ['record_count' => Room::count()
			, 'rates' => Betting::DICE_RATE, 'games' => Game::all()]);
    }

	private function preDataset($item) {
		$item->rates;
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		$item->total_base_users = $item->base_user_count;
		$item->total_unique_users = $item->total_unique_users;
		return $item;
	}
	
	private function fileDataset($item, $field) {
		if($item->{$field}) {
			$file = new stdClass;
			$file->id = $item->{$field};
			$file->filename = $item->{$field};
			$file->path = $item->{$field};
			return $file;
		}
		return null;
	}
	
	private function validateDataset($data, $existingId) {
		$rules = [
			'name' => ["required", "max:255", "string"],
			'base_user_count' => ["nullable", "numeric"],
			'entrance' => ["nullable", "numeric", "max:9999999999.99"],
			'min' => ["nullable", "numeric", "max:9999999999.99"],
			'max' => ["nullable", "numeric", "max:9999999999.99"],
			'status' => ["required"]
		];
		
		$validator = Validator::make($data, $rules);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		$res->files = [];
		
		try {
			$listing = Room::with(['rates', 'game'])->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($item));
				if($file = $this->fileDataset($item, "icon_url")) {
					$res->files['icons'][$file->id] = $file;
				}
				if($file = $this->fileDataset($item, "image_url")) {
					$res->files['images'][$file->id] = $file;
				}
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Room::create($data);					
					array_push($res->data, $this->preDataset($item));
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = Room::find($id);
					
					if($item) {
						$item->update($data);		
						array_push($res->data, $this->preDataset($item));
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {	
				$item = Room::find($id);
				
				if($item) {
					$item->delete();
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
	
	private function validateFile($data) {
		$rules = [			
			'upload' => ['image', 'max:10240', 'required'],
			"uploadField" => ["required"]
		];
		
		$validator = Validator::make($data, $rules);
				
		return $validator;
	}
	
	public function upload() {
		$res = new stdClass();
		
		try {
			$data = Request::all();

			$res->fieldErrors = [];
			
			$validator = $this->validateFile($data);
			
			if($validator->errors()->count() > 0) {
				$error = new stdClass;
				$error->name = $data["uploadField"];
				$error->status = $validator->errors()->get("upload")->first();
				
				array_push($res->fieldErrors, $error);
			} else {
				$upload = $data['upload'];
				
				$fileName = $upload->getClientOriginalName();
				$path = Storage::disk('public')->putFileAs('images', $upload, $fileName);
				$item = new stdClass;
				$item->{$data["uploadField"]} = Storage::disk('public')->url($path);
				if($file = $this->fileDataset($item, $data["uploadField"])) {
					$res->files = [];
					if ($data["uploadField"] == "image_url") {
						$res->files['images'] = [];
						$res->files['images'][$file->id] = $file;
					} else if ($data["uploadField"] == "icon_url") {
						$res->files['icons'] = [];
						$res->files['icons'][$file->id] = $file;
					}
					$res->upload = [];
					$res->upload['id'] = $item->{$data["uploadField"]};
				}
			}
		} catch(Exception $ex) {
			throw $ex;
			$res->error = $ex->getMessage();
		}
		
		return response()->json($res);
	}
}
