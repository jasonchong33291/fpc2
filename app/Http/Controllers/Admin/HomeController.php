<?php

namespace App\Http\Controllers\Admin;

use App\Models\BettingRecord;
use App\Models\Room;
use App\Models\TopupRecord;
use App\Models\User;
use App\Models\WithdrawRecord;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Collection;

class HomeController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
		Carbon::setLocale('zh');
		$user = parent::getUser();
		$today = Carbon::today();
		$topupRecords;
		if($user->role) {
			$prev = $today->clone()->setMonth($today->month - 1);
			if($user->role->regional) {
				$query = User::where('admin_id', $user->id);
				$users = $query->get();
				$rooms = null;
				$bettingRecords = BettingRecord::orderByDesc('created_at')->whereIn('user_id', $users->pluck('id'))->limit(10)->get();
				$prevTotalEarn = TopupRecord::whereIn('user_id', $users->pluck('id'))
				->whereYear('created_at', $prev->year)
				->whereMonth('created_at', $prev->month)
				->where('status', '充值完成')->sum('amount') - 
				WithdrawRecord::whereIn('user_id', $users->pluck('id'))
				->whereYear('created_at', $prev->year)
				->whereMonth('created_at', $prev->month)
				->where('status', '提取完成')->sum('amount');
				$topupRecords = TopupRecord::whereIn('user_id', $users->pluck('id'))
				->whereYear('created_at', $today->year)
				->whereMonth('created_at', $today->month)
				->where('status', '充值完成')->orderByDesc('created_at')->get();
				$monthlyTotalTopup = $topupRecords->sum('amount');
				$monthlyTotalWithdraw = WithdrawRecord::whereIn('user_id', $users->pluck('id'))
				->whereYear('created_at', $today->year)
				->whereMonth('created_at', $today->month)
				->where('status', '提取完成')->sum('amount');
			} else {
				$query = User::query();
				$users = User::all();
				$rooms = Room::all()->sortByDesc(function($query) use ($today) {
					return $query->bettingRecords()
					->whereYear('betting_records.created_at', $today->year)
					->whereMonth('betting_records.created_at', $today->month)
					->count();
				})->take(10);
				$bettingRecords = BettingRecord::orderByDesc('created_at')->limit(10)->get();
				$prevTotalEarn = TopupRecord::whereYear('created_at', $prev->year)
				->whereMonth('created_at', $prev->month)
				->where('status', '充值完成')->sum('amount') - 
				WithdrawRecord::whereYear('created_at', $prev->year)
				->whereMonth('created_at', $prev->month)
				->where('status', '提取完成')->sum('amount');
				$topupRecords = TopupRecord::whereYear('created_at', $today->year)
				->whereMonth('created_at', $today->month)
				->where('status', '充值完成')->orderByDesc('created_at')->get();
				$monthlyTotalTopup = $topupRecords ->sum('amount');
				$monthlyTotalWithdraw = WithdrawRecord::whereYear('created_at', $today->year)
				->whereMonth('created_at', $today->month)
				->where('status', '提取完成')->sum('amount');
			}
		} else {
			$topupRecords = null;
			$bettingRecords = null;
			$query = null;
			$users = new Collection();
			$rooms = null;
			$prevTotalEarn = 0;
			$monthlyTotalTopup = 0;
			$monthlyTotalWithdraw = 0;
		}
        return view('admins.home', ['user' => $user, 'user_count' => $users->count(), 'today' => $today
			, 'topup_records' => $topupRecords
			, 'betting_records' => $bettingRecords
			, 'rooms' => $rooms
			, 'users' => $query ? $query->leftJoin('topup_records', 'topup_records.user_id', 'users.id', function($tQuery) {
					return $tQuery->whereYear('topup_records.created_at', $today->year)
					->whereMonth('topup_records.created_at', $today->month)
					->where('topup_records.status', '充值完成');
				})->groupBy(['users.id', 'users.name', 'users.won', 'users.balance'])
				->select(['users.id', 'users.name', 'users.won', 'users.balance'])
				->selectRaw('sum(topup_records.amount) as total_topup')
				->orderByDesc("total_topup")
				->limit(10)
				->get() : $users->take(10)
			, 'prev_total_earn' => $prevTotalEarn
			, 'monthly_total_topup' => $monthlyTotalTopup
			, 'monthly_total_withdraw' => $monthlyTotalWithdraw]);
    }
}
