<?php

namespace App\Http\Controllers\Admin;

use App\Models\Betting;
use App\Models\BettingRecord;
use App\Models\Setting;
use App\Models\SettingTemplate;
use Artisan;
use Request;
use Exception;

class SettingController extends Controller
{
    public function index() {
		return view('admins.settings.listing', [
			'setting_template_list' => SettingTemplate::all(),
			'setting_list' => Setting::all()
		]);
	}
	
	private function checkAccess() {
		if(Request::input('access') == 'yykdev') {
			return true;
		} else {
			throw new Exception("主钥匙不正确。");
		}
	}
	
	public function update() {
		try {
			$key = Request::input("key");
			$value = Request::input("value") ?? "";
			
			switch($key) {
				case 'open_betting':
					if($this->checkAccess()) {
						return response(Artisan::call('betting:open'));
					}
				case 'generate_betting':
					if($this->checkAccess()) {
						return response(Artisan::call('betting:generate'));
					}
				case 'flush_betting':
					if($this->checkAccess()) {
						Betting::query()->delete();
						return response("彩票已全部删除。");
					}
				case 'flush_betting_record':
					if($this->checkAccess()) {
						BettingRecord::query()->delete();
						return response("下注记录已全部删除。");
					}
				default:
					$setting = Setting::where('key', $key)->first();
					
					if($setting) {
						$setting->update([
							'value' => htmlspecialchars($value, ENT_HTML5)
						]);
					} else {
						$setting = Setting::create([
							'key' => htmlspecialchars($key, ENT_HTML5),
							'value' => htmlspecialchars($value, ENT_HTML5)
						]);
					}		
					return response("");
			}
		} catch(Exception $ex) {
			abort(420, $ex->getMessage());
		}
	}
}