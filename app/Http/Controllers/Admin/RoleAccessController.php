<?php

namespace App\Http\Controllers\Admin;

use App\Models\RoleAccess;
use DB;
use Exception;
use Request;
use Validator;
use stdClass;
use Hash;

class RoleAccessController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return view('admins.roles.accesses.listing');
    }

	private function preDataset($item) {
		$item->roles;
		return $this->dataset($item);
	}
	
	private function dataset($item) {
		return $item;
	}
	
	private function validateDataset($data, $existingId) {
		$rules = [
			'name' => ["required", "max:255", "string"],
			'available' => ["required", "boolean"],
		];
		$validator = Validator::make($data, $rules);
		
		$errors = [];
		
		foreach($validator->errors()->getMessages() as $key => $value) {
			$error = new stdClass;
			$error->name = $key;
			$error->status = $value;
			
			array_push($errors, $error);
		}
		
		return $errors;
	}
	
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function listing()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$listing = RoleAccess::with('roles')->get();
			
			foreach($listing as $item) {
				array_push($res->data, $this->dataset($item));
			}
		} catch(Exception $ex) {
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = RoleAccess::create($data);
					
					array_push($res->data, $this->preDataset($item));
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
		$res = new stdClass;
		$res->data = [];
		$res->fieldErrors = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {
				$errors = $this->validateDataset($data, $id);
				
				if(count($errors) > 0) {
					$res->fieldErrors = array_merge($res->fieldErrors, $errors);
				} else {
					$item = RoleAccess::find($id);
					
					if($item) {
						$item->update($data);					
											
						array_push($res->data, $this->preDataset($item));
					}
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy()
    {
		$res = new stdClass;
		$res->data = [];
		
		try {
			$datas = Request::input('data');
			
			DB::beginTransaction();
			
			foreach($datas as $id => $data) {	
				$item = RoleAccess::find($id);
				
				if($item) {
					$item->delete();
				}
			}
			
			DB::commit();
		} catch(Exception $ex) {
			DB::rollback();
			$res->error = $ex->getMessage();
		}
		
        return response()->json($res);
    }
}
