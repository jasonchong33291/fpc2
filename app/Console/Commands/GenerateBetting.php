<?php

namespace App\Console\Commands;

use App\Models\Room;
use App\Models\Betting;
use App\Models\Setting;
use Carbon\Carbon;
use Exception;
use Illuminate\Console\Command;

class GenerateBetting extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'betting:generate {room?* : room id} {--detail : show detail log}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Betting Generate';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {
		try {
			$showDetail = $this->option('detail') ? true : false;
			$roomIds = $this->argument('room') ? : false;
			if ($roomIds) {
				$rooms = Room::whereIn('id', $roomIds)->get();
			} else {
				$rooms = Room::all();
			}
			$now = Carbon::now()
				->minute(0)
				->second(0);
			// 8 hour x 60 minute per 30 sec
			echo "彩票生成中，时间" . $now->toString() . "\n";
			foreach($rooms as $room) {
				if ($room->is_standalone) {
					// skip
				} else if ($room->is_api) {
					// generate for 24 hour
					$count = 0;
					// 8 hour, 60 minute, 60 second
					$total = 24;
					$nextEndDate = $now->clone()
						->setHour(1);
					for($i = 0; $i < $total; $i++) {
						$version = str_pad($nextEndDate->hour, 2, "0", STR_PAD_LEFT)
							. ":" . str_pad($nextEndDate->minute, 2, "0", STR_PAD_LEFT);
						if($showDetail) {
							echo $room->name . "生成第" . $version . "期彩票";
							echo "，结束时间为" . $nextEndDate->toString() . "。\n";
						}
						$exist = $room->bettings()->where("ended_at", $nextEndDate)->first();
						if (!$exist) {
							$room->bettings()->create([
								"ended_at" => $nextEndDate,
								"version" => $version
							]);
							$count++;
						}
						$nextEndDate->addHour();
					}
					echo "房间" . $room->name . ", 已生成" . $count . "期彩票\n";
				} else {
					$count = 0;
					$secondInterval = $room->interval ? : 30;
					// 8 hour, 60 minute, 60 second
					$total = 8 * 60 * 60 / $secondInterval;
					$nextEndDate = $now->clone();
					for($i = 0; $i < $total; $i++) {
						if ($secondInterval >= 60) {
							$version = str_pad($nextEndDate->hour, 2, "0", STR_PAD_LEFT)
								. ":" . str_pad($nextEndDate->minute, 2, "0", STR_PAD_LEFT);
						} else {
							$version = str_pad($nextEndDate->hour, 2, "0", STR_PAD_LEFT)
								. ":" . str_pad($nextEndDate->minute, 2, "0", STR_PAD_LEFT)
								. ":" . str_pad($nextEndDate->second, 2, "0", STR_PAD_LEFT);
						}
						if($showDetail) {
							echo $room->name . "生成第" . $version . "期彩票";
							echo "，结束时间为" . $nextEndDate->toString() . "。\n";
						}
						$exist = $room->bettings()->where("ended_at", $nextEndDate)->first();
						if (!$exist) {
							$room->bettings()->create([
								"ended_at" => $nextEndDate,
								"version" => $version
							]);
							$count++;
						}
						$nextEndDate->addSecond($secondInterval);
					}
					echo "房间" . $room->name . ", 已生成" . $count . "期彩票\n";
				}			
			}
			return 1;
		} catch (Exception $ex) {
			echo $ex->getMessage();
			return -1;
		}
    }
}
