<?php

namespace App\Console;

use App\Models\Setting;
use Artisan;
use Exception;
use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        //
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
		$openSetting = Setting::where("key", "cron.open")->first();
		$openCron = $openSetting ? $openSetting->value : "* * * * *";
		$schedule->call(function () {
			try {
				Artisan::call("betting:open");
			} catch (Exception $ex) {
				echo $ex->getMessage();
			}
        })
		->name("Open betting for per minute.")
		->cron($openCron);
		
		$generateSetting = Setting::where("key", "cron.generate")->first();
		$generateCron = $generateSetting ? $generateSetting->value : "0 */6 * * *";
		$schedule->call(function () {
			try {
				Artisan::call("betting:generate");
			} catch (Exception $ex) {
				echo $ex->getMessage();
			}
        })
		->name("Generate betting every 6 hour.")
		->cron($generateCron);
			
		$fetchSetting = Setting::where("key", "cron.fetch")->first();
		$fetchCron = $fetchSetting ? $fetchSetting->value : "* 16-0 * * *";
		$schedule->call(function () {
			try {
				// wait for 5 sec
				sleep(5);
				Artisan::call("betting:fetch");
			} catch (Exception $ex) {
				echo $ex->getMessage();
			}
        })
		->name("Fetch API every minute from 4pm to 12am.")
		->cron($fetchCron);
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
