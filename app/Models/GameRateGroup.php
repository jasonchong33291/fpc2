<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class GameRateGroup extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'name', "group"
    ];
	
	public function rates() {
		return $this->hasMany(GameRate::class, 'group_id');
	}
}
