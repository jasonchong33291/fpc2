<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Banner extends Model
{
    use HasFactory, SoftDeletes;

	protected $fillable = ["caption", "description", "type_id", "url", "show_caption"];
	
	public function type() {
		return $this->belongsTo(BannerType::class, "type_id");
	}
}
