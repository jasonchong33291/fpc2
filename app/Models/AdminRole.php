<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AdminRole extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'name', 'default_access', 'regional'
    ];
	
	protected $casts = [
		'default_access' => "boolean",
		'regional' => "boolean",
	];
	
	public function admins() {
		return $this->hasMany(Admin::class, 'role_id');
	}
	
	public function roleAccesses() {
		return $this->hasMany(AdminRoleAccess::class, "role_id");
	}

	public function accesses() {
		return $this->hasManyThrough(RoleAccess::class, AdminRoleAccess::class
			, "role_id", "id", "id", "access_id");
	}
}
