<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopupMethod extends Model
{
    use HasFactory;
	
	protected $fillable = [
		'name', 'icon_path', 'trigger_url', 'callback_url', 'api_key', 'secret_key'
	];
}
