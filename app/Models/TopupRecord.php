<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TopupRecord extends Model
{
    use HasFactory;
	
	const TOPUP = "充值完成";
	const REFUND = "已退款";
	
	protected $fillable = [
		'user_id', 'amount', 'method_id', 'status', 'approved_id', 'rejected_id', 'remark'
	];
	
	protected static function boot()
    {
        parent::boot();

        self::created(function($record){
			if($record->status == self::TOPUP) {
				$record->topup();
			}
        });
		
		self::updating(function($record){
			$status = $record->getOriginal('status');
			if($status != $record->status) {
				if($status == self::TOPUP) {
					$record->refund();
				} else if($record->status == self::TOPUP) {
					$record->topup();
				}
			}
        });
		
        self::deleting(function($record){
			if($record->status == self::TOPUP) {
				$record->refund();
			// } else if ($record->status == self::REFUND) {
				// $record->topup();
			}
        });
    }
	
	public function approved() {
		return $this->belongsTo(Admin::class, 'approved_id');
	}
	
	public function method() {
		return $this->belongsTo(TopupMethod::class, "method_id");
	}
	
	public function rejected() {
		return $this->belongsTo(Admin::class, 'rejected_id');
	}
	
	public function user() {
		return $this->belongsTo(User::class);
	}
	
	public function topup() {
		$user = $this->user;
		// if($this->amount >= 0) {
			$user->balance += $this->amount;
		// } else {
			// if($user->balance - $this->amount >= 0) {
				// $user->balance += $this->amount;
			// } else {
				// $remain = $user->won + $this->amount;
				// $user->won = 0;
				// $user->balance += $remain;
			// }
		// }
		$user->accumulate += $this->amount;
		$user->save();
	}
	
	public function refund() {
		$user = $this->user;
		// if($this->amount < 0) {
			$user->balance -= $this->amount;
		// } else {
			// if($user->balance + $this->amount >= 0) {
				// $user->balance -= $this->amount;
			// } else {
				// $remain = $user->won - $this->amount;
				// $user->won = 0;
				// $user->balance -= $remain;
			// }
		// }
		$user->accumulate -= $this->amount;
		$user->save();
	}
}
