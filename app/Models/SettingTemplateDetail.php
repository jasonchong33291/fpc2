<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SettingTemplateDetail extends Model
{
	protected $fillable = [
		'name', 'value'
	];
	
	public $timestamps = false;
}
