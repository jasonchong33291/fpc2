<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RedPack extends Model
{
    use HasFactory;
		
    protected $fillable = [
        'amount', 'user_id', 'room_id'
    ];
	
	protected $casts = [
		'amount' => "float"
	];
	
	protected static function boot()
    {
        parent::boot();

        self::created(function($record){
			$record->open();
        });
		
        self::deleting(function($record){
			$record->refund();
        });
    }
	
	public function user() {
		return $this->belongsTo(User::class);
	}
	
	public function room() {
		return $this->belongsTo(Room::class);
	}	
	
	function open() {
		$user = $this->user;
		$user->balance += $this->amount;
		$user->accumulate += $this->amount;
		$user->save();
	}
	
	function refund() {
		$user = $this->user;
		if($user->balance + $this->amount >= 0) {
			$user->balance -= $this->amount;
		} else {
			$remain = $user->won - $this->amount;
			$user->won = 0;
			$user->balance -= $remain;
		}
		$user->accumulate -= $this->amount;
		$user->save();
	}
}
