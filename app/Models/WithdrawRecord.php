<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WithdrawRecord extends Model
{
    use HasFactory;
	
	const WITHDRAW = "提取完成";
	const CANCEL = "已取消提取";
	
	protected $fillable = [
		'user_id', 'amount', 'status', 'approved_id', 'rejected_id', 'remark'
	];
	
	protected static function boot()
    {
        parent::boot();

        self::created(function($record){
			if($record->status == self::WITHDRAW) {
				$record->withdraw();
			}
        });
		
        self::updating(function($record){
			$status = $record->getOriginal('status');
			if($status != $record->status) {
				if($status == self::WITHDRAW) {
					$record->refund();
				} if($record->status == self::WITHDRAW) {
					$record->withdraw();
				}
			}
        });

        self::deleting(function($record){
			if($record->status == self::WITHDRAW) {
				$record->refund();
			// } else if ($record->status == self::CANCEL) {
				// $record->withdraw();
			}
        });
    }
	
	public function approved() {
		return $this->belongsTo(Admin::class, 'approved_id');
	}
	
	public function rejected() {
		return $this->belongsTo(Admin::class, 'rejected_id');
	}
	
	public function user() {
		return $this->belongsTo(User::class);
	}
	
	public function withdraw() {
		$user = $this->user;
		// if($this->amount < 0) {
			$user->balance -= $this->amount;
		// } else {
			// if($user->balance + $this->amount >= 0) {
				// $user->balance -= $this->amount;
			// } else {
				// $remain = $user->won - $this->amount;
				// $user->won = 0;
				// $user->balance -= $remain;
			// }
		// }
		$user->save();
	}
	
	public function refund() {
		$user = $this->user;
		// if($this->amount >= 0) {
			$user->balance += $this->amount;
		// } else {
			// if($user->balance - $this->amount >= 0) {
				// $user->balance += $this->amount;
			// } else {
				// $remain = $user->won + $this->amount;
				// $user->won = 0;
				// $user->balance += $remain;
			// }
		// }
		$user->save();
	}
}
