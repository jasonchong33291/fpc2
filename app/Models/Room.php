<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Room extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'name',
        'entrance',
        'min',
        'max',
		'base_user_count',
		'game_id',
		'interval',
		'is_standalone',
		'is_api',
		'image_url',
		'icon_url',
		'status'
    ];
	
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'base_user_count',
    ];
	
	public function bettings() {
		return $this->hasMany(Betting::class);
	}
	
	public function bettingRecords() {
		return $this->hasManyThrough(BettingRecord::class, Betting::class, 'room_id', 'betting_id', 'id', 'id');
	}
	
	public function activeBettings() {
		return $this->hasMany(Betting::class)->where("ended_at", ">", Carbon::now())->orderBy('ended_at');
	}
	
	public function upcomingBettings() {
		return $this->hasMany(Betting::class)->whereNull("ended_at")->orderBy('id');
	}
	
	public function endedBettings() {
		if ($this->is_standalone) {
			return $this->hasMany(Betting::class)->where("reward", 1)->orderByDesc('created_at');
		} else {
			return $this->hasMany(Betting::class)->where("ended_at", "<=", Carbon::now())->orderByDesc('ended_at');
		}
	}
	
	public function game() {
		return $this->belongsTo(Game::class);
	}
	
	public function rateGroups() {
		return $this->hasMany(RoomRateGroup::class);
	}
	
	public function rates() {
		return $this->hasManyThrough(RoomRate::class, RoomRateGroup::class, "room_id", "group_id", "id", "id");
	}
	
	public function getCurrentAttribute() {
		return $this->activeBettings()->first();
	}
	
	public function getLastAttribute() {
		return $this->endedBettings()->first();
	}
	
	public function getLastWithResultAttribute() {
		$last = $this->endedBettings()->first();
		$last->results;
		return $last;
	}
	
	public function getTotalUniqueUsersAttribute() {
		$total = 0;
		$ids = $this->endedBettings()->pluck('id')->toArray();
		if($this->current) {
			array_push($ids, $this->current->id);
		}
		$uniqueUsers = BettingRecord::whereIn('betting_id', $ids);
		return $uniqueUsers->distinct('user_id')->count();
	}
}
