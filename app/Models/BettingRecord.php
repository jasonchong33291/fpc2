<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BettingRecord extends Model
{
    use HasFactory;
	protected $fillable = ["amount", "balance_amount", "won_amount"
		, "user_id", "betting_id", "room_id", "group", "group2"
		, "ball", "play_type", "play_name", "play_group", "play_option", "next_counter"];
		
	protected $appends = ["result", "roulette_color"];
	
	protected $casts = [
		'amount' => 'float',
		'balance_amount' => 'float',
		'won_amount' => 'float'
	];
	
	protected static function boot()
    {
        parent::boot();

        self::created(function($record){
			$user = $record->user;
			if($record->balance_amount || $record->won_amount) {
				$user->balance -= $record->balance_amount;
				$user->won -= $record->won_amount;
			} else {
				if($user->balance < $record->amount) {
					$user->won -= ($record->amount - $user->balance);
					$user->balance = 0;
				} else {
					$user->balance -= $record->amount;
				}
			}
			$user->save();
        });
		
        self::deleting(function($record){
			$user = $record->user;
			if($record->balance_amount || $record->won_amount) {
				$user->balance += $record->balance_amount;
				$user->won += $record->won_amount;
			} else {
				if($user->accumulate > $user->balance) {
					$remain = $user->accumulate - $user->balance;
					if($record->amount > $remain) {
						$user->balance = $user->accumulate;
						$user->won += $record->amount - $remain;
					} else {
						$user->balance += $record->amount;
					}
				} else {
					$user->won += $record->amount;
				}
			}
			$user->save();
        });
    }

	public function user() {
		return $this->belongsTo(User::class);
	}
	
	public function betting() {
		return $this->belongsTo(Betting::class);
	}
	
	public function rates() {
		return $this->hasMany(BettingRecordRate::class, 'record_id');
	}
	
	public function getResultAttribute() {
		if ($this->play_type == "2d" || $this->play_type == "4d") {
			$total = 0;
			if ($this->betting) {
				$rateGroups = $this->betting->room->rateGroups;
				$options = explode(" ", $this->play_group);
				$haveBig = str_contains($options[0], "B");
				$haveSmall = str_contains($options[$haveBig && count($options) > 1 ? 1 : 0], "S");
				$withBox = str_contains($options[count($options) - 1], "BOX");
				foreach ($this->betting->results as $prize) {
					if ($prize->value == $this->play_option || ($withBox && $prize->value == strrev($this->play_option))) {
						$keyword = "";
						switch ($prize->api_prize) {
							case "firstPrize":
								$keyword = "1st_prize";
								break;
							case "secondPrize":
								$keyword = "2nd_prize";
								break;
							case "thirdPrize":
								$keyword = "3rd_prize";
								break;
							case "con":
								if ($haveBig)
									$keyword = "consolation_prize";
								break;
							case "spe":
								if ($haveBig)
									$keyword = "special_prize";
								break;
						}
						if (!$keyword) {
							continue;
						}
						$group = $rateGroups->where("group", $this->play_type . "_" . $keyword)->first();
						if (!$group) {
							// avoid bug
							continue;
						}
						if ($haveBig) {
							$bigRate = $group->rates()
								->where("name", "big_" . $keyword)
								->orWhere("name", $keyword)->first();
							if (!$bigRate) {
								continue;
							}
							$total += $bigRate->rate;
						}
						if ($haveSmall) {
							$smallRate = $group->rates()->where("name", "small_" . $keyword)->first();
							if (!$smallRate) {
								continue;
							}
							$total += $smallRate->rate;
						}
					}
				}
				if ($options[count($options) - 1] == "IBOX") {
					$total /= 2;
				}
			}
			return $total;
		}		
		if($this->betting && $this->betting->total !== null) {
			if($this->ball !== null) {
				return 0;
			} else {
				switch ($this->play_type) {
					case "fpc":
						$values = $this->betting->results()->select('value')->pluck("value")->toArray();
						switch ($this->play_group) {
							case 'single_any':
								$count = count(array_filter($values, function($val) {
									return $val == $this->play_option;
								}));
								$gRate = $this->rates->where("matched", $count)->first();
								return $gRate ? $gRate->rate : 0;
							case 'double_any':
								$pValues = explode("-", $this->play_option);
								$count = count(array_intersect($pValues, $values));
								$gRate = $this->rates->where("matched", $count)->first();
								return $gRate ? $gRate->rate : 0;
							case 'all_match':
								$pValues = explode("-", $this->play_option);
								$gRate = $this->rates->first();
								return $gRate && array_unique($pValues) == 1 ? $gRate->rate : 0;
							default:
								return 0;
						}
					case "roulette":
						$result = $this->betting->results()->first();
						switch ($this->play_group) {
							case 'color':
								$rate = $this->rates()->first();
								return $this->play_option == $result->rouletteColor && $rate ? $rate->rate : 0;
							case 'number':
								$pValue = str_pad($this->numericPlayOption, 2, "0");
								$rate = $this->rates()->first();
								return $result->value == $pValue[1] && $rate ? $rate->rate : 0;
							case 'ten_range':
								$pValues = explode("-", $this->play_option);
								$rate = $this->rates()->first();
								return $result->value > $pValues[0] && $result->value < $pValues[1] && $rate ? $rate->rate : 0;
							case 'twenty_range':
								$pValues = explode("\n", $this->play_option);
								$rate = $this->rates()->first();
								return $result->value > $pValues[0] && $result->value < $pValues[1] && $rate ? $rate->rate : 0;
							case 'single':
								$rate = $this->rates()->first();
								return $this->play_option == $result->value && $rate ? $rate->rate : 0;
							default:
								return 0;
						}
					default:
						return 0;
				}
			}
		} else {
			return -1;
		}
	}
	
	public function getNumericPlayOptionAttribute() {
		switch ($this->play_option) {
			case 'zero':
				return 0;
			case 'one':
				return 1;
			case 'two':
				return 2;
			case 'three':
				return 3;
			case 'four':
				return 4;
			case 'five':
				return 5;
			case 'six':
				return 6;
			case 'seven':
				return 7;
			case 'eight':
				return 8;
			case 'nine':
				return 9;
			default:
				return -1;
		}
	}
	
	public function getRouletteColorAttribute() {
		if ($this->play_type != "roulette") {
			return "";
		}
		if ($this->play_group == "single") {
			$value = str_pad($this->play_option, 2, "0", STR_PAD_LEFT);
			$i = $value[0];
			$j = $value[1];
			return $i < 5 && 10 - $j - $i - 1 > 0 && $j - $i + 1 > 0 ? 'yellow' : (
				$j > 4 && $j - $i > 0 ? 'green': (
				$i > 4 && 10 - $j - $i - 1 < 0 && $j - $i - 1 < 0 ? 'blue': 'red'));
		} else {
			return '';
		}
	}
}
