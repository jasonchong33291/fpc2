<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable, SoftDeletes;
	
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name',
        'username',
        'password',
        'invitation_code',
        'real_name',
        'card_number',
        'withdraw_pin',
		'inviter_id',
		'admin_id',
		'from_redirect',
		'from_wallet'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
		'withdraw_pin',
		'card_number',
		'real_name',
        'password',
        'remember_token',
		'last_login_ip'
    ];
	
	protected $casts = [
		'balance' => 'float',
		'won' => 'float',
		'accumulate' => 'float',
	];
	
	public function admin() {
		return $this->belongsTo(Admin::class);
	}

	public function bettingRecords() {
		return $this->hasMany(BettingRecord::class);
	}
	
	public function inviter() {
		return $this->belongsTo(User::class, 'inviter_id');
	}
	
	public function inviterRecords() {
		return $this->hasMany(InvitationRecord::class, 'inviter_id');
	}
	
	public function invitedRecords() {
		return $this->hasMany(InvitationRecord::class, 'invited_id');
	}
	
	public function logins() {
		return $this->hasMany(UserLogin::class);
	}
	
	public function records() {
		return $this->hasMany(BettingRecord::class);
	}
	
	public function redPacks() {
		return $this->hasMany(RedPack::class);
	}
	
	public function topupRecords() {
		return $this->hasMany(TopupRecord::class);
	}
	
	public function withdrawRecords() {
		return $this->hasMany(WithdrawRecord::class);
	}

	public function getTotalBalanceAttribute() {
		return $this->won + $this->balance;
	}
}
