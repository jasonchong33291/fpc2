<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
	protected $fillable = [
		'key', 'value'
	];
	
	public $timestamps = false;
	
    public function details() {
		return $this->hasManyThrough(SettingTemplateDetail::class, SettingTemplate::class, "key", "template_id", "key", "id");
	}
}
