<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;

class Game extends Model
{
    use HasFactory;
	
    protected $fillable = [
        'name', 'key', 'min', 'max', 'count'
    ];
	
	public function rateGroups() {
		return $this->hasMany(GameRateGroup::class);
	}
	
	public function rooms() {
		return $this->hasMany(Room::class);
	}
}
