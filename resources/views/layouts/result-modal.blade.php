<div id="result_modal" class="modal fade">
	<div class="modal-dialog modal-xl modal-dialog-centered">
		<div class="modal-content">
			<div class="modal-body">
				<div id="win_animation" style="display: none;">
					<img id="win_animation_img" src="{{ asset('images/GIF_06.gif') }}" class="god-animate" />
				</div>
				<div id="result_panel" class="row justify-content-center" style="display: none;">
					<div class="win-container">
						<span id="balance_label">Win</span>: <span id="total_balance"></span>
					</div>
				</div>
			</div>
		</div> 
	</div>
</div>