@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">充值</div>

                <div class="card-body">
                    @if (session('message'))
                        <div class="alert alert-success" role="alert">
                            {{ session('message') }}
                        </div>
                    @endif
					<form method="POST" action="{{ route('topup') }}">
						@csrf
						<div class="row form-group">
							<div class="col-6">
								<label>充值金额</label>
							</div>
							<div class="col-6">
								<input class="form-control" name="amount" type="number" placeholder="充值金额" />
							</div>
						</div>

						<div class="row">
							<div class="col-12 text-right">
								<button type="submit" class="button btn-primary">充值</button>
							</div>
						</div>
					</form>
                </div>
            </div>
        </div>
		<div class="col-md-4">
            <div class="card">
                <div class="card-header">我的钱包</div>

                <div class="card-body">
					<div class="row">
						<div class="col-md-4">
							<label>当前余额</label>
						</div>
						<div class="col-md-8 text-right">
							<label>${{ number_format($user->totalBalance, 2) }}</label>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection