@extends('admins.layouts.app')

@section('css')
	<style>
		.responsive{
			width: 100%!important;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">用户</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">用户</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>用户数量</small>
                <h5 id="count" class="text-info">{{ $users->count() }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		const defaultUserList = [{ id: "", name: "{{ __('系统') }}" }];
		let users = defaultUserList.concat({!! $users !!});
		const dtOpts = {
			listingUrl: "{{ Route('admin.user') }}",
			createUrl: "{{ Route('admin.user.create') }}",
			updateUrl: "{{ Route('admin.user.update') }}",
			deleteUrl: "{{ Route('admin.user.delete') }}",
			order: [[ 0, "asc" ]],
			fields: [{
			@if($user->role && !$user->role->regional)
				label: "{{ __('负责人') }}",
				name: "admin_id",
				type: "select",
				options : [{ id: "", name: "{{ __('无') }}" }].concat({!! $admins !!}),
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
			@endif
				label: "{{ __('邀请人') }}",
				name: "inviter_id",
				type: "select",
				options : users,
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
				label: "{{ __('用户名称') }}",
				name: "name",
			}, {
				label: "{{ __('账号') }}",
				name: "username",
			}, {
				label: "{{ __('密码') }}",
				name: "password",
				type: "password",
				attr: {
					autocomplete: 'off'
				}
			}, {
				label: "{{ __('确认密码') }}",
				name: "password_confirmation",
				type: "password",
				attr: {
					autocomplete: 'off'
				}
			}],
			postCreate: function (editor, e, json, data) {
				users.push(data);
				editor.field('inviter_id').update(users);
				$("#count").text(users.length);
			},
			postEdit: function (editor, e, json, data, id) {
				let index = users.findIndex(user => user.id == id);
				if(index > -1) {
					users[index] = data;
					editor.field('inviter_id').update(users);
				}
			},
			postRemove: function (editor, e, json, ids) {
				ids.forEach(id => {
					let index = users.findIndex(user => user.id == id);
					if(index > -1) {
						users.splice(index, 1);
					}
				});
				editor.field('inviter_id').update(users);
				$("#count").text(users.length);
			},
			// hideOnEdit: ["username"],
			columns: [{
				title: "{{ __('用户名称') }}",
				data: "name"
			}, {
			@if($user->role && !$user->role->regional)
				title: "{{ __('负责人') }}",
				data: "admin.name",
				defaultContent: "{{ __('无') }}",
				responsivePriority: 1
			}, {
			@endif
				title: "{{ __('邀请人') }}",
				data: "inviter.name",
				defaultContent: "{{ __('系统') }}",
				responsivePriority: 4,
			}, {
				title: "{{ __('账号') }}",
				data: "username",
				responsivePriority: 2
			}, {
				title: "{{ __('余额') }}",
				data: "balance",
				defaultContent: "0.00",
				responsivePriority: 5
			}, {
				title: "{{ __('流水') }}",
				data: "won",
				defaultContent: "0.00",
				responsivePriority: 6
			}, {
				title: "{{ __('充值总额') }}",
				data: "accumulate",
				defaultContent: "0.00",
				responsivePriority: 7
			}, {
				title: "{{ __('账号状态') }}",
				data: "deleted_at",
				defaultContent: "<b class='text-success'>已激活</b>",
				responsivePriority: 8,
				render: function(data) {
					return data ? ("<b class='text-danger'>已冻结</b><br /><i>" + new Date(data).toLocaleString() + "</i>")
						: "<b class='text-success'>已激活</b>";
				}
			}, {
			@if($user->role && !$user->role->regional)
				title: "{{ __('最后登录IP') }}",
				data: "last_login_ip_text",
				responsivePriority: 3,
				defaultContent: "{{ _('无登录记录') }}"
			}, {
			@endif
				title: "{{ __('加入日期') }}",
				data: "created_at",
				responsivePriority: 9,
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
				@if(RouteGuard::check('admin.user.create'))
					extend: "create",
					editor: editor,
					text: "{{ __('增加') }}"
				}, {
				@endif
				@if(RouteGuard::check('admin.user.update'))
					extend: "editSingle",
					editor: editor,
					text: "{{ __('更改') }}"
				}, {
				@endif
				@if(RouteGuard::check('admin.user.delete'))
					extend: "remove",
					editor: editor,
					text: "{{ __('冻结账号/激活账号') }}"
				}, {
				@endif
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload();
					}
				}, {
					text: "{{ __('发送Whatsapp') }}",
					extend: "selectedSingle",
					action: function (e, dt) {
						const data = dt.rows({ selected: true }).data()[0];
						if(data.raw_password) {
							window.open("https://web.whatsapp.com/send?text=你的用户账号为"
								+ data.username + "，密码为" + data.raw_password + "。");
						} else {
							Swal.fire("无法发送", "系统已加密密码，只有刚增加或者更改过密码的用户可以发送。", "warning");
						}
					}
				}, {
					text: "{{ __('发送Telegram') }}",
					extend: "selectedSingle",
					action: function (e, dt) {
						const data = dt.rows({ selected: true }).data()[0];
						if(data.raw_password) {
							window.open("https://t.me/share/url?url={{ url('/') }}&text=你的用户账号为"
								+ data.username + "，密码为" + data.raw_password + "。");
						} else {						
							Swal.fire("无法发送", "系统已加密密码，只有刚增加或者更改过密码的用户可以发送。", "warning");
						}
					}
				}];
			}
		}
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection