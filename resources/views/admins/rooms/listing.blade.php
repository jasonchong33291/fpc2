@extends('admins.layouts.app')

@section('css')
	<style>
		.responsive{
			width: 100%!important;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">房间</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">房间</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>-</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		const members = {};
		let interval;
		const games = {!! json_encode($games) !!};
		
		const dtOpts = {
			listingUrl: "{{ Route('admin.room') }}",
			createUrl: "{{ Route('admin.room.create') }}",
			updateUrl: "{{ Route('admin.room.update') }}",
			deleteUrl: "{{ Route('admin.room.delete') }}",
			uploadUrl: "{{ Route('admin.room.upload') }}",
			order: [[ 0, "asc" ]],
			fields: [{
				label: "{{ __('名字') }}",
				name: "name",
				type: "text",
			}, {
				label: "{{ __('Game') }}",
				name: "game_id",
				type: "select",
				options : [{ id: "", name: "{{ __('无') }}" }].concat({!! $games !!}),
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
				label: "{{ __('基础人数') }}",
				name: "base_user_count",
				type: "text",
				default: 0
			}, {
				label: "{{ __('最低入场') }}",
				name: "entrance",
				type: "text",
			}, {
				label: "{{ __('最低下注') }}",
				name: "min",
				type: "text",
			}, {
				label: "{{ __('最高下注') }}",
				name: "max",
				type: "text",
			}, {
				label: "{{ __('状态') }}",
				name: "status",
				type: "select",
				options : ["avalable", "unavailable"]
			}, {
				label: "{{ __('lobby照片') }}",
				name: "image_url",
				type: "upload",
				display: function ( file_id ) {
					var file = null;
					
					try {
						file = gEditor.file( 'images', file_id );
					} catch{
					}
					if(file) {
						return '<img src="'+ file.path +'" />';
					} else {
						return null;
					}
				},
				clearText: "Remove",
				noImageText: '无上传照片.',
				attr: {
					accept: "image/*"
				}
			}, {
				label: "{{ __('API照片') }}",
				name: "icon_url",
				type: "upload",
				display: function ( file_id ) {
					var file = null;
					
					try {
						file = gEditor.file( 'icons', file_id );
					} catch{
					}
					if(file) {
						return '<img src="'+ file.path +'" />';
					} else {
						return null;
					}
				},
				clearText: "Remove",
				noImageText: '无上传照片.',
				attr: {
					accept: "image/*"
				}
			}],
			columns: [{
				title: "{{ __('名字') }}",
				data: "name",
				responsivePriority: 1,
			}, {
				title: "{{ __('游戏') }}",
				data: "game.name",
				responsivePriority: 2,
				defaultContent: "无"
			}, {
				title: "{{ __('状态') }}",
				data: "status",
				responsivePriority: 5,
				defaultContent: "无"
			}, {
				title: "{{ __('lobby照片') }}",
				data: "image_url",
				responsivePriority: 3,
				render: function (file_id, config, row) {
					var file;
					try {
						file = gEditor.file('images', file_id );
					}catch{
					}
					var img = document.createElement("img");
					img.title = row.path;
					img.alt = row.caption;
					if(file) {
						img.src = file.path;
					} else {
						img.src = "{{ asset('storage') }}/" + row.url;
					}
					return img.outerHTML;
				}
			}, {
				title: "{{ __('API照片') }}",
				data: "icon_url",
				responsivePriority: 3,
				render: function (file_id, config, row) {
					var file;
					try {
						file = gEditor.file('icons', file_id );
					}catch{
					}
					var img = document.createElement("img");
					img.title = row.path;
					img.alt = row.caption;
					if(file) {
						img.src = file.path;
					} else {
						img.src = "{{ asset('storage') }}/" + row.url;
					}
					return img.outerHTML;
				}
			}, {
				title: "{{ __('基础人数') }}",
				data: "total_base_users",
				responsivePriority: 6,
				defaultContent: "0"
			}, {
				title: "{{ __('已参与人数') }}",
				data: "total_unique_users",
				responsivePriority: 3,
				defaultContent: "0"
			}, {
				title: "{{ __('显示人数') }}",
				data: "total_unique_users",
				responsivePriority: 4,
				render: function (data, config, row) {
					return (row.total_base_users || 0) + (data || 0);
				}
			}, {
				title: "{{ __('最低入场') }}",
				data: "entrance",
				responsivePriority: 10,
				defaultContent: "无"
			}, {
				title: "{{ __('最低下注') }}",
				data: "min",
				responsivePriority: 11,
				defaultContent: "无下限"
			}, {
				title: "{{ __('最高下注') }}",
				data: "max",
				responsivePriority: 12,
				defaultContent: "无上限"
			}, {
				title: "{{ __('赔率') }}",
				data: "rates",
				defaultContent: "无",
				responsivePriority: 4,
				render: function(data) {
					let html = "";
					data.forEach((item, index) => {
						if(index % 4 == 0) {
							html += "<div class='row'>";
						}
						html += "<div class='col-md-6 col-12'>" + item?.name + ": " + item?.rate + "</div>";
						if(index % 4 == 3) {
							html += "</div>";
						}
					});
					
					return html;
				}
			}, {
				title: "{{ __('调整') }}",
				data: "id",
				responsivePriority: 4,
				render: function(data) {
					let html = "<a class='btn btn-primary' href='{{ route("admin.room.rate", ["id" => ""]) }}/" + data + "'>调整赔率</a>";					
					return html;
				}
			}],
			buttons: function(editor) {
				return [{
					extend: "create",
					editor: editor,
					text: "{{ __('新房间') }}"
				}, {
					extend: "edit",
					editor: editor,
					text: "{{ __('调整房间') }}"
				}, {
					extend: "remove",
					editor: editor,
					text: "{{ __('删除房间') }}"
				}, {
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload(null, false);
					}
				}];
			}
		}
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection