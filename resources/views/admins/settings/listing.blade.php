@extends('admins.layouts.app')

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">设定</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">设定</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>-</small>
                <h5 id="count" class="text-info">-</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			@foreach($setting_template_list as $template)
				<div class="row form-group">
					<div class="col-6">
						<label class="form-label">{{ $template->name }}</label>
					</div>
					<div id="{{ $template->type }}_{{ $template->id }}" class="col-6">
						@php($value = (($setting = $setting_list->where('key', $template->key)->first()) ? $setting->value : ''))	
						@switch($template->type)
							@case('date')
								<input type="date" name="{{ $template->key }}" value="{{ ${$template->key}->format('Y-m-d') }}" class="form-control setting-change"
									data-key="{{ $template->key }}" data-name="{{ $template->name }}" />
								@break
							@case('datetime-local')
								<input type="datetime-local" name="{{ $template->key }}" value="{{ ${$template->key}->format('Y-m-d\TH:i') }}" class="form-control setting-change"
									data-key="{{ $template->key }}" data-name="{{ $template->name }}" />
								@break
							@case('radio')
								@foreach($template->details as $detail)
									<div class="form-check form-check-inline">
										<input id="{{ $detail->id }}" type="radio" name="{{ $template->key }}" value="{{ $detail->value }}" class="form-check-input setting-click"
											data-key="{{ $template->key }}" data-name="{{ $template->name }}" data-disabled="name" data-parent="#selection_{{ $template->id }}" {{ $value == $detail->value ? 'checked' : '' }} />
										<label for="{{ $detail->id }}" class="form-check-label">{{ $detail->name }}</label>
									</div>
								@endforeach
								@break
							@case('select')
								<select class="form-control setting-change" data-key="{{ $template->key }}" data-name="{{ $template->name }}">
								@foreach($template->details as $detail)
									<option value="{{ $detail->value }}" {{ $value == $detail->value ? 'selected' : '' }}>{{ $detail->name }}</option>
								@endforeach
								</select>
								@break
							@case('button')
								<button data-key="{{ $template->key }}" data-name="{{ $template->name }}" class="button btn-warning setting-button">{{ $template->details->first()->name }}</button>
								@break
							@default
								<input type="{{ $template->type }}" name="{{ $template->key }}" value="{{ $value }}" class="form-control setting-change" data-key="{{ $template->key }}" data-name="{{ $template->name }}" />
								@break
						@endswitch
					</div>
				</div>
			@endforeach
			
			<div class="row form-group" id="access_container">
				<div class="col-12">
					<input id="access" class="form-control" type="password" placeholder="主钥匙" autocomplete="off" />
					
					<span class="invalid-feedback" role="alert">
						<strong>全部删除程序需要主钥匙</strong>
					</span>
				</div>
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		const settingUrl = "{{ Route('admin.setting.update') }}";
	</script>
    <script type="text/javascript" src="{{ asset('js/setting.js') }}" defer></script>
@endsection