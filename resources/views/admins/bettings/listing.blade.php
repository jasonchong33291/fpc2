@extends('admins.layouts.app')

@section('css')
	<style>
		.responsive{
			width: 100%!important;
		}
		
		.background-red {
			color: white;
			background-color: red;
		}
		
		.background-blue {
			color: white;
			background-color: blue;
		}
		
		.background-green {
			color: white;
			background-color: green;
		}
		
		.background-yellow {
			color: white;
			background-color: #ffaf36;
		}
		
		.roulette-result {
			border-radius: 50%;
			height: 35px;
			width: 35px;
			padding: 3px;
			font-size: 12px;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">彩票</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">彩票</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>彩票数量</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
        <div class="col-md-3">
			<select class="form-control" onchange="changeListingUrl('room_id', this.value, 0)">
				<option value="">全部房间</option>
				@foreach($rooms as $room)
				<option value="{{ $room->id }}" {{ $room_id && $room_id == $room->id ? "selected" : "" }}>{{ $room->name }}</option>
				@endforeach
			</select>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
@parent
<script type="text/javascript">
	let interval;
	let dListingUrl = "{{ Route('admin.betting') }}";
	let qsOptions = {};
	const fpcOrders = {!! json_encode($fpc_order) !!};
		
	function convertQueryString() {
		let qs = "";
		Object.keys(qsOptions).forEach((key, index) => {
			qs += (index == 0 ? "?" : "&") + key + "=" + qsOptions[key];
		});
		return qs;
	}	
	function changeListingUrl(field, val, colIndex = -1) {
		qsOptions[field] = val;
		// reload data
		dTable.ajax.url(dListingUrl + convertQueryString()).load();
		if(colIndex > -1) {
			// show hide column
			const column = dTable.column(colIndex);
			 // Toggle the visibility
			column.visible(val ? false : true);
		}
	}
	const dtOpts = {
		listingUrl: dListingUrl,
		createUrl: "{{ Route('admin.betting.create') }}",
		updateUrl: "{{ Route('admin.betting.update') }}",
		deleteUrl: "{{ Route('admin.betting.delete') }}",
		order: [[ 0, "asc" ]],
		serverSide: true,
		processing: true,
		fields: [{
			label: "{{ __('第1颗骰子') }}",
			name: "ball1",
			type: "text",
			fieldInfo: '{{ $dice_min }}-{{ $dice_max }}'
		}, {
			label: "{{ __('第2颗骰子') }}",
			name: "ball2",
			type: "text",
			fieldInfo: '{{ $dice_min }}-{{ $dice_max }}'
		}, {
			label: "{{ __('第3颗骰子') }}",
			name: "ball3",
			type: "text",
			fieldInfo: '{{ $dice_min }}-{{ $dice_max }}'
		}],
		columns: [{
			title: "{{ __('房间') }}",
			data: "room_name",
			defaultContent: "已删除",
		}, {
			title: "{{ __('期') }}",
			data: "version"
		}, {
			title: "{{ __('结果') }}",
			data: "results",
			render: function (data, config, row) {
				if (data.length > 0) {
					switch (row.game_key) {
							case "fpc":
								return data.map(x => {
									const img = document.createElement("img");
									img.src = "{{ asset('images') }}/" + fpcOrders[x.value] + "-01.png";
									img.style.width = "20px";
									return img.outerHTML;
								});
							case "roulette":
								const i = document.createElement("i");
								i.className = "background-" + data[0].roulette_color + " roulette-result";
								i.innerHTML = (data[0].value).toString().padStart(2, "0");
								return i.outerHTML;
							default:
								return data.map(x => x.value).join(", ");
						}
				} else {
					return "未开奖";
				}
			}
		}, {
			title: "{{ __('下注数') }}",
			data: "record_count",
			defaultContent: "-",
		}, {
			title: "{{ __('参与人数') }}",
			data: "unique_user_count",
			defaultContent: "-",
		}, {
			title: "{{ __('下注金额') }}",
			data: "in_sum",
			defaultContent: "<span class='text-success'>0</span>",
			render: function(data) {
				return "<span class='text-success'>" + (data || "0.00") + "</span>";
			}
		}, {
			title: "{{ __('结算金额') }}",
			data: "out_sum",
			defaultContent: "未结算",
			render: function(data, config, row) {
				return row.reward ? "<span class='text-danger'>" + (data || "0.00") + "</span>" : "未结算";
			}
		}, {
			title: "{{ __('结束日期') }}",
			data: "ended_at",
			render: function (data) {
				return data ? new Date(data).toLocaleString() : "";
			}
		}],
		buttons: function(editor) {
			return [{
				extend: "edit",
				editor: editor,
				text: "{{ __('更新') }}"
			}, {
				text: "{{ __('随机生成彩球') }}",
				extend: "selected",
				action: function (e, dt, node, config) {
					const ids = dt.rows({ selected: true }).data().toArray().map(x => x.id);
					if(ids.length > 0) {
						$.ajax({
							url: "{{ route('admin.betting.random') }}",
							data: {
								ids: ids,
							},
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
							type: "POST"
						}).done(function(record) {
							dt.ajax.reload(null, false);
						}).fail(function(err) {
							Swal.fire("发生错误", err.responseJSON.message, "error");
						});
					}
				}
			}, {
				text: "{{ __('手动结算') }}",
				extend: "selected",
				action: function (e, dt, node, config) {
					const ids = dt.rows({ selected: true }).data().toArray().map(x => x.id);
					if(ids.length > 0) {
						$.ajax({
							url: "{{ route('admin.betting.deliver') }}",
							data: {
								ids: ids,
							},
							headers: {
								'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
							},
							type: "POST"
						}).done(function(record) {
							dt.ajax.reload(null, false);
						}).fail(function(err) {
							Swal.fire("发生错误", err.responseJSON.message, "error");
						});
					}
				}
			}, {
				text: "{{ __('刷新') }}",
				action: function (e, dt, node, config) {
					dt.ajax.reload(null, false);
				}
			}, {
				text: "{{ __('自动刷新') }}",
				action: function (e, dt, node, config) {
					if(interval) {
						node[0].innerText = "自动刷新";
						clearInterval(interval);
						interval = null;
					} else {
						node[0].innerText = "刷新中";
						interval = setInterval(function() {
							dt.ajax.reload(null, false);
						}, 30000);
					}
				}
			}];
		}
	}
</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection