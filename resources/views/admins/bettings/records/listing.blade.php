@extends('admins.layouts.app')

@section('css')
	<style>
		.responsive{
			width: 100%!important;
		}
		
		.background-red {
			color: white;
			background-color: red;
		}
		
		.background-blue {
			color: white;
			background-color: blue;
		}
		
		.background-green {
			color: white;
			background-color: green;
		}
		
		.background-yellow {
			color: white;
			background-color: #ffaf36;
		}
		
		.roulette-result {
			border-radius: 50%;
			height: 35px;
			width: 35px;
			padding: 3px;
			font-size: 12px;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">下注记录</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">下注记录</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>记录数量</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row" style="margin-top: 10px; margin-bottom: 10px;">
        <div class="col-md-3">
			<select class="form-control" onchange="changeListingUrl('room_id', this.value, 1)">
				<option value="">全部房间</option>
				@foreach($rooms as $room)
				<option value="{{ $room->id }}">{{ $room->name }}</option>
				@endforeach
			</select>
		</div>
		<div class="col-md-3">
			<select class="form-control" onchange="changeListingUrl('result', this.value, 7)">
				<option value="">全部开奖</option>
				<option value="中奖">中奖</option>
				<option value="没中">没中</option>
				<option value="未开奖">未开奖</option>
			</select>
		</div>
	</div>
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		let interval;
		let dListingUrl = "{{ Route('admin.betting.record') }}?tz=" + (new Date().getTimezoneOffset() / -60);
		let qsOptions = {};
		const fpcOrders = {!! json_encode($fpc_order) !!};
		
		function convertQueryString() {
			let qs = "";
			Object.keys(qsOptions).forEach((key, index) => {
				qs += 	"&" + key + "=" + qsOptions[key];
			});
			return qs;
		}	
		function changeListingUrl(field, val, colIndex = -1) {
			qsOptions[field] = val;
			// reload data
			dTable.ajax.url(dListingUrl + convertQueryString()).load();
			if(colIndex > -1) {
				// show hide column
				const column = dTable.column(colIndex);
				 // Toggle the visibility
				column.visible(val ? false : true);
			}
		}
		const dtOpts = {
			listingUrl: dListingUrl,
			order: [[ 10, "desc" ]],
			serverSide: true,
			processing: true,
			columns: [{
				title: "{{ __('用户') }}",
				data: "user_name",
				defaultContent: "已删除"
			}, {
				title: "{{ __('房间') }}",
				data: "room_name",
				defaultContent: "已删除"
			}, {
				title: "{{ __('期') }}",
				data: "betting_version",
				defaultContent: "已删除"
			}, {
				title: "{{ __('金额') }}",
				data: "amount"
			}, {
				title: "{{ __('玩法') }}",
				data: "play_name",
				defaultContent: "未开奖"
			}, {
				title: "{{ __('选择') }}",
				data: "play_option",
				render: function(data, type, row) {
					if (data) {
						switch (row.play_type) {
							case "fpc":
								return data.split("-").map(x => {
									if (fpcOrders[x]) {
										const img = document.createElement("img");
										img.src = "{{ asset('images') }}/" + fpcOrders[x] + "-01.png";
										img.style.width = "20px";
										return img.outerHTML;
									} else {
										return "-";
									}
								});
							case "roulette":
								const b = document.createElement("b");
								b.className = "background-" + row.roulette_color + " roulette-result";
								b.innerHTML = (data).toString().padStart(2, "0");
								return b.outerHTML;
							default:
								return data;
						}
					} else {
						return "";
					}
				}
			}, {
				title: "{{ __('赔率') }}",
				data: "betting_rates[ - ]"
			}, {
				title: "{{ __('结算') }}",
				data: "won",
				render: function(data) {
					return data != null ? (data) : "未结算";
				}
			}, {
				title: "{{ __('开奖') }}",
				data: "betting_results",
				render: function(data, type, row) {
					if (data?.length > 0) {
						switch (row.play_type) {
							case "fpc":
								return data.map(x => {
									const img = document.createElement("img");
									img.src = "{{ asset('images') }}/" + fpcOrders[x.value] + "-01.png";
									img.style.width = "20px";
									return img.outerHTML;
								});
							case "roulette":
								const b = document.createElement("b");
								b.className = "background-" + data[0].roulette_color + " roulette-result";
								b.innerHTML = (data[0].value).toString().padStart(2, "0");
								return b.outerHTML;
							default:
								return data.map(x => x.value).join(", ");
						}
					} else {
						return "未开奖";
					}
				}
			}, {
				title: "{{ __('开奖结果') }}",
				data: "result",
				render: function(data) {
					return data > -1 ? (data > 0 ? "中奖" : "没中") : "未开奖";
				}
			}, {
				title: "{{ __('日期') }}",
				data: "created_at",
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload(null, false);
					}
				}, {
					text: "{{ __('自动刷新') }}",
					action: function (e, dt, node, config) {
						if(interval) {
							node[0].innerText = "自动刷新";
							clearInterval(interval);
							interval = null;
						} else {
							node[0].innerText = "刷新中";
							interval = setInterval(function() {
								dt.ajax.reload(null, false);
							}, 30000);
						}
					}
				}];
			}
		}
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection