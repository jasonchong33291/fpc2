@extends('admins.layouts.app')

@section('css')
	<style>
		.responsive{
			width: 100%!important;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-6">
            <h4 class="page-title">广告</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">广告</li>
                </ol>
            </nav>
        </div>
        <div class="col-6">
            <div class="text-right">
                <small>-</small>
                <h5 id="count" class="text-info">{{ $record_count }}</h5>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		const members = {};
		let interval;
		const types = {!! json_encode($types) !!};
		
		const dtOpts = {
			listingUrl: "{{ Route('admin.banner') }}",
			createUrl: "{{ Route('admin.banner.create') }}",
			updateUrl: "{{ Route('admin.banner.update') }}",
			deleteUrl: "{{ Route('admin.banner.delete') }}",
			uploadUrl: "{{ Route('admin.banner.upload') }}",
			order: [[ 0, "asc" ]],
			fields: [{
				label: "{{ __('标题') }}",
				name: "caption",
				type: "text",
			}, {
				label: "{{ __('位置') }}",
				name: "type_id",
				type: "select",
				options : [{ id: "", name: "{{ __('无') }}" }].concat({!! $types !!}),
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
				label: "{{ __('照片') }}",
				name: "url",
				type: "upload",
				display: function ( file_id ) {
					var file = null;
					
					try {
						file = gEditor.file( 'images', file_id );
					} catch{
					}
					if(file) {
						return '<img src="'+ file.path +'" />';
					} else {
						return null;
					}
				},
				clearText: "Remove",
				noImageText: '无上传照片.',
				attr: {
					accept: "image/*"
				}
			}, {
				label: "{{ __('内文') }}",
				name: "description",
				type: "text",
			}, {
				label: "{{ __('显示标题与内文') }}",
				name: "show_caption",
				type: "checkbox",
				separator: "|",
                options: [
                    { label: '显示', value: "1" }
                ],
				unselectedValue: "0"
			}],
			columns: [{
				title: "{{ __('标题') }}",
				data: "caption",
				responsivePriority: 1,
			}, {
				title: "{{ __('内文') }}",
				data: "description",
				responsivePriority: 5,
				defaultContent: "无"
			}, {
				title: "{{ __('照片') }}",
				data: "id",
				responsivePriority: 2,
				render: function (file_id, config, row) {
					var file;
					try {
						file = gEditor.file('images', file_id );
					}catch{
					}
					var img = document.createElement("img");
					img.title = row.path;
					img.alt = row.caption;
					if(file) {
						img.src = file.path;
					} else {
						img.src = "{{ asset('storage') }}/" + row.url;
					}
					return img.outerHTML;
				}
			}, {
				title: "{{ __('位置') }}",
				data: "type.name",
				responsivePriority: 3,
				defaultContent: "{{ __('无') }}"
			}, {
				title: "{{ __('显示标题与内文') }}",
				data: "show_caption",
				responsivePriority: 4,
				render: function (data) {
					return "<i class='feather icon-" + (data ? "check" : "x") + "' style='color: " + (data ? "green" : "red") + ";'></i>";
				}
			}],
			buttons: function(editor) {
				return [{
					extend: "create",
					editor: editor,
					text: "{{ __('新广告') }}"
				}, {
					extend: "edit",
					editor: editor,
					text: "{{ __('调整广告') }}"
				}, {
					extend: "remove",
					editor: editor,
					text: "{{ __('删除广告') }}"
				}, {
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload(null, false);
					}
				}];
			}
		}
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection