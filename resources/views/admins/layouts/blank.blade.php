<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}后台</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <!-- Bootadmin -->
    <link href="{{ asset('css/bootadmin/style.min.css') }}" rel="stylesheet">
    @yield('css')
	
    <!-- Scripts -->
    <script type="text/javascript" src="{{ asset('js/app.js') }}" defer></script>
    <!-- Bootadmin -->
    <script type="text/javascript" src="{{ asset('js/bootadmin.js') }}" defer></script>
	@yield('js')
</head>
<body id="landing" class="	sidebar-open">
	<div class="loading">
		<div class="loading-center"><img src="{{ asset('images/loading/running.gif') }}" alt="Loading" /></div>
	</div>
	<div class="page-container animsition">
		<div id="app">
			<main class="py-4">
				@yield('content')
			</main>
		</div>
	</div>
</body>
</html>