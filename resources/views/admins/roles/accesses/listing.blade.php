@extends('admins.layouts.app')

@section('header')
	<style>
		.responsive{
			width: 100%!important;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12">
            <h4 class="page-title">工作范围</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">工作范围</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		const dtOpts = {
			listingUrl: "{{ Route('admin.role.access') }}",
			createUrl: "{{ Route('admin.role.access.create') }}",
			updateUrl: "{{ Route('admin.role.access.update') }}",
			deleteUrl: "{{ Route('admin.role.access.delete') }}",
			order: [[ 0, "desc" ]],
			fields: [{
				label: "{{ __('工作名称') }}",
				name: "name",
			}, {
				label: "{{ __('访问区域') }}",
				name: "modules",
				type: "textarea"
			}, {
				label: "{{ __('许可') }}",
				name: "available",
				type: "radio",
				options : [{ value: "0", label: "{{ __('禁止') }}" }, { value: "1", label: "{{ __('许可') }}" }],
				unselectedValue: ""
			}],
			columns: [{
				title: "{{ __('工作名称') }}",
				data: "name"
			}, {
				title: "{{ __('访问区域') }}",
				data: "modules",
				render: function (data) {
					return data ? data.replaceAll("\n", "<br />") : "{{ __('无') }}";
				}
			}, {
				title: "{{ __('许可') }}",
				data: "available",
				render: function (data) {
					return data ? "<b class='text-success'>许可</b>": "<b class='text-danger'>禁止</b>";
				}
			}, {
				title: "{{ __('职位列表') }}",
				data: "roles",
				render: function (data) {
					return data && data.length > 0 ? data.map(x => x.name).join("<br />") : "{{ __('无') }}";
				}
			}],
			buttons: function(editor) {
				return [{
					extend: "create",
					editor: editor,
					text: "{{ __('增加') }}"
				}, {
					extend: "editSingle",
					editor: editor,
					text: "{{ __('更新') }}"
				}, {
					extend: "remove",
					editor: editor,
					text: "{{ __('删除') }}"
				}, {
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload();
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection