@extends('admins.layouts.app')

@section('header')
	<style>
		.responsive{
			width: 100%!important;
		}
	</style>
@endsection

@section('content')
<div class="page-breadcrumb">
    <div class="row">
        <div class="col-12">
            <h4 class="page-title">职位</h4>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="{{ route('admin.dashboard') }}">看板</a></li>
                    <li class="breadcrumb-item active" aria-current="page">职位</li>
                </ol>
            </nav>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
			<table id="table" class="table table-hover display responsive"></table>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script type="text/javascript">
		const dtOpts = {
			listingUrl: "{{ Route('admin.role') }}",
			createUrl: "{{ Route('admin.role.create') }}",
			updateUrl: "{{ Route('admin.role.update') }}",
			deleteUrl: "{{ Route('admin.role.delete') }}",
			order: [[ 0, "desc" ]],
			fields: [{
				label: "{{ __('职位名称') }}",
				name: "name",
			}, {
				label: "{{ __('工作范围') }}",
				name: "accesses[].id",
				type: "checkbox",
				options : {!! $accesses !!},
				optionsPair: {
					value: "id",
					label: "name"
				}
			}, {
				label: "{{ __('工作范围以外') }}",
				name: "default_access",
				type: "radio",
				options : [{ value: "0", label: "{{ __('全部禁止') }}" }, { value: "1", label: "{{ __('全部许可') }}" }],
				unselectedValue: ""
			}, {
				label: "{{ __('系统类别') }}",
				name: "regional",
				type: "radio",
				options : [{ value: "0", label: "{{ __('主系统') }}" }, { value: "1", label: "{{ __('子系统') }}" }],
				unselectedValue: ""
			}],
			columns: [{
				title: "{{ __('职位名称') }}",
				data: "name"
			}, {
				title: "{{ __('工作范围') }}",
				data: "accesses",
				render: function (data) {
					return data && data.length > 0 ? data.map(x => x.name).join("<br />") : "{{ __('无') }}";
				}
			}, {
				title: "{{ __('工作范围以外') }}",
				data: "default_access",
				render: function (data) {
					return data ? "<b class='text-success'>全部许可</b>": "<b class='text-danger'>全部禁止</b>";
				}
			}, {
				title: "{{ __('系统类别') }}",
				data: "regional",
				render: function (data) {
					return data ? "子系统": "<b>主系统</b>";
				}
			}, {
				title: "{{ __('职员列表') }}",
				data: "admins",
				render: function (data) {
					return data && data.length > 0 ? data.map(x => x.name).join("<br />") : "{{ __('无') }}";
				}
			}],
			buttons: function(editor) {
				return [{
					extend: "create",
					editor: editor,
					text: "{{ __('增加') }}"
				}, {
					extend: "editSingle",
					editor: editor,
					text: "{{ __('更新') }}"
				}, {
					extend: "remove",
					editor: editor,
					text: "{{ __('删除') }}"
				}, {
					text: "{{ __('工作范围管理') }}",
					action: function (e, dt, node, config) {
						window.open("{{ route('admin.role.access') }}");
					}
				}, {
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload();
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection