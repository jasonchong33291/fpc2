<!DOCTYPE html>
<html lang="en">
<head>
  <title>FPC</title>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500, 600,600i,700,700i,800,800i" rel="stylesheet">
  <link href="{{ asset('css/bootstrap-grid.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet"/>
  <link href="{{ asset('css/index.css') }}" rel="stylesheet"/>
  <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">
  <link href="https://unpkg.com/aos@next/dist/aos.css" rel="stylesheet" />
</head>
<body id="page-top">
<nav class="navbar navbar-expand-lg navbar-dark fixed-top" id="mainNav">
  <div class="container">
    <a class="navbar-brand js-scroll-trigger" href="#">
      <h2>Logo</h2>
    </a>
    <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
      <i class="fas fa-bars" style="color: #c37cc6"></i>
    </button>
    <div class="collapse navbar-collapse" id="navbarResponsive">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#games" >Games</a>
        </li>
       <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#features">Features</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#tools">Tools</a>
        </li>
        <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#about-us">About Us</a>
        </li>
		    <li class="nav-item">
          <a class="nav-link js-scroll-trigger" href="#contact-us">Contact Us</a>
        </li>
      </ul>
    </div>
  </div>
</nav>

<section class="game-section" id="games">
  <div class="container">
    <div class="row">
      <div class="col-sm-5">
       <div >
          <h2 class="title">Our Games</h2>
          <p style="margin-bottom: 50px">Unique and fun game play, specially designed to deliver exciting gaming experience to all players.</p>
          <a href="{{ route('lobby') }}" class="btn-demo">Demo Play</a>
       </div>
      </div>
    </div>
  </div>
</section>

<section class="features-section text-center" id="features">
  <div class="container">
    <div class="row">
      <div class="col-sm-12">
        <h2 class="title">5 Features</h2>
        <div class="divider"></div>
      </div>
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm">
            <div>
              <img alt="jackpot" src="{{ asset('images/iocn1-01.png') }}" width="100%" class="iocn">
              <p class="features-title">Jackpot</p>
            </div>
          </div>
          <div class="col-sm">
            <div>
              <img alt="buy_feature" src="{{ asset('images/iocn1-02.png') }}" width="100%" class="iocn">
              <p class="features-title">Buy Feature</p>
            </div>
          </div>
          <div class="col-sm">
            <div>
              <img alt="angbao" src="{{ asset('images/iocn1-03.png') }}" width="100%" class="iocn">
              <p class="features-title">Ang Pau</p>
            </div>
          </div>
          <div class="col-sm">
            <div>
              <img alt="leaderboard" src="{{ asset('images/iocn1-04.png') }}" width="100%" class="iocn">
              <p class="features-title">Leaderboard</p>
            </div>
          </div>
          <div class="col-sm">
            <div>
              <img alt="seasonal_event" src="{{ asset('images/iocn1-05.png') }}" width="100%" class="iocn">
              <p class="features-title">Seasonal Events</p>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section class="retention-section" id="tools">
  <div class="container">
    <div class="row">
      <div class="col-sm-5 ml-auto text-right">
       <div >
          <h2 class="retention-title">Customer Retention</h2>
          <h2 class="retention-title mb-3">Tools</h2>
          <img alt="customer_retention" src="{{ asset('images/glow.png') }}" height="15px" style="margin-bottom: 30px;">
          <p>Player rewards</p>
          <p>Big data</p>
          <p>Machine learning</p>
          <p>Fraud detection</p>
       </div>
      </div>
    </div>
  </div>
</section>

<section class="about-section" id="about-us">
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-5">
        <img alt="about_us" src="{{ asset('images/zeus.png') }}" width="100%">
      </div>
      <div class="col-sm-5 padding-l">
       <div >
        <div class="pl-sm-4">
            <h2 style="color: #000" class="about-title">About Us</h2>
            <div class="divider" style="margin: 0"></div>
        </div>
          <p style="margin-top: 50px; margin-bottom: 30px; color: #000"><strong>UU Slots</strong> is determined to create unique and exciting online video slots and other games, leading innovation in the industry and staying ahead of competitors. Our team is dedicated to provide the best service, to ensure the mutual benefits of all parties and provide peace of mind to our business partners.</p>
       </div>
      </div>
    </div>
  </div>
</section>

<section class="contact-section" id="contact-us">
  <div class="container">
    <div class="row">
      <div class="col-sm-7">
        <div>
          <h2 class="contact-title">Contact Us</h2>
          <div class="divider contact" style="margin: 0"></div>
          <form class="mt-4" onsubmit="() => window.location.href = {{ config('app.url') . '?submitted=1' }};">
            <div class="form-group">
              <input class="input" placeholder="Name" />
            </div>
            <div class="form-group">
              <input class="input" placeholder="Company" />
            </div>
            <div class="form-group">
              <input class="input" placeholder="Email" />
            </div>
            <div class="form-group">
              <input class="input" placeholder="Phone Number" />
            </div>
            <div class="form-group">
              <textarea placeholder="Message" rows="5" class="input-textarea"></textarea>
            </div>
            <button type="submit" class="btn-submit">Submit</button>
          </form>
        </div>
      </div>
      <div class="col-sm-5 text-center">
        <img alt="contact_us" src="{{ asset('images/lady4.png') }}" width="80%">
      </div>
    </div>
  </div>
</section>

<footer style="background: #000; padding: 20px; color: #fff; font-size: 13px">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <span class="d-block">&#169; Copyright © 2022 UU Slots ®</span>
      </div>
    </div>
  </div>
</footer>
</body>
<script src="{{ asset('js/jquery.min.js') }}" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>
<script src="{{ asset('js/bootstrap.bundle.min.js') }}"></script>

<script type="text/javascript">
  $( document ).ready(function() 
    {
      $("a").on('click', function(event) {
        if (this.hash !== "") {
          event.preventDefault();

          // Store hash
          var hash = this.hash;
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
            window.location.hash = hash;
          });
        }
      });
    });
</script>
</html>