@extends('layouts.app')

@section('title', "登录")

@section('theme', "url(" . asset('images/bg-anim.gif') . "); background-color: #303030")
<style>
    .btn-login{
        background: radial-gradient(92.09% 85.42% at 86.3% 87.5%, rgba(0, 0, 0, 0.23) 0%, rgba(0, 0, 0, 0) 86.18%), radial-gradient(65.28% 65.28% at 26.39% 20.83%, rgba(255, 255, 255, 0.38) 0%, rgba(255, 255, 255, 0) 69.79%, rgba(255, 255, 255, 0) 100%), rgb(45, 29, 227);
        width: 100%;
        color: #fff;
        height: 40px;
        border-radius: 5px;
        outline: 0;
        border: 0;
    }
</style>
@section('content')
<div class="container" style="display: flex; align-items: center; justify-content: center; width: 100%; height: 100vh">
    <div class="row justify-content-center w-100">
        <div class="col-md-5" style="margin-top: 50px;">
            <div>
                <img src="{{ asset('images/uuslots-logo.png') }}" width="280px" style="display: block; margin: 0px auto 30px auto" />
            </div>
            <form method="POST" action="{{ route('login') }}" autocomplete="off">
                @csrf

                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="username" type="text" class="form-control @error('username') is-invalid @enderror" name="username" value="{{ old('username') }}" required autofocus placeholder="{{ __('账号') }}">

                        @error('username')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <div class="form-group row">
                    <div class="col-md-12">
                        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required placeholder="{{ __('密码') }}">

                        @error('password')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                        @enderror
                    </div>
                </div>

                <!--
                <div class="form-group row">
                    <div class="col-md-6 offset-md-4">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

                            <label class="form-check-label" for="remember">
                                {{ __('记住我') }}
                            </label>
                        </div>
                    </div>
                </div>
                -->

                <div class="form-group row mb-0">
                    <div class="col-md-12">
                        <button type="submit" class="btn-login">
                            {{ __('登录') }}
                        </button>

                        @if (Route::has('password.request'))
                            <a class="btn btn-link" href="{{ route('password.request') }}">
                                {{ __('Forgot Your Password?') }}
                            </a>
                        @endif
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
