@extends('layouts.app')

@php($previous = true)

@section('button', "返回")

@section('title', "下注记录")

@section('theme', "black")

@section('css')
    <link href="{{ asset('css/datatables.min.css') }}" rel="stylesheet" />
	<style>

		@font-face{
			font-family: SFMonoLight;
			src: url({{  asset('fonts/SFMonoLight.otf') }});
		}


		.background-red {
			color: white;
			background-color: red;
		}
		
		.background-blue {
			color: white;
			background-color: blue;
		}
		
		.background-green {
			color: white;
			background-color: green;
		}
		
		.background-yellow {
			color: white;
			background-color: #ffaf36;
		}
		
		.roulette-result {
			border-radius: 50%;
			height: 35px;
			width: 35px;
			padding: 3px;
			font-size: 12px;
		}
	</style>
@endsection

@section('content')
<div class="container">
    <div class="row" style="margin-top: 50px;">
        <div class="col-md-3">
			<div class="row form-group">
				<div class="col-md-12 col-sm-4" style="margin-bottom: 10px;">
					<select class="form-control" onchange="changeListingUrl('room_id', this.value, 0)">
						<option value="">全部房间</option>
						@foreach($rooms as $room)
						<option value="{{ $room->id }}">{{ $room->name }}</option>
						@endforeach
					</select>
				</div>
				<div class="col-md-12 col-sm-4" style="margin-bottom: 10px;">
					<select class="form-control" onchange="changeListingUrl('result', this.value, 6)">
						<option value="">全部开奖</option>
						<option value="中奖">中奖</option>
						<option value="没中">没中</option>
						<option value="未开奖">未开奖</option>
					</select>
				</div>
			</div>
		</div>
        <div class="col-md-9 col-sm-12">
            <div class="card">
                <div class="card-body">
					<table id="table" class="table table-hover display responsive"></table>
				</div>
			</div>
		</div>
    </div>
</div>
@endsection

@section('js')
	<script src="{{ asset('js/moment.min.js') }}" defer></script>
	<script src="{{ asset('js/datatables.min.js') }}" defer></script>
	<script type="text/javascript">	
		let interval;
		let dListingUrl = "{{ Route('record.betting') }}";
		let qsOptions = {};
		const fpcOrders = {!! json_encode($fpc_order) !!};
		
		function convertQueryString() {
			let qs = "";
			Object.keys(qsOptions).forEach((key, index) => {
				qs += (index == 0 ? "?" : "&") + key + "=" + qsOptions[key];
			});
			return qs;
		}	
		function changeListingUrl(field, val, colIndex = -1) {
			qsOptions[field] = val;
			// reload data
			dTable.ajax.url(dListingUrl + convertQueryString()).load();
			if(colIndex > -1) {
				// show hide column
				const column = dTable.column(colIndex);
				 // Toggle the visibility
				column.visible(val ? false : true);
			}
		}
		const dtOpts = {
			listingUrl: dListingUrl,
			order: [[ 9, "desc" ]],
			// serverSide: true,
			// processing: true,
			columns: [{
				title: "{{ __('房间') }}",
				data: "betting",
				responsivePriority: 7,
				render: function (data, type, row) {
					return data ? data.room.name : (row.next_counter !== null ? "4D" : "已删除");
				}
			}, {
				title: "{{ __('期') }}",
				data: "betting",
				responsivePriority: 6,
				render: function (data, type, row) {
					return data ? data.version : (row.next_counter !== null ? "下" + (row.next_counter + 1) + "期" : "已删除");
				}
			}, {
				title: "{{ __('金额') }}",
				responsivePriority: 1,
				data: "amount"
			}, {
				title: "{{ __('玩法') }}",
				data: "play_name",
				defaultContent: "",
				responsivePriority: 9,
			}, {
				title: "{{ __('选择') }}",
				data: "play_option",
				responsivePriority: 3,
				render: function(data, type, row) {
					if (data) {
						switch (row.play_type) {
							case "fpc":
								return data.split("-").map(x => {
									if (fpcOrders[x]) {
										const img = document.createElement("img");
										img.src = "{{ asset('images') }}/" + fpcOrders[x] + "-01.png";
										img.style.width = "20px";
										return img.outerHTML;
									} else {
										return "-";
									}
								});
							case "roulette":
								const b = document.createElement("b");
								b.className = "background-" + row.roulette_color + " roulette-result";
								b.innerHTML = (data).toString().padStart(2, "0");
								return b.outerHTML;
							default:
								return data;
						}
					} else {
						return "";
					}
				}
			}, {
				title: "{{ __('赔率') }}",
				data: "betting_rates",
				responsivePriority: 8,
				render: function(data) {
					return data !== null && data.length > 0 ? data.join(" - ") : "{{ __('未确认') }}";
				}
			}, {
				title: "{{ __('结算') }}",
				data: "won",				
				responsivePriority: 10,
				render: function(data) {
					return data !== null ? (data) : "未结算";
				}
			}, {
				title: "{{ __('开奖') }}",
				data: "betting",
				responsivePriority: 5,
				render: function(data, type, row) {
					if (data && data.results.length > 0) {
						switch (row.play_type) {
							case "fpc":
								return data.results.map(x => {
									const img = document.createElement("img");
									img.src = "{{ asset('images') }}/" + fpcOrders[x.value] + "-01.png";
									img.style.width = "20px";
									return img.outerHTML;
								});
							case "roulette":
								const b = document.createElement("b");
								b.className = "background-" + data.results[0].roulette_color + " roulette-result";
								b.innerHTML = (data.results[0].value).toString().padStart(2, "0");
								return b.outerHTML;
							default:
								return data.results.map(x => x.value).join(", ");
						}
					} else {
						return "未开奖";
					}
				}
			}, {
				title: "{{ __('开奖结果') }}",
				data: "result",
				responsivePriority: 4,
				render: function(data) {
					return data > -1 ? (data > 0 ? "中奖" : "没中") : "未开奖";
				}
			}, {
				title: "{{ __('日期') }}",
				data: "created_at",
				responsivePriority: 2,
				render: function (data) {
					return data ? new Date(data).toLocaleString() : "";
				}
			}],
			buttons: function(editor) {
				return [{
					text: "{{ __('刷新') }}",
					action: function (e, dt, node, config) {
						dt.ajax.reload(null, false);
					}
				}];
			}
		};
	</script>
	<script src="{{ asset('js/dt.js') }}" defer></script>
@endsection