@extends('layouts.app')

@section('button', "大厅")

@section('title', $room->name)

@section('title_other', "")

@section('css')
<style>
	*{
		font-family: 'Roboto', sans-serif;
		font-weight:  400;
	}

	.form-group{
		margin-bottom:  0!important;
	}
	.rounded-card {
		border-radius: 12px!important;
	}
	.shadow {
		box-shadow: 0 .5rem 1rem rgba(0,0,0,.15)!important;
	}
	.border-0 {
	border: 0!important;
	}

	.btn-red{
		color:  #fff;
		border-radius: 20px;
		padding: 0.375rem 1rem;
	font-size: 1rem;
		background-color: red;
	}

	.btn-red:hover{
		color:  #fff;
	}

	.card {
	    position: relative;
	    display: flex;
	    flex-direction: column;
	    min-width: 0;
	    background-color: #fff;
	    background-clip: border-box;
	    border: 1px solid rgba(0,0,0,.125);
	}

	.padding-checkbox{
		padding:  0 2.5px;
	}
	.bg-black{
		background: #000;
		color:  #fff!important;
	}
	
	.four-d-checkbox {
		box-shadow: 0 0.5rem 1rem rgba(0,0,0,.15);
		background: white;
		border-radius: 9999px;
		padding: 0.25rem;
	}
	
	.four-d-checkbox.active {
		background: red;
		color: white;
	}
	
	.four-d-checkbox-badge {
		border-radius: 50%;
		width: 100%;
	}
	
	.four-d-option {
		border-radius: 8px;
		box-shadow: 0 0.5rem 1rem rgba(0,0,0,.15);
		text-align: center;
		padding: 2.5px 0;
	}	
	
	.four-d-option.disabled {
		opacity: 0.5;
	}
	
	.four-d-option b {
		font-size: 20px;
	}
	
	.four-d-radio {
		box-shadow: 0 0.5rem 1rem rgba(0,0,0,.15);
		background: white;
		border-radius: 9999px;
		padding: 5% 20%;		
	}
	
	.four-d-radio.active {
		background: red;
		color: white;
	}
	
	.four-d-radio-circle {
		border-radius: 50%;
		background: red;
		width: 100%;
	}
	
	.four-d-radio-circle.disabled {
		background: grey;
	}
	
	.active .four-d-radio-circle {
		background: white;
	}
	
	.four-d-number-container {
		margin: 5px 0px;
		border-radius: 8px;
		text-align: center;
		padding: 15px;
		opacity: 0.5;
		box-shadow: 0 0.5rem 1rem rgba(0,0,0,.15);
	}
	
	.four-d-number-icon {
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		right: 25px;
		cursor: pointer;
	}
	
	.four-d-number-container.selected {
		opacity: 1;
	}
	
	.four-d-number-input-container {
		border-radius: 12px;
		position: relative;
		background: red;
		color: white;
		text-align: center;
	}
	
	.four-d-number-input-placeholder {
		font-size: 50px;
		white-space: nowrap;
		letter-spacing: 1rem;
		line-height: 1.1;
	}
	
	.four-d-number-input-label {
		position: absolute;
		top: 15px;
		left: 15px;
		font-weight:  300;
		font-size: 23px;
		line-height: 1;
	}	
	
	.four-d-total-container {
		margin-top: 30px;
		border-radius: 12px;
		background: red;
		color: white;
		text-align: center;
		padding: 7.5px 30px;
	}
	
	.four-d-total-placeholder {
		font-size: 7.5vw;
		white-space: nowrap;
		letter-spacing: 5px;
		font-weight:  300;
		line-height: 1.1;
	}
	
	.four-d-total-day {
		color: #E6D5CC;
	}
	
	.four-d-total-label {
		font-size: 20px;
		line-height: 1;
	}
	
	.four-d-total-icon {
		color: white;
		font-size: 16px;
		display: inline-block;
	}
	
	.four-d-total-icon:hover {
		color: white;
		text-decoration: none;
	}
	
	.four-d-select-container {
		position: relative;
	}
	
	.four-d-select-label {
		position: absolute;
		top: 50%;
		transform: translateY(-50%);
		left: 5px;
		font-size: 20px;
		line-height: 1;
	}

	.font-small{
		font-size: 12px;
	}
	
	.four-d-select {
	}

	.four-d-option-value{
		line-height:  1.1;
	}
	
	.col-of-5 {
		width: 19%;
	}
	
	.col-of-7 {
		width: 12%;
	}

	@media(max-width:  414px){
		.four-d-number-input-placeholder{
			line-height:  1.1;
			font-size: 55px ;
		}

		.four-d-radio{
			font-size: 6px;
		}

		.four-d-option div{
			font-size: 10px;		
		}
	}
	
	@media(min-width: 1000px) {
		.four-d-total-placeholder {
			font-size: 50px;
		}
	}
	
	.btn-normal {
		background-color: #aaa;
		color: white !important;
	}
	
	.btn-normal:hover {
		background-color: #919191;
	}
	
	.btn-normal:active {
		background-color: #919191;
	}
	
	.btn-delete {
		background-color: #F57A00;
		color: white !important;
	}
	
	.btn-delete:hover {
		background-color: #cf6700;
	}
	
	.btn-delete:active {
		background-color: #c26100;
	}
	
	.btn-add {
		background-color: #EB6864;
		color: white !important;
	}
	
	.btn-add:hover {
		background-color: #e74742;
	}
	
	.btn-add:active {
		background-color: #e53c37;
	}
	
	.tutorial {
		position: fixed;
		top: 50%;
		left: 50%;
		transform: translate(-50%, -50%);
		background: black;
		width: 100%;
		height: 100%;
		display: none;
	}
	
	.tutorial .cancel {
		position: fixed;
		top: 10px;
		right: 10px;
		width: 1%;
		color: white;
		text-shadow: 1px 1px black;
		z-index: 2;
		cursor: pointer;
		font-size: 30px;
		line-height: 1;
	}
	
	.tutorial .carousel {
		height: 100%;
	}
	
	.tutorial .carousel-item {
		width: auto;
		margin-left: calc(50% - 391px / 2);
	}
</style>
@endsection

@section('content')
<div class="container">
	<div class="row">
		<div class="col-12">
			<div class="four-d-total-container">
				<div class="row p-0">
					<div class="col-3 p-0 m-auto">
						<div class="four-d-total-label">Total Point</div>
					</div>
					<div class="col-6 p-0 m-auto">
						<div id="fourDTotal" class="four-d-total-placeholder">0.00</div>
						<div id="fourDMultiDay" class="four-d-total-day"></div>
					</div>
					<div class="col-3 col-sm-3 col-md-3 text-center text-sm-right m-auto p-0">
						<a class="four-d-total-icon mr-sm-2" href="#" onclick="showTutorial()"><i class="feather icon-help-circle"></i> Tutorial</a>
					
						<a class="four-d-total-icon mr-sm-2" href="{{ route('record.betting.list', ['id' => $room->id]) }}"><i class="feather icon-list"></i> Record</a>
						
						<a class="four-d-total-icon" href="{{ route('result', ['id' => $room->id]) }}"><i class="feather icon-gift"></i> Result</a>
					</div>
				</div>
			</div>
		</div>
	</div>
	
	<div class="row py-3">
		<div class="ml-auto col-6 col-sm-10 text-right my-auto">
			<div class="d-flex my-auto">
				<img src="{{ asset('images/balance.svg') }}" width="30"/>
				<div class="ml-2" style="font-size: 18px"><b>$ {{ number_format($user->totalBalance, 2) }}</b></div>
			</div>
		</div>
		<div class="ml-auto col-6 col-sm-2 text-right">
			<div class="form-group">
				<div class="four-d-select-container">
					<div class="dropdown">
						  <a class="btn btn-red dropdown-toggle w-100" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
						    Days <span id="day" class="ml-2">1</span>
						  </a>

						  <div class="dropdown-menu" aria-labelledby="dropdownMenuLink" id="dayOption">
						  	@foreach([1, 2, 3, 4, 5, 6, 7, 8, 9] as $day)
						    <a class="dropdown-item">{{ $day }}</a>
							@endforeach
						  </div>
						</div>
				</div>
			</div>
		</div>
	</div>
	<div class="card rounded-card shadow border-0">
		<div class="card-body p-2">
			<div class="row">
				<div class="col-12 text-center pb-2">
					<div class="row mx-0">
						@foreach($four_d as $key => $value)
						<div class="col col-of-7 padding-checkbox">
							<div id="type_{{ $key }}" class="btn four-d-checkbox{{ $key == 'M' ? '' : '' }}">
								<img class="four-d-checkbox-badge" src="{{ asset('images/4d/' . $value . '.png') }}" />
								<input {{ $key == 'M' ? '' : '' }} type="checkbox" name="type" value="{{ $key }}" style="display: none;" />
								<br />
								{{ $key }}
							</div>
						</div>
						@endforeach
					</div>
				</div>
			</div>
			<div class="row pt-2">
				<div class="col-12">
					<div class="form-group">
						<div class="four-d-number-input-container p-1" onclick="openNumberPad()">
							<div class="four-d-number-input-label">2D</div>
							<div id="fourDNumber" class="four-d-number-input-placeholder">__</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="row pt-4  mx-0">
		<div class="col-5 col-sm-6" style="margin: auto;">
			<div class="row">
				@foreach(["B" => "BIG", "S" => "SMALL", "SA" => "2A"] as $key => $option)
			<div class="col-4 padding-checkbox">
				<div class="form-group">
					<div class="four-d-option {{ $key == 'SA' ? 'disabled' : '' }}" onclick="openOptionPad(this, '{{ $key }}', '{{ $option }}')">
						<div>{{ $option }}</div>
						<div><b id="option_{{ $key }}" data-short="{{ $key }}" class="four-d-option-value">0</b></div>
					</div>
				</div>
			</div>
			@endforeach
			</div>
		</div>
		<div class="col-7 col-sm-6">
			<div class="form-group">
				<div class="row">
					@foreach(["BH", "REV", "BOX", "IBOX", "BT"] as $value)
					<div class="col col-of-5 padding-checkbox">
						<div id="sub_type_{{ $value }}" class="btn four-d-radio w-100 {{ $value == 'BOX' || $value == 'IBOX' ? '' : 'disabled' }}">
							<input type="radio" name="sub_type" value="{{ $value }}" style="display: none;" />
							{{ $value }}
							<br />
							<img class="four-d-radio-circle" src="{{ asset('images/4d/circle.svg') }}" />
						</div>
					</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>

	<div id="number_container" style="margin: 7.5px 0;">
		<div class="row">
			<div class="col-12 position-relative">
				<div class="four-d-number-container selected text-center" onclick='selectNumber()'>
					Not Set
				</div>
				<i class="four-d-number-icon feather icon-x mr-4 text-danger" onclick="deleteNumber()"></i>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-8 pr-2">
			<button id="addNumberBtn" class="btn btn-lg btn-block btn-danger w-100">Add Number</button>
		</div>
		<div class="col-4 pl-0">
			<button id="betBtn" disabled class="btn btn-lg btn-block btn-success w-100">Bet</button>
		</div>
	</div>
	<div class="row">
		<div class="col-12 py-3 text-center">Place bet first</div>
	</div>
	<div class="row">
		<div class="col-12 py-3 text-center">
			<button onclick="preHandleBack()" class="btn btn-lg btn-danger">Return to Wallet</button>
		</div>
	</div>
	<div class="row" style="margin-top: 50px;">
		<div class="col-12 text-center">
			- CLOSING TIME -
			<br />
			Fortune Star 7pm
			<br />
			Singapore 6:25pm
			<br />
			Malaysia 6:58pm
		</div>
	</div>
</div>
<div id="toastContainer" style="display: none; position: fixed; bottom: 0; width: 100%; left: 0; text-align: center">
	<div id="toastMessage"></div>
</div>
<div id="optionPadModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content">
      <div class="modal-body">
				<div class="row mb-3">
						<div class="col-12">
							<div  style="border: 1px solid #ddd; border-radius: 10px" class="p-3">
								<div class="row">
									<div class="col-3 text-center mt-auto">
										<div id="optionPadTitle" class="text-danger mb-0" style="font-size: 18px;"></div>
									</div>
									<div class="col-9 text-center">
										<h2 style="font-weight: 500; font-size:  font-size: 3.5rem;" id="optionPadValue" class="mb-0"></h2>
									</div>
								</div>
							</div>
						</div>
				</div>
		<div class="row px-2">
			@foreach([7, 8, 9, 4, 5, 6, 1, 2, 3] as $text)
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-normal btn-lg font-weight-bold" onclick="optionClick('optionPad', '{{ $text }}')">{{ $text }}</button>
			</div>
			@endforeach
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-normal btn-lg font-weight-bold" onclick="optionClick('optionPad', '.')">.</button>
			</div>
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-normal btn-lg font-weight-bold" onclick="optionClick('optionPad', '0')">0</button>
			</div>
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-delete btn-lg font-weight-bold" onclick="optionDelete('optionPad')">
					<i class='feather icon-delete'></i>
				</button>
			</div>
			<div class="col-3 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-success btn-lg font-weight-bold" onclick="optionPadSecondary()">All</button>
			</div>
			<div class="col-9 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-success btn-lg font-weight-bold" onclick="optionPadPrimary()">Save</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>


<div id="numberPadModal" class="modal" tabindex="-1" role="dialog">
  <div class="modal-dialog modal-dialog-centered" role="document">
    <div class="modal-content bg-black">
      <div class="modal-body">
      	<div style="background: url({{  asset('images/4dplay-bg.jpg') }}); background-size:  100% 100%; min-height: 150px; border-bottom: 0">
	      	<div class="row">
	      		<div class="col-9">
	      			<small class="d-block font-small">New Experience</small>
					<h2  style="font-weight: 500;" id="numberPadTitle" class="modal-title"></h2>
	      		</div>
		      	<div class="col-3">
			        <button type="button" class="close" data-dismiss="modal" aria-label="Close"  style="color: #fff; font-size: 32px; opacity: 1">
			          <span aria-hidden="true">&times;</span>
			        </button>
		      	</div>
	      	</div>
	      </div>
			<div class="row mb-3">
				<div class="col-12 text-center">
					<h3 style="font-weight: 700; text-align: center">Key in your lucky number</h3>
				</div>
				<div id="numberPadValue" class="col-6 col-sm-4 mx-auto four-d-number-input-placeholder text-center"></div>
			</div>
		<div class="row px-2">
			@foreach([7, 8, 9, 4, 5, 6, 1, 2, 3] as $text)
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-normal btn-lg font-weight-bold" onclick="numberClick('numberPad', '{{ $text }}')">{{ $text }}</button>
			</div>
			@endforeach
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button id="hashBtn" type="button" class="btn btn-block btn-normal btn-lg font-weight-bold" onclick="numberClick('numberPad', '#')">0-9</button>
			</div>
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-normal btn-lg font-weight-bold" onclick="numberClick('numberPad', '0')">0</button>
			</div>
			<div class="col-4 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-delete btn-lg font-weight-bold" onclick="numberDelete('numberPad')">
					<i class='feather icon-delete'></i>
				</button>
			</div>
			<div class="col-3 px-1" style="margin: 5px 0;">
				<button type="button" class="btn btn-block btn-add btn-lg font-weight-bold" onclick="numberPadSecondary()"><i class="feather icon-plus"></i></button>
			</div>
			<div class="col-9 px-1" style="margin: 5px 0;">
				<button id="primaryBtn" disabled type="button" class="btn btn-block btn-success btn-lg font-weight-bold" onclick="numberPadPrimary()">Save</button>
			</div>
		</div>
      </div>
    </div>
  </div>
</div>

<div id="tutorial" class="tutorial">
	<a class="cancel" href="#" onclick="closeTutorial()">x</a>
	<div id="silder" class="carousel slide" data-ride="carousel">
		@php($tutorials = ["bet_amount.jpg", "amount_left.jpg", "consecutive_days.jpg", "brand_choice.jpg", "numbers.jpg", "big_small.jpg", "add_on.jpg", "add_bet.jpg", "bet.jpg"])
		<ol class="carousel-indicators">
		@foreach($tutorials as $index => $tutorial)
			<li data-target="#silder" data-slide-to="{{ $index }}" class="{{ $index == 0 ? 'active' : '' }}"></li>
		@endforeach
		</ol>
		<div class="carousel-inner">
		@foreach($tutorials as $index => $tutorial)
		<div class="carousel-item{{ $index == 0 ? ' active' : '' }}">
		  <img class="d-block w-100" src="{{ asset('images/tutorial/' . $tutorial) }}" alt="Tutorial {{ $index + 1 }}" />
		</div>
		@endforeach
		</div>
	  <a class="carousel-control-prev" href="#silder" role="button" data-slide="prev">
		<span class="carousel-control-prev-icon" aria-hidden="true"></span>
		<span class="sr-only">Previous</span>
	  </a>
	  <a class="carousel-control-next" href="#silder" role="button" data-slide="next">
		<span class="carousel-control-next-icon" aria-hidden="true"></span>
		<span class="sr-only">Next</span>
	  </a>
	</div>
</div>
@endsection

@section('js')
@endsection

@section('footer-js')
	<script type="text/javascript">
		document.getElementById("dayOption")?.addEventListener("click",function(e) {
			const input = e.target;
			$('#day').text(input.innerText);
			updateTotal();
		});
		
		const betBtn = document.getElementById("betBtn");
		
		let toastTimeout;
		
		function showToastMessage(message, status = "success") {
			const toastContainer = document.getElementById("toastContainer");
			const toastMessage = document.getElementById("toastMessage");
			
			toastMessage.innerText = message;
			toastMessage.className = "alert alert-" + status;
			
			if (toastTimeout) {
				clearTimeout(toastTimeout);
			} else {
				toastContainer.style.display = "block";
			}
			
			toastTimeout = setTimeout(function() {
				toastTimeout = null;
				toastContainer.style.display = "none";
			}, 1000);
		}
		
		function updateTotal() {
			let total = 0;
			for(let itemEleNative of $(".four-d-number-container")) {
				const itemEle = $(itemEleNative);
				const amountEle = itemEle.find(".four-d-selected-amount");
				const amount = parseFloat(amountEle.text());
				if (!isNaN(amount)) {
					total += amount;
				}
			}
			const day = parseInt($("#day").text());
			if (day > 1) {
				$("#fourDMultiDay").text(total.toFixed(2) + " x " + day + "D");
				total = total * day;				
			} else {
				$("#fourDMultiDay").text("");
			}
			$("#fourDTotal").text(total.toFixed(2));
		}
		
		function generateColumn(itemEle, { type = "Not Set", number = "Not Set", option = "Not Set", subType = "", amount = "0.00", append = true }) {
			const typeEle = $("<div class='col-2 padding-checkbox four-d-selected-type font-small'>" +
				type +
			"</div>");
			const numberEle = $("<div class='col-2 padding-checkbox text-center four-d-selected-number font-small'>" +
				number +
			"</div>");
			const optionEle = $("<div class='col-2 padding-checkbox text-center four-d-selected-option font-small'>" +
				option +
			"</div>");
			const subTypeEle = $("<div class='col-2 padding-checkbox text-center four-d-selected-sub-type font-small'>" +
				subType +
			"</div>");
			const amountEle = $("<div class='col-3 padding-checkbox four-d-selected-amount font-small'>" +
				amount +
			"</div>");
			const content = "<div class='row'>" +
				typeEle[0].outerHTML + numberEle[0].outerHTML + optionEle[0].outerHTML +
				subTypeEle[0].outerHTML + amountEle[0].outerHTML +
			"</div>";
			if (itemEle) {
				itemEle.html(content);
				updateTotal();
			} else {
				return content;
			}
		}
		
		function updateRow(itemEle, { type, number, option, subType }) {
			const typeEle = itemEle.find(".four-d-selected-type");
			const numberEle = itemEle.find(".four-d-selected-number");
			const optionEle = itemEle.find(".four-d-selected-option");
			const subTypeEle = itemEle.find(".four-d-selected-sub-type");
			const amountEle = itemEle.find(".four-d-selected-amount");
			if (type !== undefined) {
				typeEle.text(type);
			}
			if (number !== undefined) {
				numberEle.text(number);
			}
			if (option !== undefined) {
				optionEle.text(option);
			}
			if (subType !== undefined) {
				subTypeEle.text(subType);
			}
			if (typeEle.text() === "Not Set" ||
				numberEle.text() === "Not Set" ||
				optionEle.text() === "Not Set") {
				amountEle.text("0.00");
				betBtn.disabled = true;
			} else {
				let numbers = numberEle.text().split("");
				let groupedNumbers = [];
				numbers.forEach(number => {
					if (groupedNumbers.indexOf(number) === -1) {
						groupedNumbers.push(number);
					}
				});
				let subTypeRate;
				switch(subTypeEle.text()) {
					case "BOX":
						if (groupedNumbers.length === 1) {
							subTypeRate = 1;
						} else {
							subTypeRate = 2;
						}
						break;
					case "IBOX":
					case "BH":
					case "REV":
					case "BT":
					default:
						subTypeRate = 1;
				}
				amountEle.text((typeEle.text().length * optionEle.text().split(" ").reduce((pv, obj) => pv + parseFloat(obj), 0)
					* subTypeRate * (numberEle.text().indexOf("#") > -1 ? 10 : 1)).toFixed(2));
				betBtn.disabled = false;
			}
			updateTotal();
		}
		
		function onClickCheckbox(evt) {
			const btn = $(this).find("input");
			if (btn[0].checked) {
				$(this).removeClass("active");
				btn[0].checked = false;
			} else {
				$(this).addClass("active");
				btn[0].checked = true;
			}			
			const itemEle = $(".four-d-number-container.selected");
			if (itemEle) {
				const options = $("input[name=type]:checked").serializeArray();
				if (itemEle[0].children.length > 0) {
					updateRow(itemEle, { type: options.map(x => x.value).join("") });
				} else {
					generateColumn(itemEle, { type: options.map(x => x.value).join("") });
				}
			}
		}
		
		function onClickRadio(evt) {
			if ($(this).hasClass("disabled")) {
				return;
			}
			const btn = $(this).find("input");
			$(".four-d-radio").removeClass("active");
			if (!btn[0].checked) {
				$(this).addClass("active");
				btn[0].checked = true;
			} else {
				$(this).removeClass("active");
				btn[0].checked = false;
			}
			const itemEle = $(".four-d-number-container.selected");
			if (itemEle) {
				const option = $("input[name=sub_type]:checked").val();
				if (itemEle[0].children.length > 0) {
					updateRow(itemEle, { subType: option || "" });
				} else {
					generateColumn(itemEle, { subType: option || "" });
				}
			}
		}
		
		function selectNumber() {
			$(".four-d-number-container").removeClass("selected");
			$(event.currentTarget).addClass("selected");
			// get selected
			const itemEle = $(".four-d-number-container.selected");
			const typeEle = itemEle.find(".four-d-selected-type");
			const numberEle = itemEle.find(".four-d-selected-number");
			const optionEle = itemEle.find(".four-d-selected-option");
			const subTypeEle = itemEle.find(".four-d-selected-sub-type");
			// set number
			const number = parseInt(numberEle.text());
			$("#fourDNumber").text(isNaN(number) ? "__" : numberEle.text());
			// set type
			$(".four-d-checkbox").removeClass("active");
			const typeOptions = typeEle.text().split("");
			const types = typeEle.text().split("");
			if (types[0] != "") {
				types.forEach(type => {
					const option = typeOptions.find(x => x.indexOf(type) > -1);
					if (option) {
						$("#type_" + type).addClass("active");
						$("#type_" + type).find("input")[0].checked = true;
					} else {
						$("#type_" + type).removeClass("active");
						$("#type_" + type).find("input")[0].checked = false;
					}
				});
			}
			// set option
			const subTypeOptions = optionEle.text() !== "Not Set" ? optionEle.text().split(" ") : [];
			const subTypes = ["B", "S", "SA"];
			subTypes.forEach(type => {
				const option = subTypeOptions.find(x => x.endsWith(type) > -1);
				if (option) {
					$("#option_" + type).text(option.replace(type, ""));
				} else {
					$("#option_" + type).text(0);
				}
			});
			// set sub type
			$(".four-d-radio").removeClass("active");
			if (subTypeEle.text() !== "Not Set") {
				$("#sub_type_" + subTypeEle.text()).addClass("active");
				$("input[name=subType]").val(subTypeEle.text());
			} else {
				$("input[name=subType]").val("");
			}
		}
		
		function deleteNumber() {
			$(event.currentTarget).parents(".row").remove();
			updateTotal();
			if ($("#number_container")[0].children.length === 0) {
				$("#number_container").append("<div class'empty'>No more number, please add.</div>");
				betBtn.disabled = false;
			}
		}
		
		function addNumber() {
			$(".four-d-number-container").find(".empty")?.remove();
			const itemEle = $(".four-d-number-container.selected");
			$(".four-d-number-container").removeClass("selected");
			const typeEle = itemEle.find(".four-d-selected-type");
			if (typeEle.length > 0) {
				const numberEle = itemEle.find(".four-d-selected-number");
				const optionEle = itemEle.find(".four-d-selected-option");
				const subTypeEle = itemEle.find(".four-d-selected-sub-type");
				const amountEle = itemEle.find(".four-d-selected-amount");
				const newItemEle = $("<div class='four-d-number-container selected text-center' onclick='selectNumber()'>" +
						generateColumn(null, { type: typeEle.text(), option: optionEle.text()
					, number: "", subType: subTypeEle.text(), amount: amountEle.text() }) +
				"</div>");
				const row = $("<div class='row'>" +
					"<div class='col-12'>" +
						newItemEle[0].outerHTML +
						"<i class='four-d-number-icon feather icon-x' onclick='deleteNumber()'></i>" +						
					"</div>" +
				"</div>");
				$("#number_container").append(row);
				updateTotal();
			} else {
				const row = $("<div class='row'>" +
					"<div class='col-12'>" +
						"<div class='four-d-number-container selected text-center' onclick='selectNumber()'>" + 
							"Not Set" +
						"</div>" +
						"<i class='four-d-number-icon feather icon-x' onclick='deleteNumber()'></i>" +
					"</div>" +
				"</div>");
				$("#number_container").append(row);
			}
		}
		
		function getRecords() {
			const records = [];
			const eles = $(".four-d-number-container");
			for(let i = 0; i < eles.length; i++) {
				const itemEleNative = eles[i];
				const itemEle = $(itemEleNative);
				const typeEle = itemEle.find(".four-d-selected-type");
				const numberEle = itemEle.find(".four-d-selected-number");
				const optionEle = itemEle.find(".four-d-selected-option");
				const subTypeEle = itemEle.find(".four-d-selected-sub-type");
				const amountEle = itemEle.find(".four-d-selected-amount");
				const types = typeEle.text().split("");
				for (let type of types) {
					const data = {
						option: optionEle.text() + " " + subTypeEle.text(),
						type: type,
						value: numberEle.text(),
						amount: parseFloat(amountEle.text()) / types.length,
						group: i
					};
					records.push(data);
				}
			}
			return records;
		}
		
		function bet() {
			betBtn.disabled = true;				
			return $.ajax({
				url: "{{ route('room.bet.4d', ['id' => $room->id]) }}",
				type: "POST",
				data: {
					records: getRecords(),
					rebet: $("#day").text()
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				betBtn.disabled = false;
				window.location.href = "{{ route('record.betting.list', ['id' => $room->id]) }}";
			}).fail(function(err) {
				betBtn.disabled = false;
				showToastMessage(err?.responseJSON?.message || err.responseText, err.status === 420 ? "info" : "danger");
			});
		}
		
		function toggleBtn() {			
			if (modalValue.indexOf("_")  > -1 || modalValue.indexOf("##") > -1) {
				$("#primaryBtn")[0].disabled = true;
			} else {
				$("#primaryBtn")[0].disabled = false;
			}
		}
		
		const maxDigit = 2;
		let modalValue = "";
		let valueStart = undefined;
		let optionKey = undefined;
		
		function optionClick(modal, value) {
			if (valueStart !== undefined) {
				if (valueStart < maxDigit) {
					modalValue = modalValue.substring(0, valueStart) + value + modalValue.substring(valueStart + 1);
					valueStart++;
				}
			} else {
				if (modalValue === "0") {
					modalValue = value;
				} else {
					modalValue += value;
				}
			}
			$("#" + modal + "Value").text(modalValue);
		}
		
		function numberClick(modal, value) {
			optionClick(modal, value);
			toggleBtn();
		}
		
		function optionDelete(modal) {
			if (valueStart !== undefined) {
				if (valueStart > 0) {
					modalValue = modalValue.substring(0, valueStart - 1) + "_" + modalValue.substring(valueStart);
					valueStart--;
				}
			} else {
				modalValue = modalValue.substring(0, modalValue.length - 1);
				if (modalValue.length === 0) {
					modalValue = "0";
				}
			}
			$("#" + modal + "Value").text(modalValue);
		}
		
		function numberDelete(modal) {
			optionDelete(modal);
			toggleBtn();
		}
		
		function numberReset(modal, text = "") {			
			modalValue = text;
			$("#" + modal + "Value").text(text);
		}
		
		function showModal(modal, title) {
			const modalTitle = $("#" + modal + "Title");
			modalTitle.text(title);
			$('#' + modal + "Modal").modal('show');
		}
		
		function hideModal(modal) {
			$('#' + modal + "Modal").modal('hide');
		}
		
		function numberSubmit(eleId) {			
			$("#" + eleId).text(modalValue);
			const itemEle = $(".four-d-number-container.selected");
			if (itemEle) {
				if (itemEle[0].children.length > 0) {
					updateRow(itemEle, { number: modalValue });
				} else {
					generateColumn(itemEle, { number: modalValue });
				}
			}
		}
		
		function openNumberPad() {
			numberReset('numberPad', $("#fourDNumber").text());
			const index = modalValue.indexOf("_");
			valueStart = index > -1 ? index : modalValue.length;
			showModal('numberPad', "4D");
		}
		
		function numberPadPrimary() {
			numberSubmit("fourDNumber");
			hideModal('numberPad');
		}
		
		function numberPadSecondary() {
			numberSubmit("fourDNumber");
			addNumber();
			numberReset('numberPad', "__");
			valueStart = 0;
		}
		
		function optionSubmit(itemEle) {
			if (itemEle) {
				const options = $(".four-d-option-value")
				.filter((i, x) => parseFloat(x.innerText) > 0)
				.map((i, x) => x.innerText + x.dataset.short)
				.toArray();
				if (itemEle[0].children.length > 0) {
					updateRow(itemEle, { option: options.join(" ") });
				} else {
					generateColumn(itemEle, { option: options.join(" ") });
				}
			}
		}
				
		function openOptionPad(ele, key, text) {
			if ($(ele).hasClass("disabled")) {
				return;
			}			
			optionKey = key;
			numberReset('optionPad', $("#option_" + key).text());
			valueStart = undefined;
			showModal('optionPad', text);
		}
		
		function optionPadPrimary() {
			$("#option_" + optionKey).text(modalValue);
			const itemEle = $(".four-d-number-container.selected");
			optionSubmit(itemEle);
			hideModal('optionPad');
		}
		
		function optionPadSecondary() {
			for(let itemEleNative of $(".four-d-number-container")) {
				optionSubmit($(itemEleNative));
			}
			hideModal('optionPad');
		}
		
		const checkboxs = document.getElementsByClassName("four-d-checkbox");
		for(let checkbox of checkboxs) {
			checkbox.addEventListener("click", onClickCheckbox);
		}
		
		const radios = document.getElementsByClassName("four-d-radio");
		for(let radio of radios) {
			radio.addEventListener("click", onClickRadio);
		}
		
		const addNumberBtn = document.getElementById("addNumberBtn");
		addNumberBtn.addEventListener("click", addNumber);
		betBtn.addEventListener("click", bet);
		
		function closeTutorial() {
			$('#tutorial').hide();
		}
		
		function showTutorial() {
			$('#tutorial').show();
		}
		
		function preHandleBack() {
			Swal.fire({
				icon: 'warning',
				title: "Return to wallet",
				text: "Are you sure you want to leave?",
				showCloseButton: true,
				showCancelButton: true,
				confirmButtonText: "Yes",
				cancelButtonText: "No"
			}).then(res => {
				if (res.isConfirmed) {
					handleBack();
				}
			});
		}
	</script>
@endsection