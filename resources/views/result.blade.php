@extends('layouts.app')

@php($previous = true)
@php($navbar = true)
@php($backRoute = route('room', ['id' => $room_id]))

@section('theme', "theme-red")

@section('title', "Result")

@section('css')
<style>
	*{
		font-family: 'Roboto', sans-serif;
		font-weight:  400;
	}

	.date-arrow {
		font-size: 20px;
	}
	
	.date-arrow i {
		font-size: 30px;
	}
	
	.date-input {
		height: calc(1.5em + 0.5rem + 2px);
	    padding: 0.25rem 0.5rem;
	    font-size: .875rem;
	    line-height: 1.5;
	    border-radius: 0.2rem;
	}
		
	.four-d-radio {
		border-radius: 2px;
		line-height: 1.2;
		padding: 0.25rem;
		cursor: pointer;
	}
	
	.four-d-radio.active {
		background: #EB6864;
		color: white;
	}
	
	.four-d-radio-badge {
		border-radius: 50%;
		width: 100% !important;
	}
	
	.table td {
		border: 1px solid rgba(0, 0, 0, 0.15);
	}
	
	.line-top td {
		border-top: 2px solid rgba(0, 0, 0, 0.15);
	}
	
	.line-bottom td {
		border-bottom: 2px solid rgba(0, 0, 0, 0.3);
	}

	.padding-checkbox{
		padding:  0 2.5px;
	}

	.four-d-jackpot{
		color:  #E7C56C;
		font-weight: 700;
	}
	@media(max-width:  414px){
		.four-d-radio-badge{
			width: 30px;
		}
	}

	td{
		line-height: 1.5;
		color: #222;
	}
	
	.col-inline {
		width: 12%;
	}
	
	.refresh {
		cursor: pointer;
	}
</style>
<link href="{{ asset('css/flatpickr.min.css') }}" rel="stylesheet" />
<link href="{{ asset('css/flatpickr-theme.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="container" style="margin-top: 5px;">
	<div class="row">
		<div class="col-4 text-right mt-auto mb-auto date-arrow">
			<a onclick="prevDate()" style="cursor: pointer; font-size: 1rem"><i class="feather icon-chevron-left align-middle"></i>Prev</a>
		</div>
		<div class="col-4 px-0 mt-1">
			<input onchange="loadResult()" id="date" class="text-center form-control date-input" readonly input="date" value="{{ \Carbon\Carbon::now()->format('Y-m-d') }}" />
		</div>
		<div class="col-4 mt-auto mb-auto date-arrow">
			<a onclick="nextDate()" style="cursor: pointer; font-size: 1rem">Next<i class="feather icon-chevron-right align-middle"></i></a>
		</div>
	</div>
	<div class="row pt-2">
		<div class="col-12 text-center">
			<div class="row mx-0">
			@foreach($four_d as $key => $value)
				<div class="col col-inline padding-checkbox">
					<div class=" four-d-radio{{ $key == 'M' ? ' active': '' }}">
						<img class="four-d-radio-badge" src="{{ asset('images/4d/' . $value . '.png') }}" />
						<input type="radio" name="type"{{ $key == 'M' ? ' checked': '' }} value="{{ $key }}" style="display: none;" />
						<br />
						{{ $key }}
					</div>
				</div>
				@endforeach
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<div class=" card card-body mt-3 mb-3 p-2 four-d-jackpot text-center" style="background-color: #9f1923;">
				4D Jackpot Pool
				<h5 style="font-size: 24px; font-weight: 700;" class="mb-0">1,234,567.89</h5>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-12">
			<table class="table">
				<tr>
					<td colspan="2" class="py-1">
						<div class="d-flex">
							<div id="drawDate" class="w-50"  style="font-size: 1.2em; font-weight: 700"></div>
							<div id="drawNo" class="w-50" style="display: inline-block; font-size: 1.2em; font-weight: 700; text-align: right">-</div>
						</div>
					</td>
				</tr>
				@foreach([1 => "1ST Prize", 2 => "2ND Prize", 3 => "3RD Prize"] as $key => $prize)
				<tr>
					<td class="text-center py-1" width="50%" style="font-size: 1.5em; line-height: 1.5;">{{ $prize }}</td>
					<td id="prize_{{ $key }}" style="font-size: 24px; font-weight: 700; line-height: 1.5;" class=" py-1">---</td>
				</tr>
				@endforeach
				<tr class="text-center">
					<td colspan="2" style="font-size: 1.5em; line-height: 1.5; border-top: 0; border-bottom: 0" class="py-1">Special</td>
				</tr>
				<tr>
					<td class="p-0" colspan="2" style="border: 0">
						<table id="specialTable" class="table mb-0">
							@foreach([0, 1, 2] as $i)
							<tr>
								@foreach([0, 1, 2, 3, 4] as $j)
								<td id="s_prize_{{ ($i * 5) + $j }}" width="20%" class="s-prize text-center py-1 px-1" style="font-size: 1.2em; font-weight: 700">---</td>
								@endforeach
							</tr>
							@endforeach
						</table>
					</td>
				</tr>
				<tr class="text-center">
					<td colspan="2" style="font-size: 1.5em; line-height: 1.5; border-top: 0; border-bottom: 0" class="py-1">Consolation</td>
				</tr>
				<tr>
					<td class="p-0" colspan="2" style="border: 0">
						<table id="consolationTable" class="table mb-0">
							@php($k = 0)
							@foreach([0, 1, 2] as $i)
							<tr>
								@foreach([0, 1, 2, 3, 4] as $j)
								<td id="c_prize_{{ ($i * 5) + $j }}" width="20%" class="c-prize text-center py-1 px-1" style="font-size: 1.2em; font-weight: 700">---</td>
								@endforeach
							</tr>
							@endforeach
						</table>
					</td>
				</tr>
			</table>
		</div>
	</div>
	<div class="row">
		<div class="col-12 text-center">
			<div onclick="loadResult()" class="refresh">
				<i class="feather icon-refresh-cw text-center"></i><br />Refresh
			</div>
		</div>
	</div>
</div>
@endsection

@section('js')
	<script src="{{ asset('js/flatpickr.js') }}" defer></script>
	<script src="{{ asset('js/result.js') }}" defer></script>
@endsection

@section('footer-js')
	<script type="text/javascript">
		let loading = false;
		
		function loadResult() {
			if (loading) {
				return;
			}
			loading = true;
			// clear data
			$("#drawDate").text("-");
			$("#drawNo").text("-");
			$("#prize_" + 1).text("-");
			$("#prize_" + 2).text("-");
			$("#prize_" + 3).text("-");
			$(".s-prize").text("-");
			$(".c-prize").text("-");
			$.ajax({
				url: "{{ route('result', ['id' => $room_id]) }}",
				type: "POST",
				data: {
					date: $("#date").val(),
					type: $("input[name=type]:checked").val()
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				if (res.loading) {
					// result not ready, wait for a while and reload
					setTimeout(loadResult, 5000);
					loading = false;
					return;
				}
				// render
				$("#drawDate").text(res.betting?.date || "-");
				$("#drawNo").text(res.betting?.version || "-");
				$("#prize_" + 1).text(res.prizes.find(x => x.api_prize === "firstPrize")?.value.toString() || "-");
				$("#prize_" + 2).text(res.prizes.find(x => x.api_prize === "secondPrize")?.value.toString() || "-");
				$("#prize_" + 3).text(res.prizes.find(x => x.api_prize === "thirdPrize")?.value.toString() || "-");
				
				if ($("#specialTable")[0].children.length > 2) {
					$("#specialTable")[0].children[2].remove();
				}
				res.prizes.filter(x => x.api_prize === "spe").forEach((prize, i) => {
					const ele = $("#s_prize_" + i);
					if (ele.length === 0) {
						let content = "";
						for (let i = 0; i < 5; i++) {
							let newEle = $("<td" + (i > 0 && i < 4 ? ("id='s_prize_" + (i + 10) + "'") : "") +
								" width='20%' class='s-prize text-center'></td>");
							if (i === 1) {
								ele = newEle;
							}
							content += newEle[0].outerHTML;
						}
						$("#specialTable").html("<tr>" +
							content +
						"</tr>");
					}
					ele.text(prize.value.toString() || "-");
				});
				if ($("#consolationTable")[0].children.length > 2) {
					$("#consolationTable")[0].children[2].remove();
				}
				res.prizes.filter(x => x.api_prize === "con").forEach((prize, i) => {
					const ele = $("#c_prize_" + i);
					if (ele.length === 0) {
						let content = "";
						for (let i = 0; i < 5; i++) {
							let newEle = $("<td" + (i > 0 && i < 4 ? ("id='c_prize_" + (i + 10) + "'") : "") +
								" width='20%' class='c-prize text-center'></td>");
							if (i === 1) {
								ele = newEle;
							}
							content += newEle[0].outerHTML;
						}
						$("#consolationTable").html("<tr>" +
							content +
						"</tr>");
					}
					ele.text(prize.value.toString() || "-");
				});
				loading = false;
			}).fail(function(err) {
				Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
				loading = false;
			});
		}
		
		function converDate(date) {
			return date.getFullYear() + "-" + (date.getMonth() + 1).toString().padStart(2, "0")
				  + "-" + date.getDate().toString().padStart(2, "0");
		}
		
		function prevDate() {
			const date = new Date($("#date").val());
			date.setDate(date.getDate() - 1);
			$("#date").val(converDate(date));
			loadResult();
		}
		
		function nextDate() {
			const date = new Date($("#date").val());
			date.setDate(date.getDate() + 1);
			$("#date").val(converDate(date));
			loadResult();
		}
		
		function onClickRadio(evt) {
			const btn = $(this).find("input");			
			$(".four-d-radio").removeClass("active");
			if (!btn[0].checked) {
				$(this).addClass("active");
				btn[0].checked = true;
			}
			loadResult();
		}
		
		const radios = document.getElementsByClassName("four-d-radio");
		for(let radio of radios) {
			radio.addEventListener("click", onClickRadio);
		}
	</script>
@endsection