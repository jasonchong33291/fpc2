@extends('layouts.app')

@section('button', "大厅")

@section('title', $room->name)

@section('title_other', "")

@php($dice_size = 60)

@section('css')
<style>
*{
	font-family: 'mandarin'; 
}

@font-face{
	font-family: mandarin;
	src: url({{  asset('fonts/上首财神体.ttf') }});
}

@font-face{
	font-family: MyriadPro;
	src: url({{  asset('fonts/MyriadPro.ttf') }});
}

.roulette-text{
	font-family: MyriadPro!important;
	color:  #000;
}

.roulette-text b{
	font-family: MyriadPro!important;
}

h2{
	font-size: 40px;
	font-weight: 400;
}

.roulette-table-bg{
	background: #000;
	padding:  30px;
	min-height: 100vh;
}

.dice-wrapper-box{
	background: linear-gradient(180deg, rgba(65,64,66,1) 0%, rgba(37,27,28,1) 100%);
}

.board-wrapper-box{
	background: linear-gradient(180deg, rgba(22,108,60,1) 0%, rgba(1,85,43,1) 100%);
}

.chip-wrapper-box{
	background: linear-gradient(180deg, rgba(37,27,28,1) 0%, rgba(65,64,66,1) 100%);
}

.light-board.active {
	animation: blink-cell 1s linear infinite;
}

@keyframes blink-cell {
  50% {
	box-shadow: 0 0 5px 5px yellow inset;
	-moz-box-shadow: 0 0 5px 5px yellow inset;
	-webkit-box-shadow: 0 0 5px 5px yellow inset;
  }
}

.chip {
	width: 70px;
	height: 70px;
}

.chip-on-board {
	height: 40px;
	width: 40px;
	-webkit-transition: left 1s, top 0.5s;
	-moz-transition: left 1s, top 0.5s;
	-ms-transition: left 1s, top 0.5s;
	-o-transition: left 1s, top 0.5s;
	transition: left 1s, top 0.5s;
}

.chip-cursor {
	position: absolute;
	pointer-events: none;
	cursor: move;
}

.chip-board {
	position: absolute;
	pointer-events: none;
}

.chip-amount {
	color: white;
	font-size: 10px;
	position: absolute;
	pointer-events: none;
}

.chip-selected {
	border-radius: 50%;
	border: 3px dashed yellow;
	margin-top: -5px;
}

@media only all and (max-width:992px) {
	.chip {
		width: 40px;
		height: 40px;
	}
	
	.chip-amount {
		font-size: 5px;
	}

	.chip-on-board {
		height: 30px;
		width: 30px;
	}
}

.gamename{
	height: 160px;
	width: 400px;
	position: absolute;
	top: 0;
	padding: 15px;
	left: 50%;
	text-align: center;
	transform: translateX(-50%);
	z-index: 999;
	background: url('{{asset("images/gamename-bg.png")}}');
	background-size: 100% 100%;
}

.gamename h3{
	font-weight: 550;
	color: #f2b800;;
	font-size: 85px;
	line-height: 0.75;
	margin-bottom: 0;
}

.gamename h4{
	color:  #f2b800;;
	font-size: 35px;
}

.title{
	font-size: 40px;
	color: #fff;
}

.result-box {
	margin-bottom: 5px;
}

.title-right{
	font-size: 32px; 
	color: #fff;
}

.bet-minmax h3{
	text-align: left;
	color: #fff ;
	font-size: 40px;
}

.round{
	display: flex;
	align-items: center;
	height: 100%;
}

.fpc-result-dice-box {
	padding :2.5px;
	background: #edf2f3;
}

.fpc-result-container {
	overflow-x: scroll;
	white-space: nowrap;
	display: inline-flex;
}

.fpc-result-dice-container {
	flex: 0 0 8.3333333333%;
    max-width: 8.3333333333%;
}

.fpc-result-dice-header {
	text-align: left;
	font-size: 10px;
	height: 10px;
	line-height: 1;
}

.dice-result img{
	width: 40px;
}

.modal-content{
	background:  transparent!important;
	border:  0;
}

.modal-body{
	text-align: center;
	color:  #fff;
	font-size: 28px;
}

.modal-result i {
	color: #fff;
	border-radius: 50%;
	font-size: 60px;
	margin:  auto;
	display: flex;
	justify-content: center;
	align-items: center;
	line-height: 1;
	width: 100px;
	height: 100px;
}

#board_overlay {
	display: none; 
	position: absolute; 
	left: 0; 
	top: 0; 
	width: 100%; 
	height: 100%; 
	font-size: 50px;
	background: #000000AA;
}

@media(max-width:  991px){
	#board_overlay {
		font-size: 20px;
	}
	
	.gamename {
		padding: 5px 10px 10px 10px;
		height: 80px;
		width: 194px;
	}

	.gamename h3{
		font-size: 40px;
		text-align: center;
	}

	.gamename h4{
		 font-size: 16px;
	}

	.title{
		font-size: 20px;
		margin-bottom: 5px;
	}

	.title-right{
		font-size: 20px; 
		color: #fcee21;
	}

	.bet-minmax h3{
		font-size: 18px;
	}

	.dice-result img{
		width: 13px;
	}

	.result-border{
		padding: 10px;
	}

	.timer{
		font-size: 14px!important;
		padding:  0;
	}
}

.game {
	min-height: 220px;
}

.game-container {
	background: radial-gradient(circle, rgba(190,30,45,1) 66.6642%, rgba(79,0,0,1) 100%);
}

.bowl {
	position: absolute;
	width: 240px;
	left: 50%;
	top: 10px;
	transform: translateX(-50%);
}

.bowl-cover {
	position: absolute;
	width: 240px;
	left: 50%;
	top: 10px;
	transform: translateX(-50%);
}

.dice-container {
  position: absolute;
  top: 50%;
  left: 50%;
  z-index: 2;
  transform: translate(-50px, -60px);
}

.dice {
  position: absolute;
  width: {{ $dice_size }}px;
  height: {{ $dice_size }}px;  
  transform-style: preserve-3d;
  -webkit-transition: transform 1s;
  -moz-transition: transform 1s;
  -ms-transition: transform 1s;
  -o-transition: transform 1s;
  transition: transform 1s;
}

.dice-one {
  top: -15px;
  left: 20px;
  z-index: 2;
}

.dice-two {
  top: 55px;
  left: -10px;
  z-index: 1;  
}

.dice-three {
  top: 55px;
  left: 60px;
  z-index: 3;  
}

.side {
  position: absolute;
  background-color: #ffF;
  border-radius:15px;
  border: 1px solid #e5e5e5;
  text-align: center;
  line-height: 2em;
}


.side img{
  border-radius:15px;
}

/*Tiger */

.side:nth-child(1) { 
  transform: translateZ({{ $dice_size / 2 }}px); }

.side:nth-child(5) {
  transform: rotateY(90deg) translateZ({{ $dice_size / 2 }}px); }

.side:nth-child(6) {
  transform: rotateY(-90deg) translateZ({{ $dice_size / 2 }}px); }

.side:nth-child(3) {
  transform: rotateX(90deg) translateZ({{ $dice_size / 2 }}px); }

.side:nth-child(4) {
  transform: rotateX(-90deg) translateZ({{ $dice_size / 2 }}px); }

.side:nth-child(2) {
  transform: rotateY(-180deg) translateZ({{ $dice_size / 2 }}px); }

.show-1 {
  transform: rotateX(720deg) rotateZ(-720deg); }

.show-2 {
  transform: rotateX(-900deg) rotateZ(900deg); }

.show-5 {
  transform: rotateY(-450deg) rotateZ(-1440deg); }

.show-6 {
  transform: rotateY(810deg) rotateZ(720deg); }

.show-3 {
  transform: rotateX(-810deg) rotateZ(-1080deg); }

.show-4 {
  transform: rotateX(450deg) rotateZ(-720deg); }

.fpc-overlay {
	position: fixed;
	top: 0;
	left: 0;
	width: 100%;
	height: 100%;
	background: #222222DD;
	z-index: 1001;
	text-align: center;
}

.fpc-overlay .gamename {
	position: unset;
	transform: none;
	margin: auto;
	margin-top: 10%;
}

.fpc-overlay .click-to-play {
	color: grey;
	animation: blink-word 1s linear infinite;
}

.bowl-overlay {
	display: none;
	position: fixed;
	left: 50%;
	top: -50%;
	height: 75%;
	transform: translate(-50%, -50%);
	-webkit-transition: top 1s;
	-moz-transition: top 1s;
	-ms-transition: top 1s;
	-o-transition: top 1s;
	transition: top 1s;
}

@keyframes blink-word {
	50% {
		color: white;
	}
}

.bg-audio {
	top: 1%;
	right: 1.5%;
	position: fixed;
	z-index: 1001;
	cursor: pointer;
}

.red-pack {
	position: fixed;
	top: 50%;
	left: 50%; margin-left: -50px;
	margin-top: -50px;
}

.animate-top-left {
	position: fixed;
	-webkit-transition: left 0.5s, top 0.5s;
	-moz-transition: left 0.5s, top 0.5s;
	-ms-transition: left 0.5s, top 0.5s;
	-o-transition: left 0.5s, top 0.5s;
	transition: left 0.5s, top 0.5s;
}

#board_overlay_text {
	color: white;
	white-space: pre-wrap;
	transform: translate(-50%, -50%);
	left: 50%;top: 50%;
	position: absolute;
	user-select: none;
}

body {	
	user-select: none;
}

body img {
	-webkit-user-drag: none;
}
	@media(max-width:  1280px) {
		.bet-minmax h3 {
			font-size: 28px;
		}
		.result-box {
			margin-bottom: 30px;
		}
	}

	@media(max-width:  992px){

		.result-box {
			margin-bottom: 30px;
			overflow-y: scroll;
		}
	}
	
	.timer-container {
		position: absolute;
		top: 20px;
		right: 20px;
		width: 50px;
		height: 50px;
		background: lightgreen;
		border-radius: 50%;
		z-index: 1;
		padding: 2px;
	}

	.timer {
		background: green;
		border-radius: 50%;
		height: 46px;
		width: 46px;
		padding: 2px;
		text-align: center;
		color: lightgreen;
	}
	
	.timer-content {
		display: flex;
		height: 100%;
		justify-content: center;
		align-content: center;
		flex-direction: column;
	}
	
	.btn-grey {
		color: #EEEEEE !important;
		background-color: #636363;
		font-size: 20px;
		padding: 0;
	}
	
	.btn-grey > i {
		vertical-align: middle;
		padding-right: 5px;
	}
	
	.white-board {
		position: absolute;
		background-color: #FFFFFFBB;
		z-index: 1;
	}
	
	.white-board-right {
		transform: translate(50%, -50%);
		right: 0;
		top: 50%;
		height: 10vw;
		max-height: 60px;
		width: 30px;
	}
	
	.white-board-bottom {
		transform: translate(-50%, 50%);
		bottom: 0;
		left: 50%;
		width: 10vw;
		max-width: 60px;
		height: 30px;
	}
	
	.topbar {
		color: #EEEEEE;
		background-color: #636363;
	}
	
	.topbar > .middle {
		margin: auto 0;
	}
	
	.feature-container {
		position: absolute;
		bottom: 20px;
		right: 20px;
	}
	
	.pending > .chip-on-board {
		opacity: 0.5;
	}
	
	.toast-container {
		display: none;
		position: fixed;
		bottom: 0;
		width: 100%;
		left: 0;
		text-align: center;
		z-index: 1003;
	}
	
	.rate-label {
		font-size: 50px;
		color: gold;
		text-shadow: 1px 1px 3px black;
		display: none;
		position: absolute;
		transform: translate(-50%, -50%);
		left: 50%;
		top: 0px;
	}
	
	.white-board > .rate-label {
		font-size: 25px;
		text-shadow: 1px 1px 1px grey;
	}
	
	.timer-animate {
		background: -webkit-linear-gradient(left, black 50%, green 50%);
		border-radius: 100%;
		height: var(--size);
		width: var(--size);
		position: relative;
		-webkit-animation: time calc(var(--duration) * 1s) steps(1000, start) 1;
	}
	.mask {
		border-radius: 100% 0 0 100% / 50% 0 0 50%;
		height: 100%;
		left: 0;
		position: absolute;
		top: 0;
		width: 50%;
		-webkit-animation: mask calc(var(--duration) * 1s) steps(500, start) 1;
		-webkit-transform-origin: 100% 50%;
	}
	@-webkit-keyframes time {
		100% {
			-webkit-transform: rotate(360deg);
		}
	}
	@-webkit-keyframes mask {
		0% {
			background: green;
			-webkit-transform: rotate(0deg);
		}
		50% {
			background: green;
			-webkit-transform: rotate(-180deg);
		}
		50.01% {
			background: black;
			-webkit-transform: rotate(0deg);
		}
		100% {
			background: black;
			-webkit-transform: rotate(-180deg);
		}
	}
	
	.win-container {
		background-color: gold;
		padding-left: 10px;
		padding-right: 10px;
		border-radius: 15px;
	}
	
	.win-container {
		text-shadow: 0 -1px 0 #f4cc9b, 0 1px 0 #a77334, 0 2px 0 #9b6b30, 0 3px 0 #90632d, 0 4px 0 #7a5426, 0 4px 2px #7a5426, 0 0.075em 0.1em rgba(26, 35, 39, 0.3), 0 0.15em 0.3em rgba(222, 153, 69, 0.2);
	}
	</style>
    <link href="{{ asset('css/bootadmin/feathericons.css') }}" rel="stylesheet" />
@endsection

@section('content')
<div class="landscape">
	<div class="container px-0">
		<div class="row col-sm-6 mx-auto px-0 topbar">
			@php($user = Auth::user())
			<div>
				<button onclick="preHandleBack()" class="btn btn-grey">
					<i class="feather icon-chevron-left"></i>
				</button>
			</div>
			<div class="middle ml-2">
				<i class="feather icon-dollar-sign"></i>
				<span id="betAmount">0.00</span>
			</div>
			<div class="balance-container middle ml-2">
				<i class="feather icon-dollar-sign"></i>
				<span class="balance">{{ number_format($user->totalBalance, 2) }}</span>
			</div>
		</div>
		<div class="row col-sm-6 mx-auto px-0">
			<div class="col-sm-12 dice-wrapper-box py-3">
				<!-- Timer -->
				<div class="timer-container">
					<div class="timer">
						@if ($room->is_standalone)
						<button id="confirm" class="btn btn-control" style="color: white;">Bet</button>
						@else
						<div class="timer-content" id="second">-</div>
						@endif
					</div>
				</div>
				<!-- Dice -->
				<div class="game-container">
					<!-- Dice here -->
					<img id="bowl" class="bowl" src="{{ asset('images/bowl.png') }}" />
					<img id="bowl_cover" class="bowl-cover" src="{{ asset('images/cover.png') }}" />	
					<div class="game">
						<div id="dice_container" class="dice-container">
							@php($numberWords = ["one", "two", "three", "four", "five", "six"])
							@foreach([1, 2, 3] as $i => $diceIndex)
							<div id='dice{{ $diceIndex }}' class="dice dice-{{ $numberWords[$i] }}">
								@foreach($fpc_order as $j => $fpc)
								@if($j > 0)
								<div id="dice-{{ $numberWords[$i] }}-side-{{ $numberWords[$j - 1] }}" class='side {{ $numberWords[$j - 1] }}'>
								  <img src="{{asset('images/dices/' . $fpc . '.png')}}" width="100%" height="100%">
								</div>
								@endif
								@endforeach
							</div>
							@endforeach
						</div>
					</div>
					@php($last = $room->endedBettings()->orderByDesc('id')->first())
				</div>
			
				<!-- Additional Feature -->
				<div class="feature-container">
					<button id="bgAudioBtn" class="btn btn-grey p-1">
						<i class="feather icon-volume-x"></i>
					</button>
				</div>
			</div>

			<div class="col-sm-12 board-wrapper-box py-2">
				<input type="hidden" id="betting_id" value="{{ $room->current ? $room->current->id : '' }}" />
				<div class="row text-center">
					<div class="col-12" style="position: relative;">
						@php($last24 = $room->endedBettings()->limit(24)->get()->sortBy('id')->values())
						@php($rateGroups = $room->rateGroups)
						@php($single = $rateGroups->where('group', 'single_any')->first())
						@php($double = $rateGroups->where('group', 'double_any')->first())
						@php($all = $rateGroups->where('group', 'match_all')->first())
						<!-- FPC Board -->
						<div style="position: relative;">
							<div class="row mb-2 px-3">
								@for($i = 1; $i < 7; $i++)
								<div id="light_board_{{ $i }}" class="col-4 p-1 light-board" style="position: relative;">
									<div id="board_{{ $i }}" onclick="addRecord(this, '{{ $i }}', {{ $single->id }}, true)">
										<img src="{{ asset('images/' . $fpc_order[$i] . '-01.png') }}" style="width: 100%" />
										@foreach($single->rates as $rate)
										<div id="rate_{{ $i }}_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
									@if ($i % 3 != 0)
									<div id="board_{{ $i }}_{{ $i + 1 }}" class="white-board white-board-right light-board" onclick="addRecord(this, '{{ $i }}-{{ $i + 1 }}', {{ $double->id }}, true, { top: true, right: true })">
										@foreach($double->rates as $rate)
										<div id="rate_{{ $i }}_{{ $i + 1 }}_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
									@endif
									@if ($i < 4)
									<div id="board_{{ $i }}_{{ $i + 3 }}" class="white-board white-board-bottom light-board" onclick="addRecord(this, '{{ $i }}-{{ $i + 3 }}', {{ $double->id }}, true, { bottom: true, left: true })">
										@foreach($double->rates as $rate)
										<div id="rate_{{ $i }}_{{ $i + 3 }}_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
									@endif
								</div>
								@endfor
							</div>
							
							<div class="row mb-2 px-3">
								<div id="board_1_3" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 1 }}-{{ 3 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[1] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[3] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_1_3_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
								<div id="board_4_6" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 4 }}-{{ 6 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[4] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[6] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_4_6_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
								<div id="board_3_5" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 3 }}-{{ 5 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[3] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[5] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_3_5_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
								<div id="board_2_6" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 2 }}-{{ 6 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[6] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[2] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_2_6_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
								<div id="board_1_5" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 1 }}-{{ 5 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[1] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[5] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_1_5_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
								<div id="board_2_4" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 2 }}-{{ 4 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[4] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[2] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_2_4_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
								<div id="board_3_4" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 3 }}-{{ 4 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[3] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[4] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_3_4_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
								<div id="board_1_6" class="col-3 p-1 light-board" onclick="addRecord(this, '{{ 1 }}-{{ 6 }}', {{ $double->id }}, false)">
									<div class="row" style="margin:0;">
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[6] . '-01.png') }}" style="width: 100%" />
										</div>
										<div class="col-6" style="padding: 0;">
											<img src="{{ asset('images/' . $fpc_order[1] . '-01.png') }}" style="width: 100%" />
										</div>
										@foreach($double->rates as $rate)
										<div id="rate_1_6_{{ $rate->matched }}" class="rate-label">{{ $rate->rate }}x</div>
										@endforeach
									</div>
								</div>
							</div>
							<div id="pending_record" class="col-12 pending" style="position: absolute; left: 0; top: 0; z-index: 2;"></div>
							<div id="commit_record" class="col-12" style="position: absolute; left: 0; top: 0; z-index: 2;"></div>
							<div id="board_overlay" style="z-index: 3;">
								<div id="board_overlay_text"></div>
							</div>
						</div>
						</div>
					</div>
				</div>

				<div class="col-sm-12 chip-wrapper-box py-2">
					<div class="row" style="margin-top: 20px; margin-bottom: 10px;">
						<div class="col-10 mx-auto px-0">
							<div class="row justify-content-center">
								@foreach(['1', '2', '5', '10', '20'] as $cAmount)
								<div class="col-2 p-0">
									<img class="chip" onmouseup="createDragChip(event, {{ $cAmount }})"
									ontouchstart="createDragChip(event, {{ $cAmount }}, false)"
									src="{{ asset('images/chip-' . $cAmount . '.png') }}" width="80px"  style="margin-bottom: 8px;" />
								</div>
								@endforeach
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-4 px-1">
							<button id="undoBtn" disabled type="button" onclick="undo()" class="btn btn-block btn-grey btn-control">
								<i class="feather icon-undo"></I>Undo
							</button>
						</div>
						<div class="col-4 px-1">
							<button id="clearBtn" disabled type="button" onclick="clearBet()" class="btn btn-block btn-grey btn-control">
								Clear
							</button>
						</div>
						<div class="col-4 px-1">
							<button id="rebetBtn"{{ $user->bettingRecords()->whereHas('betting', function($q) use($room) { $q->where('room_id', $room->id); })->first() ? '' : ' disabled' }} type="button" onclick="rebet()" class="btn btn-block btn-grey btn-control">
								Re-bet
							</button>
						</div>
						<!--<div class="col-4 px-1">
							<button type="button" onclick="submit(event)" class="btn btn-block btn-grey">
								<i class="feather icon-check"></I>Submit
							</button>
						</div>
						<div class="col-4 px-1">
							<button type="button" onclick="resetBet()" class="btn btn-block btn-grey">
								<i class="feather icon-refresh-cw"></I>Reset
							</button>
						</div>
						<div class="col-4 px-1">
							<button type="button" onclick="clearBet()" class="btn btn-block btn-grey">
								<i class="feather icon-x"></i>Undo
							</button>
						</div>-->
					</div>
				</div>

				<div class="col-sm-12 text-center">
					<div class="row">
						<div class="col-10 px-0">
							@if ($room->is_standalone)
							@php($last10 = $room->endedBettings()->where('standalone_user', $user->id)
								->limit(10)->get()->sortByDesc('id')->values())
							@else
							@php($last10 = $room->endedBettings()->limit(10)->get()->sortByDesc('id')->values())
							@endif
							<div id="result_container" class="fpc-result-container mx-0">
								@for($i = 0; $i < 10; $i++)
								<div class="fpc-result-dice-container">
									@php($lastItem = $last10->get($i))
									<div id="result_{{ $lastItem ? $lastItem->id : '' }}">
										@for($j = 0; $j < 3; $j++)
										<div class="fpc-result-dice-box">
											@if ($lastItem && $result = $lastItem->results->get($j))
											<img src="{{ asset('images/stats/' . $fpc_order[$result->value] . '.png') }}" style="width: 100%" />
											@else
											<img src="{{ asset('images/empty.png') }}" style="width: 100%" />
											@endif
										</div>
										@endfor
									</div>
								</div>
								@endfor
							</div>
						</div>
						<div class="col-2 px-0">
							<!-- percentage here -->
							<div class="bet-minmax">
								<div class="row mx-0">
									@php($array = ($last10->map(function($item) {
										return $item->results->map(function($data) {
											return $data->value;
										});
									})->toArray()))
									@php($merged = count($array) > 0 ? call_user_func_array('array_merge', $array) : [])
									@php($fpcCount = array_count_values($merged))
									@foreach($fpc_order as $j => $fpc)
									@if($j > 0)
									<div class="col-12 p-0 my-auto">
										<div class="d-flex">
											<img src="{{asset('images/stats/' . $fpc . '.png')}}" style="width: 20px; border-radius: 5px; margin-right: 5px" /><p id="percentage_{{ $j }}" class="mb-0 text-center">{{ isset($fpcCount[$j]) ? number_format($fpcCount[$j] / count($merged) * 100, 0) : 0 }}%</p>
										</div>
									</div>
									@endif
									@endforeach
								</div>
							</div>
						</div>
					</div>
					
					<!--<div class="bet-minmax">
						@if($room->min)
						<h3>最小 ：{{ $room->min }}</h3>
						@endif
						@if($room->max)
						<h3>最大 ：{{ $room->max }}</h3>
						@endif
					</div>-->

				</div>
			</div>
			</div>
		</div>
</div>
<div id="fpc_overlay" class="fpc-overlay">
	<div class="gamename">
		<h3>鱼虾蟹</h3>
		<h4>Fish Prawn Crab</h4>
	</div>
	<div class="click-to-play">Click to Start</div>
</div>
<img id="bowl_overlay" class="bowl-overlay" src="{{ asset('images/bowl.png') }}" />
<!-- resultl modal -->
@include('layouts.result-modal', ['title' => "The result is: "])
<!-- transparent chip sample -->
<div id="sample_chip" class="chip-on-board" style="position: absolute; left: 0; bottom: 0; pointer-events: none;"></div>
<div id="toastContainer" class="toast-container">
	<div id="toastMessage" style="margin-bottom: 0;"></div>
</div>
<!-- background music and sound effect source -->
<audio id="bg_audio" loop>
	<source src="{{ asset('audios/Ballad of Four Seasons.mp3') }}" type="audio/mp3"></source>
</audio>
<audio id="chip_audio" preload>
	<source src="{{ asset('audios/chip.mp3') }}" type="audio/mp3"></source>
</audio>
<audio id="win_audio" preload>
	<source src="{{ asset('audios/win.wav') }}" type="audio/wav"></source>
</audio>
<audio id="dice_audio" preload>
	<source src="{{ asset('audios/dice-rolling.wav') }}" type="audio/wav"></source>
</audio>
@endsection

@section('js')
	<script type="text/javascript">
	    const channelName = "presence-room.{{ $room->id }}";
		const eventName = "betting.updated";
		const gameKey = "{!! $room->game->key !!}";
		const fpcOrders = {!! json_encode($fpc_order) !!};
		const lastResult = {!! $last && $last->results->count() > 0 ? $last->results()->pluck("value") : json_encode([1, 2, 3]) !!};
		let isOpening, isCommiting;		
		let startDate = {{ !$room->is_standalone && $room->current ? $room->current->ended_at->setTimezone('UTC')->timestamp * 1000 : 0 }};
		const chipAmounts = [0.1, 0.5, 1, 5, 10, 50];
		const merged = {!! json_encode($merged) !!};
		const fpcCount = {!! json_encode($fpcCount) !!};
		const rates = {!! $rateGroups !!};
		let rebetId = "";
		let groupedRecords = [];
		let pendingRecords = [];
		
		function clearRecord(ids = []) {
			const id = $("#betting_id").val();
			if(id) {
				return $.ajax({
					url: "{{ route('room.clear', ['id' => $room->id]) }}",
					data: {
						ids: ids,
						betting_id: id,
					},
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: "POST"
				}).done(function(res) {
					if (ids.length > 0) {
						ids.forEach(id => {
							const index = pendingRecords.findIndex(x => x.id === id);
							if (index > -1) {
								const record = pendingRecords[index];
								$("#chip" + record.ele_id).remove();
								pendingRecords.splice(index, 1);
							}
						});
					}
					syncGroupedRecord();
					updateBalance(res?.user);
					updateBetAmount();
				}).fail(function(err) {
					Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
				});
			}
		}
		
		function loadRecord() {
			const id = $("#betting_id").val();
			if(id) {
				$.ajax({
					url: "{{ route('room.record', ['id' => $room->id]) }}",
					data: {
						betting_id: id,
					},
					headers: {
						'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
					},
					type: "POST"
				}).done(function(records) {
					$("#pending_record").empty();
					records.forEach(function(record) {
						if (record.group && !rebetId) {
							rebetId = record.group;
							$("#rebetBtn").prop("disabled", true);
						}
						const ele = document.getElementById("board_" + record.play_option.replaceAll("-", "_"));
						if (ele) {
							const position = getRecordPosition(record.play_option);
							const room_rate_id = rates.find(x => x.group === record.play_group)?.id;
							addRecordProcess(ele, record.amount, record.play_option, room_rate_id, @if($room->is_standalone) true @else false @endif, position !== null, false, position, record.id, record.betting_id);
						} else {
							console.warn("element not found", "board_" + record.play_option.replaceAll("-", "_"));
						}
					});
				}).fail(function(err) {
					Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
				});
			}
		}
		
		function loadTotalWon(id, results) {
			$.ajax({
				url: "{{ route('room', ['id' => $room->id]) }}",
				type: "POST",
				data: {
					betting_id: id,
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				// set total won
				if (res.cost) {
					showResultProcess(results, res.cost, res.won);
				} else {
					betRestart();
				}
				updateBalance(res.user);
			}).fail(function(err) {
				betRestart();
			});
		}

		function updateBalance(user) {
			$(".balance").text(user?.total_balance);
		}
		
		function createDragChip(e, cAmount, createVisual = true) {
			const selectedChips = document.getElementsByClassName("chip-selected");
			
			for (let selectedChip of selectedChips) {
				selectedChip.className = selectedChip.className.replace(" chip-selected", "");
			}
			
			if (e.target.classList.length == 1) {
				e.target.className += " chip-selected";
			}
			
			const dragChips = document.getElementsByClassName("chip-cursor");
			let dragChip = dragChips.length > 0 ? dragChips[0] : null;
			if (dragChip) {
				dragChip.dataset.value = cAmount;
				dragChip.src = "{{ asset('images/chip-') }}" + cAmount + ".png";
				return;
			}
			
			createChip(e.clientY - 20, e.clientX - 20, cAmount, createVisual, true);				
		}
		
		function createChip(top, left, cAmount, createVisual, isCursor, value = null, height = 0, width = 0, isNew = true) {			
			const dragChip = document.createElement("img");
			dragChip.dataset.value = cAmount;
			dragChip.src = "{{ asset('images/chip-') }}" + cAmount + ".png";
			dragChip.className = (isCursor ? "chip chip-cursor" : "chip-on-board chip-board");
			if (createVisual) {
				dragChip.style.left = left + "px";
				dragChip.style.top = top + "px";
				document.body.addEventListener('mousemove', moveDragChip);
			} else {
				dragChip.style.display = "none";
			}
			if (!isCursor) {
				dragChip.className += " chip-amount-" + cAmount.toString().replace(".", "") + " chip-board-" + value;
				if (isNew) {
					document.getElementById("pending_record").appendChild(dragChip);
				} else {
					document.getElementById("commit_record").appendChild(dragChip);
				}
			} else {
				dragChip.style.zIndex = 2;
				document.body.appendChild(dragChip);
			}
			return dragChip;
		}
		
		function updateBetAmount() {
			const amount = pendingRecords.reduce((pv, item) => pv + item.amount, 0);
			const amountEle = document.getElementById("betAmount");
			amountEle.innerText = amount.toFixed(2);
		}
		
		function addToGroupedRecord(value, amount, recordData) {
			const gIndex = groupedRecords.findIndex(x => x.value === value);
			let chips = {};
			let chipOrder = [];
			if (gIndex > -1) {
				const exist = groupedRecords[gIndex];
				chips = exist.chips;
				chips[amount] = chips[amount] ? chips[amount] + 1: 1;
				// update
				exist.amount += amount;
				exist.chips = chips;
				if (exist.chipOrder.findIndex(x => x === amount.toString()) === -1) {
					exist.chipOrder.push(amount.toString());
				}
				// pre
				//const higherAmount = chipAmounts.find(x => x === exist.amount);
				// group chip on board
				// if (higherAmount) {
				//	exist.chips[higherAmount] += 1;
					// $(".chip-board-" + value).remove();
				//}			
				// update
				groupedRecords[gIndex] = exist;
				chipOrder = exist.chipOrder;
			} else {
				// add
				chips[amount] = 1;
				chipOrder.push(amount.toString());
				groupedRecords.push(Object.assign({					
					chips: chips,
					chipOrder: chipOrder
				}, recordData));
			}
			return { chips, chipOrder };
		}
		
		function syncGroupedRecord() {			
			groupedRecords = [];
			pendingRecords.forEach(record => {
				addToGroupedRecord(record.value, record.amount, record);
			});
		}
		
		function addRecord(target, value, roomRateId, withOffset, position = null) {
			const dragChips = document.getElementsByClassName("chip-cursor");
			const dragChip = dragChips.length > 0 ? dragChips[0] : null;
			if (!dragChip) {
				return;
			}
			if (isOpening) {
				return;
			}
			let amount = parseFloat(dragChip.dataset.value);
			addRecordProcess(target, amount, value, roomRateId, {{ $room->is_standalone ? 0 : 1 }}, withOffset, true, position, null, $("#betting_id").val());
		}
			
		function addRecordProcess(target, amount, value, roomRateId, isNew, withOffset, playSound = true, position = null, recordId = null, bettingId = null) {
			// find existing by value
			$("#undoBtn").prop("disabled", false);
			$("#clearBtn").prop("disabled", false);
			const needProcess = {{ $room->is_standalone ? 0 : 1 }};
			const recordData = {
				amount: amount,
				room_rate_id: roomRateId,
				value: value,
				betting_id: bettingId,
				processing: needProcess,
				id: recordId
			};
			const pIndex = pendingRecords.push(recordData);			
			const { chips, chipOrder } = addToGroupedRecord(value, amount, recordData);
			// calculate
			const chipSameAmount = chips[amount] - 1;
			const chipLevelCount = chipOrder.findIndex(x => x === amount.toString());
			const sampleChip = document.getElementById("sample_chip");
			const chipWidth = sampleChip.clientWidth;
			const chipHeight = sampleChip.clientHeight;
			const chipStackWidth = chipWidth - 10;
			const chipStackHeight = chipHeight - 10;
			const chipGroupHeight = 1;
			const parent = target.parentElement;
			const defaultTop = (withOffset ? parent.offsetTop : 0) + target.clientTop + target.offsetTop + target.scrollTop;
			const defaultLeft = (withOffset ? parent.offsetLeft : 0) + target.clientLeft + target.offsetLeft + target.scrollLeft;
			const chipPerColumn = Math.floor((target.clientWidth) / chipStackWidth);
			const chipPerRow = Math.floor((target.clientHeight - (chipStackHeight * 1)) / chipStackHeight);
			// create
			let actualTop = defaultTop;
			let actualLeft = defaultLeft;
			//let remainChipOnRow = chipSameAmount;
			const row = Math.floor(chipSameAmount / chipPerRow);
			const targetRow = Math.floor(chipLevelCount / chipPerColumn);
			// if (targetRow > chipPerRow) {
				// group ?
				// const reassignedRow = chipPerRow > 0 ? targetRow % chipPerRow : 0;
				// const reassignedColumn = chipPerColumn > 0 ? chipLevelCount % chipPerColumn : 0;
				// if (reassignedRow && reassignedColumn) {
					// actualTop -= (reassignedRow * chipStackHeight) + (chipSameAmount * chipGroupHeight);
					// actualLeft += reassignedColumn * chipStackWidth;
				// } else {
				// }
			// } else {
				actualTop -= (targetRow * chipStackHeight) + (chipSameAmount * chipGroupHeight);
				actualLeft += (chipLevelCount - (targetRow * chipPerColumn)) * chipStackWidth;
			// }
			remainChipOnRow = chipSameAmount % chipPerRow;
			//}
			//actualLeft = defaultLeft + (remainChipOnRow * chipStackWidth);
			// let border = document.createElement("div");
			// border.style.border = "red 2px solid";
			// border.style.position = "absolute";
			// border.style.top = defaultTop + "px";
			// border.style.left = defaultLeft + "px";
			// border.style.width = target.clientWidth + "px";
			// border.style.height = target.clientHeight + "px";
			// border.style.pointerEvents = "none";
			// // document.getElementById("pending_record").appendChild(border);
			// console.log("remain", chipSameAmount, row, remainChipOnRow, chipWidth, chipStackWidth, chipHeight);
			// console.log(target.style.top > 0 ? "+" : "-");
			actualTop = position
				? (position.top
					? actualTop - (chipHeight / 2)
					: actualTop + (chipHeight / 4))
				: actualTop + (target.clientHeight / 2) - (chipHeight / 2);
			actualLeft = position
				? (position.left
					? actualLeft - (chipWidth / 2)
					: actualLeft + (chipWidth / 4))
				: actualLeft + (target.clientWidth / 2) - (chipWidth / 2);
			const chip = createChip(actualTop, actualLeft, amount, true, false
				, value, target.clientHeight, target.clientWidth, isNew);					
			updateBetAmount();
			// play sound
			if (playSound) {
				duplicateChipAudio();
			}
			if (needProcess && isNew) {
				confirmSingleBet({
					amount: amount,
					room_rate_id: roomRateId,
					value: value
				}, pIndex, chip);
			} else if (recordId) {
				chip.id = "chip" + recordId;
				pendingRecords[pIndex - 1].ele_id = recordId;
			} else {
				const tempId = new Date().getTime();
				chip.id = "chip" + tempId;
				pendingRecords[pIndex - 1].ele_id = tempId;
			}
		}
		
		function moveDragChip(e) {
			const dragChips = document.getElementsByClassName("chip-cursor");
			const dragChip = dragChips.length > 0 ? dragChips[0] : null;
			
			if (!dragChip) {
				return;
			}
			dragChip.style.left = e.clientX - 20 + "px";
			dragChip.style.top = e.clientY - 20 + "px";
		}
		
		function releaseDragChip() {			
			const dragChips = document.getElementsByClassName("chip-cursor");
			const dragChip = dragChips.length > 0 ? dragChips[0] : null;
			
			if (dragChip) {
				document.body.removeChild(dragChip);
				document.body.removeEventListener('mousemove', moveDragChip);
			}
			const selectedChips = document.getElementsByClassName("chip-selected");
			for (let selectedChip of selectedChips) {
				selectedChip.className = selectedChip.className.replace(" chip-selected", "");
			}
		}
		
		function resetBet() {
			pendingRecords = [];
			$("#commit_record").empty();
			$("#pending_record").empty();
			syncGroupedRecord();
			updateBetAmount();
		}
				
		function confirmSingleBet(pendingRecord, pIndex, chip) {
			const id = $("#betting_id").val();
			if(!id) {
				return showToastMessage("no more bet.", "warning");
			}
			return $.ajax({
				url: "{{ route('room.bet', ['id' => $room->id]) }}",
				type: "POST",
				data: {
					records: [pendingRecord],
					betting_id: id
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				pendingRecords[pIndex - 1].processing = 0;
				pendingRecords[pIndex - 1].id = res.record[0].id;
				pendingRecords[pIndex - 1].ele_id = res.record[0].id;
				chip.id = "chip" + res.record[0].id;
				$("#commit_record").append(chip.outerHTML);
				chip.remove();
				updateBalance(res.user);
				showToastMessage("下注成功", "success", 2000);
				// play sound?
				return res;
			}).fail(function(err) {
				pendingRecords.splice(pIndex - 1, 1);
				chip.remove();
				showToastMessage(err?.responseJSON?.message || err.responseText, err.status === 420 ? "info" : "danger");
			});
		}
		
		function confirmBet(pendingRecords) {
			if (pendingRecords.length == 0) {
				return;
			}
			@if($room->is_standalone)
			if (isOpening) {
				return;
			}	
			disableBet();
			@else
			const id = $("#betting_id").val();
			if(!id) {
				return showToastMessage("no more bet.", "warning");
			}
			@endif
			return $.ajax({
				url: "{{ route('room.bet', ['id' => $room->id]) }}",
				type: "POST",
				data: {
					records: pendingRecords,
					@if(!$room->is_standalone)
					betting_id: id
					@endif
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				processingRecords.forEach(record => {
					$("chip" + record.ele_id);
				});
				$("#commit_record").append($("#pending_record").html());
				$("#pending_record").html("");
				updateBalance(res.user);
				showToastMessage("下注成功", "success", 2000);
				// play sound?
				return res;
			}).fail(function(err) {
				@if($room->is_standalone)
				enableBet();
				@endif
				showToastMessage(err?.responseJSON?.message || err.responseText, err.status === 420 ? "info" : "danger");
			});
		}
		
		function clearBet() {
			resetBet();
			releaseDragChip();
			@if(!$room->is_standalone)
			clearRecord();
			@endif
			$("#clearBtn").prop("disabled", true);
			$("#undoBtn").prop("disabled", true);
			$("#rebetBtn").prop("disabled", false);
		}
		
		let toastTimeout;
		
		function showToastMessage(message, status = "success", timeout = 1000) {
			const toastContainer = document.getElementById("toastContainer");
			const toastMessage = document.getElementById("toastMessage");
			
			toastMessage.innerText = message;
			toastMessage.className = "alert alert-" + status;
			
			if (toastTimeout) {
				clearTimeout(toastTimeout);
			} else {
				toastContainer.style.display = "block";
			}
			
			toastTimeout = setTimeout(function() {
				toastTimeout = null;
				toastContainer.style.display = "none";
			}, timeout);
		}
		
		function showNoMoreBet() {
			// show 
			showBoard("No more bet");
			//moveDiceContainer();
		}
		
		function moveDiceContainer() {			
			// move bowl
			/* const overlay = document.getElementById("bowl_overlay");
			$(overlay).show();
			setTimeout(() => {
				overlay.style.top = "50%";
			}, 1);
			// move dice
			const container = document.getElementById("dice_container");
			const pRect = container.parentElement.getBoundingClientRect();
			container.style.position = "fixed";
			container.style.left = pRect.left + (pRect.width / 2) + "px";
			container.style.top = pRect.top + (pRect.height / 2) + "px";
			setTimeout(function() {
				$(container).addClass("animate-top-left");
				container.style.left = "50%";
				container.style.top = "45%";
			}, 500);*/
		}
		
		function restoreDiceContainer() {
			/*const overlay = document.getElementById("bowl_overlay");
			overlay.style.top = "";
			const container = document.getElementById("dice_container");
			const pRect = container.parentElement.getBoundingClientRect();
			container.style.transition = "";
			container.style.left = pRect.left + (pRect.width / 2) + "px";
			container.style.top = pRect.top + (pRect.height / 2) + "px";
			setTimeout(function() {
				$(overlay).hide();
				container.style.position = "";
				container.style.left = "";
				container.style.top = "";
				$(container).removeClass("animate-top-left");
			}, 1000);*/
		}
		
		function showBoard(message) {
			// show
			$('#board_overlay').show();
			$("#board_overlay_text").html(message);
		}
		
		function showResult(last, results) {
			// run animation
			setTimeout(function() {
				$(".light-board").removeClass("active");
				let grouped = {};
				results.sort().forEach((result, index) => {
					// loop previous
					Object.keys(grouped).forEach(pResult => {
						const key = result > pResult ? pResult + "_" + result : result + "_" + pResult;
						if (!grouped[key]) {
							grouped[key] = 2;
						} else {
							grouped[key] += 1;
						}
						$("#board_" + key).addClass("active");
					});
					if (!grouped[result]) {
						grouped[result] = 1;
					} else {
						grouped[result] += 1;
					}
					$("#light_board_" + result).addClass("active");
				});
				Object.keys(grouped).forEach(key => {
					$("#rate_" + key + "_" + grouped[key]).show();
				});
				//if ([...new Set(results)].length === 1) {
				//	$("#board_unique").addClass("active");
				//}
				$('#board_overlay').hide();				
				setTimeout(function() {
					loadTotalWon(last.id, results);
					// add history
					const container = $("#result_container");
					// const exist = $("#result_" + last.id);
					let content = "";
					let modalContent = "";
					last.results.forEach(result => {
						const img = document.createElement("img");
						img.src = host + "/images/stats/" + fpcOrders[result.value] + ".png";
						img.style.width = "100%";
						content += "<div class='fpc-result-dice-box'>" +
							img.outerHTML +							
						"</div>";
						modalContent += "<div class='col-4'>" + 
							"<img src='" + host + "/images/dices/" + fpcOrders[result.value] + ".png' width='100' height='100' style='border-radius: 20px;'>" + 
						"</div>";
						merged.push(result.value);
						if(!fpcCount[result.value]) {
							fpcCount[result.value] = 0;
						}
						fpcCount[result.value]++;
						if (merged.length > 30) {
							const removed = merged.splice(0, 1);
							fpcCount[removed[0]]--;
						}
					});
					for (let i = 1; i < 7; i++) {
						$("#percentage_" + i).text(((fpcCount[i] || 0) / merged.length * 100).toFixed(0) + "%");
					}					
					// if (exist.length > 0) {
						// exist.html(content);
					// } else {
						// container.children().last().remove();
						container.prepend("<div class='fpc-result-dice-container'>" +
							"<div id='result_" + last.id + "'>" +
								content +
							"</div>" +
						"</div>");
					// }
					$("#last_result_modal").html("<div class='col-12 col-sm-8 col-md-7 mx-auto'>" +
						"<div class='row'>" +
							modalContent +
						"</div>" +
					"</div>");
				}, 4000);
			}, 2500);
		}
		
		function openBowlCover() {
		}
		
		function closeBowlCover() {
		}		
		
		function disableBet() {
			$(".btn-control").prop("disabled", true);
			isOpening = true;
		}
		
		function enableBet() {
			$(".btn-control").prop("disabled", false);
			isOpening = false;
		}
		
		function showResultProcess(results, spent, won) {
			const balance = won - spent;
			const isWin = balance >= 0;
			// const resultDiv = document.createElement("div");
			// resultDiv.style.color = balance > 0 ? "green" : "red";
			// resultDiv.style.display = "inline-block";
			// resultDiv.innerText = (balance > 0 ? "+" : "") + balance.toFixed(2);
			openBowlCover();
			//restoreDiceContainer();
			setTimeout(function () {
				processLose(results);
				setTimeout(function() {
					$("#result_panel").hide();
					$('#result_modal').modal({show:true});
					$("#result_panel").slideDown(500);
					$("#total_balance").text(balance.toFixed(2));
					$("#total_balance").removeClass("text-success text-danger");
					if (isWin) {
						$("#balance_label").text("Win");
						$("#total_balance").addClass("text-success");
						$("#win_animation").show();
						winAudio.play().catch(err => {});
						setTimeout(function() {
							$('#result_modal').modal('hide');
							$("#win_animation").hide();
							// showBoard("Balance: " + resultDiv.outerHTML);
							setTimeout(function() {
								$('#board_overlay').hide();
								processWin(results);
								setTimeout(function() {
									betRestart();
								}, 2000);
							}, 1000);
						}, 5000);
					} else {
						$("#balance_label").text("Lose");
						$("#total_balance").addClass("text-danger");
						//showBoard("Balance: " + resultDiv.outerHTML);
						setTimeout(function() {
							$('#result_modal').modal('hide');
							betRestart();
						}, 2000);
					}
				}, 1000);
			}, 2000);
		}
		
		function betRestart() {
			@if (!$room->is_standalone)
				restartTimer();
				loadRecord();
			@endif
			resetBet();
			$(".rate-label").hide();
			$(".light-board").removeClass("active");
			closeBowlCover();
			showBoard("Place your bet");
			setTimeout(function() {
				$('#board_overlay').hide();
				enableBet();
			}, 1000);
		}
		
		function updateButtonTextStatus(id, text, disabled) {
			const btn = document.getElementById(id);
			if (btn) {
				btn.innerText = text;
				btn.disabled = disabled;
			}
		}
		
		function commit() {
			if (isCommiting) {
				return;
			}
			Swal.fire({
				icon: 'info',
				title: "结算",
				text: "当前可结算数额: " + won.toFixed(2),
				showCloseButton: true,
				showCancelButton: true,
				confirmButtonText: "结算",
				cancelButtonText: "取消"
			}).then(result => {
				if (result.isConfirmed) {
					isCommiting = true;
					updateButtonTextStatus("commit", "结算中", true);
					$.ajax({
						url: "{{ route('commit') }}",
						headers: {
							'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
						},
						type: "POST"
					}).done(function(res) {
						isCommiting = false;
						updateBalance(res?.user);
						updateButtonTextStatus("commit", "结算", false);
						Swal.fire("结算", "结算完成", "success");
					}).fail(function(err) {
						isCommiting = false;
						updateButtonTextStatus("commit", "结算", false);
						Swal.fire(err.status === 420 ? "无法进行" : "发生错误", err.responseJSON?.message, err.status === 420 ? "info" : "danger");
					});
				}
			});
		}
		
		function createRedPack() {
			const redpack = $("<div class='red-pack' onclick='claimRedPack()'>" +
				"<img src='{{ asset('images/redpack.jpg') }}' width='100' height='100' />" +
			"</div>");
			document.body.append(redpack[0]);
		}
		
		function claimRedPack() {
			$.ajax({
				url: "{{ route('room.redpack', ['id' => $room->id]) }}",
				type: "POST",
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				updateBalance(res.user);
				// play sound?
				$(".red-pack").remove();
			}).fail(function(err) {
				$(".red-pack").remove();
				showToastMessage(err?.responseJSON?.message || err.responseText, err.status === 420 ? "info" : "danger");
			});
		}
		
		function processBet(last) {
			disableBet();
			const container = document.getElementById("dice_container");
			container.style.transition = "top 0.5s";
			container.style.top = "30%";
			setTimeout(function() {
				container.style.transition = "top 0.2s";
				container.style.top = "50%";
			}, 500);
			setTimeout(function() {
				const results = last.results.map(x => x.value);
				rollAllDice(results);
				showResult(last, results);
			}, 600);
		}
		
		function duplicateChipAudio() {
			const audio = document.createElement("audio");
			audio.volume = (Math.random() * 0.9) + 0.1;
			audio.src = chipAudio.getElementsByTagName("source")[0].src;
			audio.play().then(function() { audio.remove(); }).catch(function() {});
		}
		
		function processLose(results) {
			let except = "";
			results.forEach(result => {
				except += ":not(.chip-board-" + result + ")";
			});
			const chips = $(".chip-board" + except);
			for (let index = 0; index < chips.length; index++) {
				const chip = chips[index];
				chip.style.left = "50%";
				chip.style.top = "-100px";
				setTimeout(function() {
					chip.remove();
				}, 1000);
				setTimeout(function() {
					duplicateChipAudio();
				}, index * 30);
			}
		}
		
		function processWin(results) {
			let only = "";
			new Set(results).forEach(result => {
				if (only) {
					only += ", ";
				}
				only += ".chip-board-" + result;
			});
			const containerRect = $("#pending_record")[0].getBoundingClientRect();
			const balanceRect = $(".balance-container")[0].getBoundingClientRect();
			const chips = $(only);
			for (let index = 0; index < chips.length; index++) {
				const chip = chips[index];
				chip.style.left = balanceRect.left - - (chip.clientWidth / 2) - containerRect.left + (balanceRect.width / 2) + "px";
				chip.style.top = balanceRect.top - (chip.clientHeight / 2) - containerRect.top + (balanceRect.height / 2) + "px";
				setTimeout(function() {
					chip.remove();
				}, 1000);
				setTimeout(function() {
					duplicateChipAudio();
				}, index * 30);
			}
		}
		
		function reBetLast() {
			$("#rebetBtn").prop("disabled", true);
			return $.ajax({
				url: "{{ route('room.rebetLast', ['id' => $room->id]) }}",
				type: "POST",
				data: {
					betting_id: $("#betting_id").val()
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(res) {
				updateBalance(res.user);
				if (res.records.length > 0) {
					rebetId = res.records[0].group;
				}
				const records = [];
				// group record
				res.records.forEach(function(record) {
					let lastIndex = chipAmounts.length - 1;
					// get the minimum deductable amount
					while (record.amount < chipAmounts[lastIndex] && lastIndex > 0) {
						lastIndex--;
					}
					while (record.amount >= chipAmounts[lastIndex]) {
						const clone = {...record};
						clone.amount = chipAmounts[lastIndex];
						records.push(clone);
						//
						record.amount -= chipAmounts[lastIndex];
						if (record.amount < chipAmounts[lastIndex] && lastIndex > 0) {
							lastIndex--;
						}
					}
				});
				// place chip to board
				records.forEach(function(record) {
					const ele = document.getElementById("board_" + record.play_option.replaceAll("-", "_"));
					if (ele) {
						const position = getRecordPosition(record.play_option);
						const room_rate_id = rates.find(x => x.group === record.play_group)?.id;
						addRecordProcess(ele, record.amount, record.play_option, room_rate_id, false, position !== null, false, position, record.id, record.betting_id);
					}
				});
				@if ($room->is_standalone)
				updateBetAmount();
				@endif
			}).fail(function(err) {
				$("#rebetBtn").prop("disabled", false);
				showToastMessage(err?.responseJSON?.message || err.responseText, err.status === 420 ? "info" : "danger");
			});
		}
	</script>
	@if(!$room->is_standalone)
	<script src="{{ asset('js/fpc.js') }}?v=5" defer></script>
	@endif
	<script src="{{ asset('js/dice.js') }}?v=5" defer></script>
@endsection

@section('footer-js')
<script>
	@if($room->is_standalone)
	const cfmBtn = document.getElementById("confirm");
	function confirmStandaloneBet() {
		const promise = confirmBet(pendingRecords);
		if (promise) {
			promise.then(res => {
				$("#betting_id").val(res.result?.id);
				updateBetAmount();
				showNoMoreBet();
				setTimeout(function() {
					$("#betting_id").val(res.result.id);
					processBet(res.result);
				}, 1000);
			});
		}
	}
	cfmBtn.addEventListener("click", confirmStandaloneBet);
	@endif
	
	function postUndo(lastBetIndex, lastBet) {
		if (lastBet.ele_id) {
			$("#chip" + lastBet.ele_id).remove();
		} else {			
			const chipEleId = lastBet.id ? "pending_record" : "commit_record";
			$("#" + chipEleId).children().last().remove();
		}
		pendingRecords.splice(lastBetIndex, 1);
		// check if remain last rebet chip
		const lastGroupBets = pendingRecords.filter(x => x.betting_id == rebetId);
		if (lastGroupBets.length === 0) {
			$("#rebetBtn").prop("disabled", false);
		}
		syncGroupedRecord();
		updateBetAmount();
		if (pendingRecords.length === 0) {
			$("#undoBtn").prop("disabled", true);
			$("#clearBtn").prop("disabled", true);
			$("#rebetBtn").prop("disabled", false);
		}
	}
	
	function undo() {
		const lastBetIndex = pendingRecords.length - 1;
		const lastBet = lastBetIndex > -1 ? pendingRecords[lastBetIndex] : null;
		if (lastBet) {
			if (lastBet.id) {
				const promise = clearRecord([lastBet.id]);
				if (!promise) {
					postUndo(lastBetIndex, lastBet);
				}
			} else {
				postUndo(lastBetIndex, lastBet);
			}
		}
	}
	
	function rebet() {
		reBetLast();
	}
	
	function getRecordPosition(value) {
		const values = value.split("-");
		if (values.length > 1) {
			if (values[0] % 3 !== 0 && values[1] - values[0] === 1) {
				return { top: true, right: true };
			} else if (value[0] < 4 && values[1] - values[0] === 3) {
				return { bottom: true, left: true };
			} else {
				return null; // bottom board, without offset
			}
		} else {
			return undefined; // joined board, with offset
		}
	}
</script>
<script>
	// audio
	let overlayShow = true;
	let bgEnabled = localStorage.getItem("bg_enabled", '') ? true : false;
	
	const chipAudio = document.getElementById("chip_audio");
	chipAudio.load();
	const diceAudio = document.getElementById("dice_audio");
	diceAudio.load();
	const winAudio = document.getElementById("win_audio");
	winAudio.load();
	winAudio.volume = 0.1;
	const bgAudio = document.getElementById("bg_audio");
	bgAudio.load();
	bgAudio.volume = 0.1;
	
	const bgAudioBtn = document.getElementById("bgAudioBtn");
	bgAudioBtn.getElementsByTagName("i")[0].className = "feather " + (bgEnabled ? "icon-volume-1" : "icon-volume-x");
	
	bgAudioBtn.addEventListener("click", event => {
		bgEnabled = !bgEnabled;
		bgAudioBtn.getElementsByTagName("i")[0].className = "feather " + (bgEnabled ? "icon-volume-1" : "icon-volume-x");
		if (!overlayShow) {				
			if (bgEnabled) {
				bgAudio.play().catch(err => {});
			} else {
				bgAudio.pause();
			}
		}
		localStorage.setItem("bg_enabled", bgEnabled ? 'enabled' : '');
		event.stopPropagation();
	});
	
	const overlay = document.getElementById("fpc_overlay");
	overlay.addEventListener("click", event => {
		if (bgEnabled) {
			bgAudio.play().catch(err => {});
		}
		overlay.style.display = "none";
		overlayShow = false;
	});
		
	function disableDoubleTapZoom(evt) {
		let t2 = evt.timeStamp
		  , t1 = evt.target.dataset.lastTouch || t2
		  , dt = t2 - t1
		  , fingers = evt.touches.length;
		evt.target.dataset.lastTouch = t2;
		if (!dt || dt > 500 || fingers > 1) return; // not double-tap

		evt.preventDefault(); // double tap - prevent the zoom
		// also synthesize click events we just swallowed up
		evt.target.click();
	}

	window.addEventListener('touchstart', disableDoubleTapZoom, { passive: false });
	
	function preHandleBack() {
		Swal.fire({
			icon: 'warning',
			title: "Return to wallet",
			text: "Are you sure you want to leave?",
			showCloseButton: true,
			showCancelButton: true,
			confirmButtonText: "Yes",
			cancelButtonText: "No"
		}).then(res => {
			if (res.isConfirmed) {
				handleBack();
			}
		});
	}
</script>
@endsection
