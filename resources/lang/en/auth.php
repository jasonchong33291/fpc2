<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => '账号密码不一致。',
    'password' => '账号密码不一致。',
    'throttle' => '太多登入请求，请等待:seconds分钟。',

];
