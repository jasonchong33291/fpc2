function updateMember(key) {
	$("." + key + "_count").text(Object.keys(members[key] || {}).length);
}

const pusher = new Pusher(pusherKey, {
  cluster: 'ap1',
  authEndpoint: host + "/broadcasting/auth",
  auth: { headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content') } },
});

function subscribeRoom(roomId, isAdmin = false) {
	const channel = pusher.subscribe("presence-room." + roomId);
	channel.bind("pusher:subscription_succeeded", function (evt) {
	  members[roomId] = evt.members;
	  if(isAdmin) {
		dTable.draw();
	  } else {
		updateMember(roomId);
	  }
	});
	channel.bind("pusher:member_added", function (evt) {
	  members[roomId][evt.member.id] = evt.member.info;
	  if(isAdmin) {
		dTable.draw();
	  } else {
		updateMember(roomId);
	  }
	});
	channel.bind("pusher:member_removed", function (evt) {
	  delete members[roomId][evt.member.id];
	  if(isAdmin) {
		dTable.draw();
	  } else {
		updateMember(roomId);
	  }
	});
}

if(roomIds) {
	roomidsArray.forEach((roomIds, key) => {
		roomIds.forEach(roomId => {
			subscribeRoom(roomId, );
		});
	});
}