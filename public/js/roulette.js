let interval;
let noMoreBet;

function restartTimer() {
	clearInterval(interval);
	startTimer();
}

function startTimer() {
	updateTime();
	interval = setInterval(updateTime, 1000);
}
	
function updateTime() {
	if(startDate) {
		const currDate = new Date();
		const remainDate = new Date(startDate) - currDate;
		if(remainDate > 0) {
			$("#hour").text("00");
			const remainMinute = remainDate % 3600000;
			$("#minute").text(Math.floor(remainMinute > 0 ? (remainMinute / 60000) : 0).toString().padStart(2, 0));
			const remainSeconds = remainDate % 60000;
			$("#second").text(Math.floor(remainSeconds > 0 ? (remainSeconds / 1000) : 0).toString().padStart(2, 0));
			const totalRemainSeconds = remainDate % 3600000 * 60 / 60000;
			if (totalRemainSeconds <= 1 && !noMoreBet) {
				noMoreBet = true;
				showNoMoreBet();
			}
		} else {
			noMoreBet = false;
			clearInterval(interval);
			$("#hour").text("--");
			$("#minute").text("--");
			$("#second").text("--");
		}
	} else {
		noMoreBet = false;
		clearInterval(interval);
		$("#hour").text("--");
		$("#minute").text("--");
		$("#second").text("--");
	}
}

function updateBet(data) {
	if(data) {
		if(data.last) {
			const results = $(".result");
			const resultLeft = $("#result_left");
			const resultRight = $("#result_right");
			if (data.last.results.length > 0) {
				const i = document.createElement("i");
				i.className = "background-" + data.last.results[0].roulette_color + " roulette-result";
				i.innerHTML = (data.last.results[0].value).toString().padStart(2, "0");				
				const content = "<div class='roulette-result roulette-ball'>" +
					i.outerHTML +
				"</div>";
				const exist = $("#result_" + data.last.id);	
				const date = new Date("2022-01-01T" + data.last.version);
				if (exist.length > 0) {
					exist.html("<div class='roulette-time text-muted'>" +
							date.getHours().toString().padStart(2, "0") + ":" + date.getMinutes().toString().padStart(2, "0") +
						"</div>" +
						content);
				} else {
					const resultToAddRight = $("<div class='row'><div class='d-flex mx-auto'>" +
						"<div class='roulette-time text-muted'>" +
							date.getHours().toString().padStart(2, "0") + ":" + date.getMinutes().toString().padStart(2, "0") +
						"</div>" +
						content +
					"</div></div>");				
					const resultRightToMoveLeft = resultRight.children().first();
					resultLeft.append(resultRightToMoveLeft[0].outerHTML);
					resultLeft.children().first().remove();
					resultRight.append(resultToAddRight);
					resultRightToMoveLeft.remove();
				}								
				showResult(data.last.id, i.outerHTML, data.last.results[0]);
			}
		}		
		if(data.current) {
			$("#betting_id").val(data.current.id);
			$("#current_version").text(data.current.version);
			startDate = data.current.ended_at_timestamp * 1000;
		}
	}		
}

let members = {};

function updateMember() {
	$("#member_count").text(Object.keys(members).length);
}

if(channelName) {
	const pusher = new Pusher(pusherKey, {
	  cluster: 'ap1',
	  authEndpoint: host + "/broadcasting/auth",
	  auth: { headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content') } },
	});

	const channel = pusher.subscribe(channelName);
	if(eventName) {
		channel.bind(eventName, updateBet);
	}
	channel.bind("pusher:subscription_succeeded", function (evt) {
	  members = evt.members;
	  updateMember();
	});
	channel.bind("pusher:member_added", function (evt) {
	  if (!evt.member) {
		  return;
	  }
	  members[evt.member.id] = evt.member.info;
	  updateMember();
	});
	channel.bind("pusher:member_removed", function (evt) {
	  if (!evt.member) {
		  return;
	  }
	  delete members[evt.member.id];
	  updateMember();
	});
}

startTimer();
updateBet({ last: null });
loadRecord();
addHistories(histories);