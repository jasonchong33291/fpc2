const dices = [
	document.getElementById('dice1'),
	document.getElementById('dice2'),
	document.getElementById('dice3')
];

const defaultDiceTimeout = 2000;
const diceInterval = 400;
const remainDiceTimeouts = [0, 0, 0];
function showDice(el, value) {
	el.classList.remove('show-' + el.dataset.value);
	el.classList.add('show-' + value);
	el.dataset.value = value;
}

function rollDice(i, el, finalValue) {
	if (remainDiceTimeouts[i] > 0) {
		remainDiceTimeouts[i] -= diceInterval;
		const value = Math.floor(Math.random() * 6);
		showDice(el, value);
		setTimeout(function() {
			rollDice(i, el, finalValue);
			if (bgEnabled)
				diceAudio.play();
		}, diceInterval);
	} else {
		showDice(el, finalValue);		
	}
}

function rollAllDice(dicesValue) {
	for (let i = 0; i < 3; i++) {
		remainDiceTimeouts[i] = defaultDiceTimeout;
		rollDice(i, dices[i], dicesValue[i]);
	}
}

if(lastResult) {
	for (let i = 0; i < 3; i++) {
		showDice(dices[i], lastResult[i]);
	}
}