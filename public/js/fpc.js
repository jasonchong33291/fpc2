let interval;
let noMoreBet;

function restartTimer() {
	clearInterval(interval);
	startTimer();
}

function startTimer() {
	updateTime();
	interval = setInterval(updateTime, 1000);
}
	
function updateTime() {
	if(startDate) {
		const currDate = new Date();
		const remainDate = new Date(startDate) - currDate;
		if(remainDate > 0) {
			const remainSeconds = remainDate % 3600000 * 60 / 60000;
			$("#second").text(Math.floor(remainSeconds > 0 ? remainSeconds : 0).toString().padStart(2, 0));
			if (remainSeconds <= 1 && !noMoreBet) {
				noMoreBet = true;
				showNoMoreBet();
			}
		} else {
			noMoreBet = false;
			clearInterval(interval);
			$("#second").text("--");
		}
	} else {
		noMoreBet = false;
		clearInterval(interval);
		$("#second").text("--");
	}
}

function updateBet(data) {
	if(data) {
		if(data.last) {
			processBet(data.last);
		}
		if(data.current) {
			$("#betting_id").val(data.current.id);
			startDate = data.current.ended_at_timestamp * 1000;
		}
	}		
}

let members = {};

function updateMember() {
	$("#member_count").text(Object.keys(members).length);
}

if(channelName) {
	const pusher = new Pusher(pusherKey, {
	  cluster: 'ap1',
	  authEndpoint: host + "/broadcasting/auth",
	  auth: { headers: { "X-CSRF-Token": $('meta[name="csrf-token"]').attr('content') } },
	});

	const channel = pusher.subscribe(channelName);
	if(eventName) {
		channel.bind(eventName, updateBet);
	}
	channel.bind("pusher:subscription_succeeded", function (evt) {
	  members = evt.members;
	  updateMember();
	});
	channel.bind("pusher:member_added", function (evt) {
	  if (!evt.member) {
		  return;
	  }
	  members[evt.member.id] = evt.member.info;
	  updateMember();
	});
	channel.bind("pusher:member_removed", function (evt) {
	  if (!evt.member) {
		  return;
	  }
	  delete members[evt.member.id];
	  updateMember();
	});
}

startTimer();
updateBet({ last: null });
loadRecord();