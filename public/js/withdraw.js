function withdraw() {
	Swal.fire({
		text: "提取金额",
		input: "number",
		inputValue: minWithdraw,
		inputValidator: function(value) {
			return value >= minWithdraw ? "" : ("提取金额不可少过" + minWithdraw + "美金。");
		}
	}).then(result => {
		if(result.isConfirmed) {
			$.ajax({
				url: withdrawURL,
				type: "POST",
				data: {
					amount: result.value
				},
				headers: {
					'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
				}
			}).done(function(user) {
				Swal.fire("提取", "提取申请提交成功。", "success");
			}).fail(function(err) {
				Swal.fire("发生错误", err.responseJSON.message, "error");
			});
		}
	});
}

$(".withdraw").on('click', withdraw);